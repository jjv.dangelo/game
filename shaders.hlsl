#define MAX_LIGHTS 8
#define MAX_MATERIALS 16

struct Directional {
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 direction;
    int enabled;
};

struct Point {
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 position;
    float range;
    float3 att;
    int enabled;
};

struct Spot {
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 position;
    float range;
    float3 direction;
    float spot;
    float3 att;
    float pad;
};

struct Material {
    float4 emissive;
    float4 ambient;
    float4 diffuse;
    float4 specular;
};

cbuffer PerObject: register(b0) {
     float4x4 world;
     float4x4 world_inv_trans;
}
 
cbuffer PerFrame: register(b1) {
    float4x4 view_proj;
    float elapsed;
    float dt;
    float3 eye_pos;
}

cbuffer Lights: register(b2) {
    Directional directional[MAX_LIGHTS];
    Point points[MAX_LIGHTS];
}

cbuffer Materials: register(b3) {
    Material materials[MAX_MATERIALS];
}

Texture2D _texture: register(t0);
SamplerState _sampler_state: register(s0);

struct VIn {
    float3 position  : POSITION;
    float3 normal    : NORMAL;
    float2 tex_coord : TEXCOORD;
    uint   material  : MATERIAL;
};

struct VOut {
    float4 position  : SV_POSITION;
    float3 position_world : POSITION;
    float3 normal    : NORMAL;
    float2 tex_coord : TEXCOORD1;
    int    material  : TEXCOORD2;
};

/// PIXEL SHADER
void compute_directional(Material mat, Directional l, float3 norm, float3 to_eye,
                         out float4 ambient, out float4 diffuse, out float4 specular)
{
    ambient = float4(0., 0., 0., 0.);
    diffuse = float4(0., 0., 0., 0.);
    specular = float4(0., 0., 0., 0.);

    float3 light_vec = -l.direction;
    ambient = mat.ambient * l.ambient;

    float diffuse_factor = dot(light_vec, norm);

    [flatten]
    if(diffuse_factor > 0.) {
        float3 r = reflect(-light_vec, norm);
        float spec_factor = pow(max(dot(r, to_eye), 0.), mat.specular.a);

        diffuse = diffuse_factor * mat.diffuse * l.diffuse;
        specular = spec_factor * mat.specular * l.specular;
    }
}

void compute_point(Material mat, Point p, float3 pos, float3 norm, float3 to_eye,
                   out float4 ambient, out float4 diffuse, out float4 specular)
{
    ambient = float4(0., 0., 0., 0.);
    diffuse = float4(0., 0., 0., 0.);
    specular = float4(0., 0., 0., 0.);

    float3 light_vec = p.position - pos;
    float d = length(light_vec);

    if(d > p.range) return;

    light_vec /= d;

    ambient = mat.ambient * p.ambient;

    float diffuse_factor = dot(light_vec, norm);

    [flatten]
    if(diffuse_factor > 0.) {
        float3 r = reflect(-light_vec, norm);
        float spec_factor = pow(max(dot(r, to_eye), 0.), mat.specular.a);

        diffuse = diffuse_factor * mat.diffuse * p.diffuse;
        specular = spec_factor * mat.specular * p.specular;
    }

    float att = 1. / dot(p.att, float3(1., d, d*d));
    diffuse *= att;
    specular *= att;
}

VOut VShader(VIn input) {
    VOut output;

    float4x4 wvp = mul(world, view_proj);

    output.position = mul(float4(input.position, 1.), wvp);
    output.position_world = mul(float4(input.position, 1.), world).xyz;
    output.normal = normalize(mul(input.normal, (float3x3)world_inv_trans));
    output.tex_coord = input.tex_coord;
    output.material = input.material;

    return output;
}

float4 PShader(VOut vs_output) : SV_TARGET {
    Material material = materials[vs_output.material];
    float3 to_eye = normalize(eye_pos - vs_output.position_world);

    float4 ambient = float4(0., 0., 0., 0.);
    float4 diffuse = float4(0., 0., 0., 0.);
    float4 specular = float4(0., 0., 0., 0.);

    float4 a, d, s;

    [unroll]
    for(int i = 0; i < MAX_LIGHTS; ++i) {
        Directional light = directional[i];

        [flatten]
        if(light.enabled) {
            compute_directional(material, light, vs_output.normal, to_eye, a, d, s);
            ambient += a;
            diffuse += d;
            specular += s;
        } else {
            break;
        }
    }

    [unroll]
    for(i = 0; i < MAX_LIGHTS; ++i) {
        Point light = points[i];

        [flatten]
        if(light.enabled) {
            compute_point(material, light, vs_output.position_world, vs_output.normal, to_eye, a, d, s);
            ambient += a;
            diffuse += d;
            specular += s;
        } else {
            break;
        }
    }

    float4 color = ambient + diffuse + specular + material.emissive;
    color.a = material.diffuse.a;

    return color;
}
