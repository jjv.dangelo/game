use crate::{math::Vec2, renderer::Color4};

pub const FONT_SHEET_CHARS_X: usize = 64;
pub const FONT_SHEET_CHARS_Y: usize = 64;
pub const FONT_SHEET_MAX_CHARS: usize = FONT_SHEET_CHARS_X * FONT_SHEET_CHARS_Y;

#[derive(Clone, Copy)]
pub struct CharSpacing {
    pub x_offset: i32,
    pub width: u32,
}

#[derive(Clone, Copy)]
pub struct FontCoordinates {
    pub top_left: Vec2,
    pub bottom_right: Vec2,
}

#[derive(Clone)]
pub struct SpriteSheet {
    pub width: u32,
    pub height: u32,
    pub image: Vec<Color4>,
}

#[derive(Clone)]
pub struct Font {
    pub spacing: [CharSpacing; FONT_SHEET_MAX_CHARS],
    pub coordinates: [FontCoordinates; FONT_SHEET_MAX_CHARS],
    pub sprite_sheet: SpriteSheet,
}
