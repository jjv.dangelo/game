#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Index {
    value: usize,
    generation: u64,
}

#[derive(Copy, Clone, Debug, Default)]
struct AllocatorEntry {
    is_live: bool,
    generation: u64,
}

pub struct GenerationalIndexAllocator {
    entries: Vec<AllocatorEntry>,
    free: Vec<usize>,
}

impl GenerationalIndexAllocator {
    pub fn with_capacity(len: usize) -> Self {
        let entries = std::iter::repeat_with(Default::default).take(len).collect();
        let free = (0..len).collect();

        Self { entries, free }
    }

    #[allow(dead_code)] // @TODO: dead_code
    pub fn allocate(&mut self) -> Index {
        // TODO: When we run out of free spaces, we should grow the `entries` and add the new free
        // spaces to `free`.
        let next = self.free.pop().unwrap();
        let entry = &mut self.entries[next];

        assert!(!entry.is_live);

        entry.is_live = true;
        entry.generation += 1;

        Index {
            value: next,
            generation: entry.generation,
        }
    }

    #[allow(dead_code)]
    pub fn deallocate(&mut self, index: Index) -> bool {
        let entry = &mut self.entries[index];

        if entry.is_live && entry.generation == index.generation {
            self.free.push(index.value);
            entry.is_live = false;
            true
        } else {
            false
        }
    }

    #[allow(dead_code)]
    pub fn is_live(&self, index: Index) -> bool {
        self.entries[index].is_live
    }

    #[allow(dead_code)]
    pub fn iter<F: FnMut(Index) -> bool>(&self, mut f: F) {
        let active_entries = self
            .entries
            .iter()
            .enumerate()
            .filter(|(_, e)| e.is_live)
            .map(|(value, e)| Index {
                value,
                generation: e.generation,
            });

        for entry in active_entries {
            if !f(entry) {
                break;
            }
        }
    }
}

impl std::ops::Index<Index> for Vec<AllocatorEntry> {
    type Output = AllocatorEntry;

    fn index(&self, index: Index) -> &Self::Output {
        &self[index.value]
    }
}

impl std::ops::IndexMut<Index> for Vec<AllocatorEntry> {
    fn index_mut(&mut self, index: Index) -> &mut AllocatorEntry {
        &mut self[index.value]
    }
}

pub struct ArrayEntry<T> {
    value: T,
    generation: u64,
}

pub struct Array<T>(Vec<Option<ArrayEntry<T>>>);

#[allow(dead_code)] // @TODO: dead_code
impl<T> Array<T> {
    pub fn with_capacity(len: usize) -> Self {
        let values = std::iter::repeat_with(|| None).take(len).collect();
        Array(values)
    }

    pub fn set(&mut self, index: Index, value: T) -> bool {
        let entry = &mut self.0[index.value];

        if let Some(mut entry) = entry.as_mut() {
            let should_update = entry.generation <= index.generation;
            if should_update {
                entry.generation = index.generation;
                entry.value = value;
            }

            should_update
        } else {
            let generation = index.generation;
            *entry = Some(ArrayEntry { value, generation });

            true
        }
    }

    pub fn get(&self, index: Index) -> Option<&T> {
        let entry = &self.0[index.value];

        if let Some(entry) = entry.as_ref() {
            if entry.generation == index.generation {
                return Some(&entry.value);
            }
        }

        return None;
    }

    pub fn get_mut(&mut self, index: Index) -> Option<&mut T> {
        let entry = &mut self.0[index.value];

        if let Some(entry) = entry.as_mut() {
            if entry.generation == index.generation {
                return Some(&mut entry.value);
            }
        }

        return None;
    }
}
