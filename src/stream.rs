pub struct ByteStream<I: IntoIterator<Item = u8>> {
    iter: I::IntoIter,
    buf: u32,
    cnt: u32,
}

impl<I: IntoIterator<Item = u8>> ByteStream<I> {
    pub fn new(iter: I) -> Self {
        let iter = iter.into_iter();
        Self {
            iter,
            buf: 0,
            cnt: 0,
        }
    }

    fn buffer_bytes(&mut self, cnt: u32) -> Option<()> {
        while self.cnt < cnt {
            let byte = self.iter.next()?;
            self.buf |= (byte as u32) << self.cnt;
            self.cnt += 8;
        }

        Some(())
    }

    pub fn read_bits(&mut self, cnt: u32) -> Option<u32> {
        if cnt > 32 {
            return None;
        }

        self.buffer_bytes(cnt)?;

        self.cnt -= cnt;
        let result = self.buf & ((1 << cnt) - 1);
        self.buf >>= cnt;

        Some(result)
    }

    pub fn read_byte(&mut self) -> Option<u8> {
        self.read_bits(8).map(|result| result as u8)
    }

    pub fn flush(&mut self) -> Option<u32> {
        let to_flush = self.cnt % 8;
        self.read_bits(to_flush)
    }

    pub fn read<T: Copy>(&mut self) -> Option<T> {
        if self.cnt != 0 {
            return None;
        }

        let len_bytes = std::mem::size_of::<T>();
        let mut mem = Vec::with_capacity(len_bytes);

        for _ in 0..len_bytes {
            let byte = self.iter.next()?;
            mem.push(byte);
        }

        let result = unsafe { (mem.as_ptr() as *const T).as_ref() };

        result.cloned()
    }
}
