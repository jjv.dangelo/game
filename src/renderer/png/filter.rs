use crate::stream::ByteStream;

const FILTER_NONE: u8 = 0;
const FILTER_SUB: u8 = 1;
const FILTER_UP: u8 = 2;
const FILTER_AVG: u8 = 3;
const FILTER_PAETH: u8 = 4;

#[derive(Clone, Copy)]
struct ColorInfo {
    width: usize,
    color_type: u8,
    bit_depth: u8,
}

impl ColorInfo {
    const fn new(width: usize, color_type: u8, bit_depth: u8) -> Self {
        Self {
            width,
            color_type,
            bit_depth,
        }
    }

    fn bytes_per_pixel(&self) -> u8 {
        match self.color_type {
            0 => 1,
            2 => 3,
            3 => 1,
            4 => 2,
            6 => 4,
            _ => unreachable!(),
        }
    }

    fn stride(&self) -> usize {
        self.width * self.bytes_per_pixel() as usize
    }
}

pub fn unfilter(
    filtered_data: Vec<u8>,
    width: usize,
    height: usize,
    color_type: u8,
    color_depth: u8,
) -> Option<(Vec<u8>, u8)> {
    let mut stream = ByteStream::new(filtered_data);
    let color_info = ColorInfo::new(width, color_type, color_depth);

    let stride = color_info.stride();
    let bpp = color_info.bytes_per_pixel();
    let mut data = Vec::with_capacity(height * stride);

    let mut prev = vec![0; stride];
    let mut curr = vec![0; stride];

    for _ in 0..height {
        let filter_method = stream.read_byte()?;

        for curr in curr.iter_mut() {
            *curr = stream.read_byte()?;
        }

        match filter_method {
            FILTER_NONE => (),
            FILTER_SUB => sub(bpp as usize, &mut curr[..]),
            FILTER_UP => up(&prev[..], &mut curr[..]),
            FILTER_AVG => avg(bpp as usize, &prev[..], &mut curr[..]),
            FILTER_PAETH => paeth(bpp as usize, &prev[..], &mut curr[..]),

            _ => {
                unimplemented!("Filter {} not implemented.", filter_method);
            }
        }

        for &curr in curr.iter() {
            data.push(curr)
        }

        std::mem::swap(&mut prev, &mut curr);
    }

    Some((data, bpp))
}

pub fn sub(bpp: usize, curr: &mut [u8]) {
    let len = curr.len();

    for i in bpp..len {
        curr[i] = curr[i].wrapping_add(curr[i - bpp]);
    }
}

pub fn up(prev: &[u8], curr: &mut [u8]) {
    for i in 0..curr.len() {
        curr[i] = curr[i].wrapping_add(prev[i]);
    }
}

pub fn avg(bpp: usize, prev: &[u8], curr: &mut [u8]) {
    for i in 0..bpp {
        curr[i] = curr[i].wrapping_add(prev[i] / 2);
    }

    for i in bpp..curr.len() {
        curr[i] = curr[i].wrapping_add(((curr[i - bpp] as i16 + prev[i] as i16) / 2) as u8);
    }
}

pub fn paeth(bpp: usize, prev: &[u8], curr: &mut [u8]) {
    #[inline]
    fn predict(a: u8, b: u8, c: u8) -> u8 {
        let ia = a as i16;
        let ib = b as i16;
        let ic = c as i16;

        let p = ia + ib - ic;
        let pa = (p - ia).abs();
        let pb = (p - ib).abs();
        let pc = (p - ic).abs();

        if pa <= pb && pa <= pc {
            a
        } else if pb <= pc {
            b
        } else {
            c
        }
    }

    for i in 0..bpp {
        curr[i] = curr[i].wrapping_add(predict(0, prev[i], 0));
    }

    for i in bpp..curr.len() {
        curr[i] = curr[i].wrapping_add(predict(curr[i - bpp], prev[i], prev[i - bpp]));
    }
}
