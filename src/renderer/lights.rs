#![allow(dead_code)]
use crate::{math::Vec3, renderer::Color4};

#[derive(Clone, Copy, Default)]
pub struct Directional {
    ambient: Color4,
    diffuse: Color4,
    specular: Color4,
    direction: Vec3,
}

impl Directional {
    pub fn new(ambient: Color4, diffuse: Color4, specular: Color4, direction: Vec3) -> Self {
        Self {
            ambient: ambient.pre_mul_alpha(),
            diffuse: diffuse.pre_mul_alpha(),
            specular: specular.pre_mul_alpha(),
            direction,
        }
    }
}

#[derive(Clone, Copy, Default)]
pub struct Point {
    ambient: Color4,
    diffuse: Color4,
    specular: Color4,
    position: Vec3,
    range: f32,
    att: Vec3,
}

impl Point {
    pub fn new(
        ambient: Color4,
        diffuse: Color4,
        specular: Color4,
        position: Vec3,
        range: f32,
        att: Vec3,
    ) -> Self {
        Self {
            ambient: ambient.pre_mul_alpha(),
            diffuse: diffuse.pre_mul_alpha(),
            specular: specular.pre_mul_alpha(),
            position,
            range,
            att,
        }
    }
}

#[derive(Clone, Copy, Default)]
pub struct Spot {
    ambient: Color4,
    diffuse: Color4,
    specular: Color4,
    position: Vec3,
    range: f32,
    direction: Vec3,
    spot: f32,
    att: Vec3,
}
