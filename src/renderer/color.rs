use std::ops::{Mul, MulAssign};

pub const WHITE: Color4 = Color4::rgb(1., 1., 1.);
pub const BLACK: Color4 = Color4::rgb(0., 0., 0.);
pub const BLUE: Color4 = Color4::rgb(0., 0., 1.);
pub const GREEN: Color4 = Color4::rgb(0., 1., 0.);
pub const RED: Color4 = Color4::rgb(1., 0., 0.);
pub const YELLOW: Color4 = Color4::rgb(1., 1., 0.);
pub const CYAN: Color4 = Color4::rgb(0., 1., 1.);
pub const PINK: Color4 = Color4::rgb(1., 0., 1.);
pub const PURPLE: Color4 = Color4::rgb(0.35, 0.2, 0.65);

/// Represents a 32-bit color.
#[repr(C)]
#[derive(Clone, Copy, Default)]
pub struct Color4 {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

#[allow(dead_code)] // @TODO: dead_code
impl Color4 {
    pub const fn new(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self { r, g, b, a }
    }

    pub const fn rgb(r: f32, g: f32, b: f32) -> Self {
        Self::new(r, g, b, 1.)
    }

    pub fn pre_mul_alpha(self) -> Self {
        Self::new(self.r * self.a, self.g * self.a, self.b * self.a, self.a)
    }
}

impl std::fmt::Debug for Color4 {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            fmt,
            "<{:.2}, {:.2}, {:.2}, {:.2}>",
            self.r, self.g, self.b, self.a
        )
    }
}

impl Mul<f32> for Color4 {
    type Output = Self;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Self::new(
            self.r * scalar,
            self.g * scalar,
            self.b * scalar,
            self.a * scalar,
        )
    }
}

impl MulAssign<f32> for Color4 {
    #[inline]
    fn mul_assign(&mut self, scalar: f32) {
        self.r = self.r * scalar;
        self.g = self.g * scalar;
        self.b = self.b * scalar;
        self.a = self.a * scalar;
    }
}

impl From<[f32; 4]> for Color4 {
    fn from(x: [f32; 4]) -> Self {
        Self::new(x[0], x[1], x[2], x[3])
    }
}

impl From<&[f32; 4]> for Color4 {
    fn from(x: &[f32; 4]) -> Self {
        Self::new(x[0], x[1], x[2], x[3])
    }
}
