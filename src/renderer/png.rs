#![allow(dead_code)]

use crate::{deflate, renderer::Color4, stream::ByteStream};

mod filter;

const IHDR_CHUNK_TYPE: &'static [u8] = b"IHDR";
const IEND_CHUNK_TYPE: &'static [u8] = b"IEND";
const IDAT_CHUNK_TYPE: &'static [u8] = b"IDAT";

pub struct Png {
    pub width: usize,
    pub height: usize,
    pub data: Vec<Color4>,
}

trait Endian {
    fn reverse_end(self) -> Self;
}

impl Endian for u32 {
    fn reverse_end(self) -> Self {
        let result = ((self & 0xFF000000) >> 24)
            | ((self & 0x00FF0000) >> 8)
            | ((self & 0x0000FF00) << 8)
            | ((self & 0xFF) << 24);

        result
    }
}

#[repr(packed)]
#[derive(Clone, Copy)]
struct Chunk {
    len: u32,
    chunk_type: [u8; 4],
}

impl Chunk {
    fn len(&self) -> u32 {
        self.len.reverse_end()
    }
}

#[repr(packed)]
#[derive(Clone, Copy)]
struct Header {
    width: u32,
    height: u32,
    depth: u8,
    color_type: u8,
    compression_method: u8,
    filter_method: u8,
    interlace_method: u8,
}

impl Header {
    fn width(&self) -> u32 {
        self.width.reverse_end()
    }

    fn height(&self) -> u32 {
        self.height.reverse_end()
    }

    fn is_supported(&self) -> bool {
        self.depth == 8
            && (self.color_type == 6 || self.color_type == 2)
            && self.depth == 8
            && self.compression_method == 0
            && self.filter_method == 0
            && self.interlace_method == 0
    }
}

#[repr(packed)]
#[derive(Clone, Copy)]
struct Footer {
    _crc: u32,
}

#[repr(packed)]
#[derive(Clone, Copy)]
pub struct Prelude([u8; 8]);

impl Prelude {
    fn is_valid(self) -> bool {
        const PNG_PRELUDE: [u8; 8] = [137, 80, 78, 71, 13, 10, 26, 10];

        self.0 == PNG_PRELUDE
    }
}

impl Png {
    pub fn from_bytes(bytes: Vec<u8>) -> Option<Self> {
        let mut stream = ByteStream::new(bytes);

        let prelude = stream.read::<Prelude>()?;
        if !prelude.is_valid() {
            return None;
        }

        let header_chunk = stream.read::<Chunk>()?;
        if header_chunk.chunk_type != IHDR_CHUNK_TYPE
            || header_chunk.len() != std::mem::size_of::<Header>() as u32
        {
            return None;
        }

        let header = stream.read::<Header>()?;
        if !header.is_supported() {
            return None;
        }

        let _footer = stream.read::<Footer>()?;

        let mut deflate_data = Vec::new();
        loop {
            let chunk = stream.read::<Chunk>()?;

            match &chunk.chunk_type as _ {
                IDAT_CHUNK_TYPE => {
                    for _ in 0..chunk.len() {
                        let byte = stream.read_bits(8)?;
                        deflate_data.push(byte as u8);
                    }
                }

                IEND_CHUNK_TYPE => {
                    break;
                }

                _ => {
                    for _ in 0..chunk.len() {
                        stream.read_byte()?;
                    }
                }
            }

            let _crc = stream.read::<Footer>()?;
        }

        let deflated_data = deflate::reverse(deflate_data)?;

        let width = header.width() as usize;
        let height = header.height() as usize;
        let (unfiltered_data, bpp) = filter::unfilter(
            deflated_data,
            height,
            width,
            header.color_type,
            header.depth,
        )?;

        let mut stream = ByteStream::new(unfiltered_data);
        let mut data = Vec::with_capacity(width * height);
        for _ in 0..(height * width) {
            let r = stream.read_byte()? as f32;
            let g = stream.read_byte()? as f32;
            let b = stream.read_byte()? as f32;
            let a = if bpp == 4 {
                stream.read_byte()? as f32
            } else {
                255.
            };

            let color = Color4::new(r / 255., g / 255., b / 255., a / 255.);
            data.push(color)
        }

        Some(Self {
            data,
            width,
            height,
        })
    }
}
