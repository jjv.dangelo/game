use crate::{
    math::Vec3,
    renderer::{Color4, Material},
};

use std::{collections::HashMap, str::FromStr};

#[derive(Clone, Copy)]
pub struct Point {
    pub vertex: usize,
    pub texture: Option<usize>,
    pub normal: Option<usize>,
    pub material: usize,
}

#[derive(Clone, Copy)]
pub struct Triangle(pub Point, pub Point, pub Point);

#[derive(Clone)]
pub struct Model {
    pub filename: String,
    pub name: String,
    pub materials: Vec<Material>,
    pub vertices: Vec<Vec3>,
    pub textures: Vec<Vec3>,
    pub normals: Vec<Vec3>,
    pub faces: Vec<Triangle>,
}

pub fn load_obj<L>(filename: String, data: String, loader: L) -> Option<Model>
where
    L: Fn(&str) -> Option<String>,
{
    let mut name = String::new();
    let mut materials = Vec::new();
    let mut material_map = HashMap::new();
    let mut vertices = Vec::new();
    let mut textures = Vec::new();
    let mut normals = Vec::new();
    let mut faces = Vec::new();
    let mut current_material = 0;

    let lines = data
        .lines()
        .filter(|line| line.len() > 0 && !line.starts_with("#"));

    for line in lines {
        if line.starts_with("vt ") {
            let texture = parse_v3(line.trim_end())?;
            textures.push(texture);
        } else if line.starts_with("vn ") {
            let normal = parse_v3(line.trim_end())?;
            normals.push(normal);
        } else if line.starts_with("v ") {
            let vertice = parse_v4_xyz(line.trim_end())?;
            vertices.push(vertice);
        } else if line.starts_with("f ") {
            parse_triangles(line.trim_end(), current_material, &mut faces)?;
        } else if line.starts_with("g ") {
            parse_name(line.trim_end(), &mut name)?;
        } else if line.starts_with("mtllib ") {
            let mat_filename = line.split(" ").skip(1).next()?;
            let mat_file = loader(mat_filename)?;
            parse_materials(mat_file, &mut materials, &mut material_map)?;
        } else if line.starts_with("usemtl ") {
            let mat_name = line.split(" ").skip(1).next()?;
            current_material = material_map[mat_name];
        }
    }

    Some(Model {
        filename,
        name,
        materials,
        vertices,
        textures,
        normals,
        faces,
    })
}

fn parse_v3(line: &str) -> Option<Vec3> {
    let parts: Vec<&str> = line.split(" ").collect();
    let (u, v, w): (f32, f32, f32) = match &parts[..] {
        [_, u, v, w] => (
            f32::from_str(*u).unwrap(),
            f32::from_str(*v).unwrap(),
            f32::from_str(*w).unwrap(),
        ),

        [_, u, v] => (f32::from_str(*u).unwrap(), f32::from_str(*v).unwrap(), 1.),

        [_, u] => (f32::from_str(*u).unwrap(), 1., 1.),

        _ => {
            return None;
        }
    };

    Some(Vec3::new(u, v, w))
}

fn parse_v4_xyz(line: &str) -> Option<Vec3> {
    let parts: Vec<&str> = line.split(" ").collect();
    let (x, y, z) = match &parts[..] {
        ["v", x, y, z] => (
            f32::from_str(*x).unwrap(),
            f32::from_str(*y).unwrap(),
            f32::from_str(*z).unwrap(),
        ),

        _ => {
            return None;
        }
    };

    Some(Vec3::new(x, y, z))
}

/// Assumes convex polygon.
fn parse_triangles(line: &str, material: usize, current: &mut Vec<Triangle>) -> Option<()> {
    let p: Option<Vec<Point>> = line
        .split(" ")
        .skip(1)
        .map(|part| parse_point(part, material))
        .collect();
    let parts = p?;
    let first = *parts.first()?;

    for part in parts[1..].windows(2) {
        match part {
            &[l, r] => {
                let t = Triangle(first, l, r);
                current.push(t);
            }

            _ => unreachable!(),
        }
    }

    Some(())
}

fn parse_point(point: &str, material: usize) -> Option<Point> {
    let parts: Vec<&str> = point.split("/").collect();
    let (vertex, texture, normal) = match &parts[..] {
        [v, "", n] => (
            usize::from_str(*v).unwrap() - 1,
            None,
            Some(usize::from_str(*n).unwrap() - 1),
        ),

        [v, t, ""] => (
            usize::from_str(*v).unwrap() - 1,
            Some(usize::from_str(*t).unwrap() - 1),
            None,
        ),

        [v, t, n] => (
            usize::from_str(*v).unwrap() - 1,
            Some(usize::from_str(*t).unwrap() - 1),
            Some(usize::from_str(*n).unwrap() - 1),
        ),

        [v, ""] => (usize::from_str(*v).unwrap() - 1, None, None),

        [v, t] => (
            usize::from_str(*v).unwrap() - 1,
            Some(usize::from_str(*t).unwrap() - 1),
            None,
        ),

        [v] => (usize::from_str(*v).unwrap() - 1, None, None),

        _ => {
            return None;
        }
    };

    Some(Point {
        vertex,
        texture,
        normal,
        material,
    })
}

fn parse_name(line: &str, name: &mut String) -> Option<()> {
    *name = line[2..].chars().collect();
    Some(())
}

fn parse_materials(
    data: String,
    materials: &mut Vec<Material>,
    material_map: &mut HashMap<String, usize>,
) -> Option<()> {
    let lines = data
        .lines()
        .filter(|line| line.len() > 0 && !line.starts_with("#"));

    let mut parsing_material = false;
    let mut current_material = Material::default();
    let mut material_name = String::new();
    let mut current_id = materials.len();

    for line in lines {
        if line.starts_with("newmtl ") {
            if parsing_material {
                material_map.insert(material_name.clone(), current_id);
                materials.push(current_material);
                current_id += 1;
            }

            material_name = line[7..].chars().collect();
            current_material = Material::default();
            parsing_material = true;
        } else if line.starts_with("K") {
            let parts: Vec<&str> = line.split(" ").collect();
            let target = if line.starts_with("Ka") {
                &mut current_material.ambient
            } else if line.starts_with("Kd") {
                &mut current_material.diffuse
            } else if line.starts_with("Ks") {
                &mut current_material.specular
            } else {
                continue;
            };

            match &parts[..] {
                [_, r, b, g, a] => {
                    let r = f32::from_str(*r).unwrap();
                    let g = f32::from_str(*g).unwrap();
                    let b = f32::from_str(*b).unwrap();
                    let a = f32::from_str(*a).unwrap();

                    *target = Color4::new(r, g, b, a);
                }

                [_, r, g, b] => {
                    let r = f32::from_str(*r).unwrap();
                    let g = f32::from_str(*g).unwrap();
                    let b = f32::from_str(*b).unwrap();

                    *target = Color4::new(r, g, b, 12.);
                }

                _ => unreachable!(),
            }
        } else if line.starts_with("Ns ") {
            let specular_power_s = line.split(" ").skip(1).next().unwrap();
            let specular_power = f32::from_str(specular_power_s).unwrap();
            current_material.specular.a = specular_power;
        }
    }

    if parsing_material {
        material_map.insert(material_name, current_id);
        materials.push(current_material);
    }

    Some(())
}
