pub mod bvh;

use crate::math::{Matrix4x4, Quaternion, Ray, Vec3, Vec4};

pub type Entity = crate::array::Index;

#[derive(Clone, Copy, Debug)]
pub struct PositionComponent {
    pub pos: Vec3,
    pub rot: Quaternion,
    pub scale: Vec3,
}

impl PositionComponent {
    pub const fn new(pos: Vec3) -> Self {
        Self {
            pos,
            rot: Quaternion::IDENTITY,
            scale: Vec3::ONE,
        }
    }

    #[allow(dead_code)]
    pub const fn with_scale(self, scale: Vec3) -> Self {
        Self { scale, ..self }
    }

    #[allow(dead_code)]
    pub const fn with_rot(self, rot: Quaternion) -> Self {
        Self { rot, ..self }
    }

    pub fn create_transform(&self) -> Matrix4x4 {
        Matrix4x4::affine_transformation(self.scale, Vec3::ZERO, self.rot, self.pos)
    }
}

pub struct Entities(Vec<Entity>);

#[allow(dead_code)] // @TODO: dead_code
impl Entities {
    fn push(&mut self, e: Entity) {
        self.0.push(e);
    }

    fn remove_item(&mut self, e: &Entity) {
        self.0.remove_item(e);
    }

    pub fn iter(&self) -> impl Iterator<Item = &Entity> {
        self.0.iter()
    }
}

#[derive(Clone, Copy)]
pub struct Camera {
    pub pos: Vec3,
    pub up: Vec3,
    pub look_at: Vec3,
    pub near_z: f32,
    pub far_z: f32,
    pub fov: f32,
    pub window_width: u32,
    pub window_height: u32,
}

pub struct CameraRay {
    pub ray: Ray,
    pub origin: Vec3,
    pub dir: Vec3,
}

#[allow(dead_code)] // @TODO: dead_code
impl Camera {
    pub fn new(window_width: u32, window_height: u32) -> Self {
        let rot = Quaternion::new(Vec3::X_AXIS, std::f32::consts::PI / 4.);

        Self {
            pos: rot.rotate_vec(-Vec3::Z_AXIS * 15.),
            look_at: rot.rotate_vec(Vec3::Z_AXIS),
            up: rot.rotate_vec(Vec3::Y_AXIS),
            near_z: 1.,
            far_z: 1000.,
            fov: std::f32::consts::PI / 4.,
            window_width,
            window_height,
        }
    }

    pub fn view_proj(&self) -> Matrix4x4 {
        let aspect_ratio = (self.window_width as f32) / (self.window_height as f32);

        let view = Matrix4x4::view_projection(self.pos, self.up, self.look_at);
        let proj = Matrix4x4::fov_projection(self.fov, aspect_ratio, self.near_z, self.far_z);
        let result = view * proj;
        result
    }

    pub fn proj(&self) -> Matrix4x4 {
        let aspect_ratio = (self.window_width as f32) / (self.window_height as f32);

        Matrix4x4::fov_projection(self.fov, aspect_ratio, self.near_z, self.far_z)
    }

    pub fn view(&self) -> Matrix4x4 {
        Matrix4x4::view_projection(self.pos, self.up, self.look_at)
    }

    pub fn calc_ray(&self, x: f32, y: f32) -> CameraRay {
        let (width, height) = (self.window_width as f32, self.window_height as f32);

        let proj = self.proj();
        let dx = (2. * x / width - 1.) / proj[(0, 0)];
        let dy = (-2. * y / height + 1.) / proj[(1, 1)];
        let inv_view = self.view().inverse();
        let p1 = Vec4::point(0., 0., 0.) * inv_view;
        let p2 = Vec4::point(self.far_z * dx, self.far_z * dy, self.far_z) * inv_view;

        let dir = (p2.to_vec3() - p1.to_vec3()).normalized();
        let origin = p1.to_vec3();
        let ray = Ray::new(origin, dir);
        CameraRay { ray, origin, dir }
    }
}
