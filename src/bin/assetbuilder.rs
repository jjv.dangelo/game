extern crate game;
extern crate winapi;

use winapi::{
    shared::{minwindef::FALSE, windef::HFONT},
    um::wingdi::{
        CreateCompatibleBitmap, CreateCompatibleDC, CreateFontA, GetCharABCWidthsW, GetPixel,
        GetTextMetricsW, PatBlt, SelectObject, SetBkColor, SetTextColor, TextOutW, ABC,
        ANTIALIASED_QUALITY, BLACKNESS, CLIP_DEFAULT_PRECIS, CLR_INVALID, DEFAULT_CHARSET,
        FW_DONTCARE, OUT_OUTLINE_PRECIS, RGB, VARIABLE_PITCH,
    },
};

use std::ptr;

fn create_font(name: &str, width: i32) -> Option<HFONT> {
    let font_name = std::ffi::CString::new(name).unwrap();
    let handle = unsafe {
        CreateFontA(
            width,
            0,
            0,
            0,
            FW_DONTCARE,
            FALSE as _,
            FALSE as _,
            FALSE as _,
            DEFAULT_CHARSET,
            OUT_OUTLINE_PRECIS,
            CLIP_DEFAULT_PRECIS,
            ANTIALIASED_QUALITY,
            VARIABLE_PITCH,
            font_name.as_ptr(),
        )
    };

    if handle.is_null() {
        println!("Font is null");
        None
    } else {
        println!("Created font \"{}\"", name);
        Some(handle)
    }
}

fn main() {
    unsafe {
        let dc = CreateCompatibleDC(ptr::null_mut());
        if dc.is_null() {
            println!("Unable to create compatible DC.");
            return;
        }

        let mut tm = Default::default();
        GetTextMetricsW(dc, &mut tm);

        let width = 32 * tm.tmMaxCharWidth;
        let height = 32 * tm.tmHeight;
        let scanlines = (height + 3) & !3; // DWORD-aligned scanlines.
        println!("Width/Height/Scanlines: {}/{}/{}", width, height, scanlines);

        let bitmap = CreateCompatibleBitmap(dc, width, scanlines);
        if bitmap.is_null() {
            println!("CreateCompatibleBitmap returned null bitmap.");
            return;
        }

        let _old_bitmap = SelectObject(dc, bitmap as *mut _);
        let font = create_font("Verdana", tm.tmMaxCharWidth).unwrap();

        SelectObject(dc, font as *mut _);

        let mut abc = [ABC::default(); 32 * 32];
        if GetCharABCWidthsW(dc, 0, 32 * 32, abc.as_mut_ptr()) == 0 {
            println!("GetCharABCWidthsW returned 0.");
            return;
        }

        if PatBlt(dc, 0, 0, width, scanlines, BLACKNESS) == 0 {
            println!("Unable to blit blackness.");
            return;
        }

        if SetBkColor(dc, RGB(0, 0, 0)) == CLR_INVALID {
            println!("Unable to set bk color.");
            return;
        }

        if SetTextColor(dc, RGB(255, 255, 255)) == CLR_INVALID {
            println!("Unable to set text color.");
            return;
        }

        for y in 0..32 {
            let top = y * tm.tmHeight;
            let mut reduced = 0;

            for x in 0..32 {
                let left = x * tm.tmMaxCharWidth;
                let c = (y * 32 + x) as usize;

                reduced += abc[c as usize].abcA;
                TextOutW(dc, left - reduced, top, &(c as u16), 1);
            }
        }

        let mut data = Vec::with_capacity((height * width) as usize * 4);
        for y in 0..height {
            for x in 0..width {
                let pixel = GetPixel(dc, x, y);

                let gray = (pixel & 0xFF) as u8;
                data.push(gray);
                data.push(gray);
                data.push(gray);
                data.push(gray);
            }
        }

        // @TODO: Cannot get GetDIBits to return a valid image--or we're reading it incorrectly.
        // Either way, we'll go with the slower GetPixel method above.

        /*
        let mut bi = BitmapInfoWrapper::default();
        bi.bmiHeader.biSize = std::mem::size_of::<BITMAPINFOHEADER>() as u32;

        SelectObject(dc, old_bitmap as *mut _);

        if GetDIBits(dc, bitmap, 0, scanlines as u32, ptr::null_mut(), &mut bi.bitmap_info, DIB_RGB_COLORS) == 0 {
            println!("DIBits failed to load bitmap info.");
            return;
        }

        let mut bitmap_data = vec![0u8; bi.bmiHeader.biSizeImage as usize];
        let result =  GetDIBits(dc, bitmap, 0, scanlines as u32, bitmap_data.as_mut_ptr() as *mut _, &mut bi.bitmap_info, DIB_RGB_COLORS);
        if result == 0 {
            println!("DIBits failed to load bitmap data.");
            return;
        }

        println!("Bitmap bit count: {}", bi.bmiHeader.biBitCount);
        println!("Bitmap dimensions: ({}, {})", bi.bmiHeader.biWidth, bi.bmiHeader.biHeight);

        let color_1 = &bi.bmiColors[0];
        let color_2 = &bi.colors[0];
        println!("Bitmap color 1: <{}, {}, {}>", color_1.rgbRed, color_1.rgbBlue, color_1.rgbGreen);
        println!("Bitmap color 1: <{}, {}, {}>", color_2.rgbRed, color_2.rgbBlue, color_2.rgbGreen);

        let mut data = Vec::with_capacity(bi.bmiHeader.biSizeImage as usize);
        println!("Bitmap data len: {}", bitmap_data.len());

        println!("bmiHeader.biHeight: {}", bi.bmiHeader.biHeight);

        // Assumes bit count == 1
        for color in bitmap_data {
            for i in 0..8 {
                let gray = ((color & (i + 1)) >> i) * 0xFF;
                data.push(gray); // r
                data.push(gray); // g
                data.push(gray); // b
                data.push(gray); // a
            }
        }

        data.truncate((width * height) as usize * 4);
        */

        println!("Data is {} bytes", data.len());

        use std::io::Write;
        let mut file = std::fs::File::create("temp.txt").unwrap();
        file.write_all(data.as_slice()).unwrap();
    }
}

/*
struct BitmapInfoWrapper {
    bitmap_info: BITMAPINFO,
    colors: [RGBQUAD; 255],
}

impl std::ops::Deref for BitmapInfoWrapper {
    type Target = BITMAPINFO;

    fn deref(&self) -> &Self::Target {
        &self.bitmap_info
    }
}

impl std::ops::DerefMut for BitmapInfoWrapper {
    fn deref_mut(&mut self) -> &mut BITMAPINFO {
        &mut self.bitmap_info
    }
}

impl Default for BitmapInfoWrapper {
    fn default() -> Self {
        Self {
            bitmap_info: BITMAPINFO::default(),
            colors: [RGBQUAD {
                rgbBlue: 0,
                rgbGreen: 0,
                rgbRed: 0,
                rgbReserved: 0,
            }; 255],
        }
    }
}
*/
