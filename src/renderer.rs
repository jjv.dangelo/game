pub mod color;
pub mod lights;
pub mod model;
pub mod png;

pub use self::color::Color4;

use crate::{
    components::PositionComponent,
    math::{Matrix4x4, Vec2, Vec3},
    renderer::lights::{Directional, Point},
};

pub enum RenderCommand {
    ClearWindow(Color4),
    SetPerspective(Matrix4x4, Vec3),

    PushDirectionalLight(Directional),
    PushPointLight(Point),
    BakeLights,

    PushRect(Color4, Matrix4x4),
    PushModel(String, Matrix4x4),
    PushModelHighlight(String, Color4, Matrix4x4),

    PushUiRect(Color4, Vec2, Vec2),
    PushString(String, Vec2, Color4),
}

pub struct RenderCommands(Vec<RenderCommand>);

#[allow(dead_code)] // @TODO: dead_code
impl RenderCommands {
    pub fn new() -> Self {
        Self::with_capacity(128)
    }

    pub fn with_capacity(capacity: usize) -> Self {
        RenderCommands(Vec::with_capacity(capacity))
    }

    #[inline]
    pub fn push(&mut self, command: RenderCommand) {
        self.0.push(command);
    }

    #[inline]
    pub fn push_ui_rect(&mut self, color: Color4, top_left: Vec2, size: Vec2) {
        self.push(RenderCommand::PushUiRect(color, top_left, size));
    }

    #[inline]
    pub fn push_rect(&mut self, color: Color4, transform: Matrix4x4) {
        self.push(RenderCommand::PushRect(color, transform));
    }

    pub fn push_string(&mut self, s: String, pos: Vec2, color: Color4) {
        self.push(RenderCommand::PushString(s, pos, color));
    }

    pub fn push_rect_outline(&mut self, position: Vec3, height: f32, width: f32, color: Color4) {
        let half_width = 0.02 * 0.5;

        // Left
        let pos_comp = PositionComponent::new(position + Vec3::new(-width + half_width, 0., 0.))
            .with_scale(Vec3::new(half_width, 0., height));
        self.push_rect(color, pos_comp.create_transform());

        // Top
        let pos_comp = PositionComponent::new(position + Vec3::new(0., 0., height - half_width))
            .with_scale(Vec3::new(width, 0., half_width));
        self.push_rect(color, pos_comp.create_transform());

        // Right
        let pos_comp = PositionComponent::new(position + Vec3::new(width - half_width, 0., 0.))
            .with_scale(Vec3::new(half_width, 0., height));
        self.push_rect(color, pos_comp.create_transform());

        // Bottom
        let pos_comp = PositionComponent::new(position + Vec3::new(0., 0., -height + half_width))
            .with_scale(Vec3::new(width, 0., half_width));
        self.push_rect(color, pos_comp.create_transform());
    }

    pub fn push_model(&mut self, model: String, world_transform: Matrix4x4) {
        self.push(RenderCommand::PushModel(model, world_transform));
    }

    pub fn push_model_highlight(
        &mut self,
        model: String,
        color: Color4,
        world_transform: Matrix4x4,
    ) {
        self.push(RenderCommand::PushModelHighlight(
            model,
            color,
            world_transform,
        ));
    }

    pub fn commands(&self) -> &[RenderCommand] {
        self.0.as_slice()
    }

    pub fn reset(&mut self) {
        unsafe { self.0.set_len(0) };
    }
}

#[derive(Clone, Copy, Default)]
pub struct Material {
    pub emissive: Color4,
    pub ambient: Color4,
    pub diffuse: Color4,
    pub specular: Color4,
}

impl Material {
    pub const fn new(ambient: Color4, diffuse: Color4, specular: Color4) -> Self {
        Self {
            emissive: Color4::new(0., 0., 0., 1.),
            ambient,
            diffuse,
            specular,
        }
    }

    #[allow(dead_code)] // @TODO: dead_code
    pub const GREEN: Self = Self::new(
        Color4::new(0.07568, 0.61424, 0.07568, 1.),
        Color4::new(0.07568, 0.61424, 0.07568, 1.),
        Color4::new(0.07568, 0.61424, 0.07568, 76.8),
    );

    #[allow(dead_code)] // @TODO: dead_code
    pub const RED_PLASTIC: Self = Self::new(
        Color4::new(0.1, 0.1, 0.1, 1.),
        Color4::new(0.6, 0.1, 0.1, 1.),
        Color4::new(1.0, 0.2, 0.2, 32.),
    );

    #[allow(dead_code)] // @TODO: dead_code
    pub const PEARL: Self = Self::new(
        Color4::new(0.25, 0.20725, 0.20725, 1.0),
        Color4::new(1.0, 0.829, 0.829, 1.0),
        Color4::new(0.296648, 0.296648, 0.296648, 11.264),
    );
}
