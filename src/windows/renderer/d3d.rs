use crate::windows::{com::ComPtr, win32::to_os_str};

use winapi::um::{
    d3dcommon::ID3DBlob, d3dcompiler::D3DCompileFromFile, debugapi::OutputDebugStringA,
};

use std::ptr;

macro_rules! check_com {
    ($hr:expr, $ptr:expr) => {
        match winapi::shared::winerror::SUCCEEDED($hr) {
            true => crate::windows::com::ComPtr::try_create($ptr),
            false => None,
        }
    };
}

// @TODO: Make `target` a type instead of a &str.
/// Compiles a shader from a file. `target` should be "vs_5_0" or "ps_5_0" for now.
pub fn compile_shader(
    shader_file_name: &str,
    entry_point: &str,
    target: &str,
) -> Option<ComPtr<ID3DBlob>> {
    use std::ffi::CString;

    let mut shader = ptr::null_mut();
    let mut error = ptr::null_mut();
    let shader_file_name = to_os_str(shader_file_name);
    let entry_point = CString::new(entry_point).unwrap();
    let target = CString::new(target).unwrap();
    let compiler_flags = 0;

    let hr = unsafe {
        D3DCompileFromFile(
            shader_file_name.as_ptr() as _, // pFileName
            ptr::null(),                    // pDefines
            ptr::null_mut(),                // pInclude
            entry_point.as_ptr(),           // pEntrypoint
            target.as_ptr(),                // pTarget
            compiler_flags,                 // Flags1
            0,                              // Flags2
            &mut shader,                    // ppCode
            &mut error,                     // ppError
        )
    };

    if !error.is_null() {
        unsafe { OutputDebugStringA((*error).GetBufferPointer() as *const _) };
    }

    check_com!(hr, shader as _)
}
