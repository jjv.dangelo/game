use crate::windows::com::ComPtr;

use winapi::{
    shared::{dxgiformat::DXGI_FORMAT_R32_UINT, winerror::SUCCEEDED},
    um::{
        d3d11::{
            ID3D11Buffer, ID3D11Device, ID3D11DeviceContext, D3D11_BIND_CONSTANT_BUFFER,
            D3D11_BIND_INDEX_BUFFER, D3D11_BIND_VERTEX_BUFFER, D3D11_BUFFER_DESC,
            D3D11_CPU_ACCESS_WRITE, D3D11_MAPPED_SUBRESOURCE, D3D11_MAP_WRITE_DISCARD,
            D3D11_PRIMITIVE, D3D11_SUBRESOURCE_DATA, D3D11_USAGE_DEFAULT, D3D11_USAGE_DYNAMIC,
        },
        d3dcommon::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
    },
};

use std::ptr;

pub struct VertexBuffer {
    vertex_buffer: ComPtr<ID3D11Buffer>,
    index_buffer: ComPtr<ID3D11Buffer>,

    vertex_count: u32,
    index_count: u32,

    vertex_stride: u32,
    topology: D3D11_PRIMITIVE,
}

impl VertexBuffer {
    pub fn new<V>(device: &mut ID3D11Device, vertices: Vec<V>, indices: Vec<u32>) -> Option<Self> {
        let vertex_buffer_desc = D3D11_BUFFER_DESC {
            Usage: D3D11_USAGE_DEFAULT,
            ByteWidth: (std::mem::size_of::<V>() * vertices.len()) as _,
            BindFlags: D3D11_BIND_VERTEX_BUFFER,
            CPUAccessFlags: 0,
            MiscFlags: 0,
            StructureByteStride: 0,
        };

        let vertex_data = D3D11_SUBRESOURCE_DATA {
            pSysMem: vertices.as_ptr() as _,
            SysMemPitch: 0,
            SysMemSlicePitch: 0,
        };

        let mut vertex_buffer = ptr::null_mut();
        let hr =
            unsafe { device.CreateBuffer(&vertex_buffer_desc, &vertex_data, &mut vertex_buffer) };
        let vertex_buffer = check_com!(hr, vertex_buffer)?;

        let index_buffer_desc = D3D11_BUFFER_DESC {
            Usage: D3D11_USAGE_DEFAULT,
            ByteWidth: (std::mem::size_of::<u32>() * indices.len()) as _,
            BindFlags: D3D11_BIND_INDEX_BUFFER,
            CPUAccessFlags: 0,
            MiscFlags: 0,
            StructureByteStride: 0,
        };

        let index_data = D3D11_SUBRESOURCE_DATA {
            pSysMem: indices.as_ptr() as _,
            SysMemPitch: 0,
            SysMemSlicePitch: 0,
        };

        let mut index_buffer = ptr::null_mut();
        let hr = unsafe { device.CreateBuffer(&index_buffer_desc, &index_data, &mut index_buffer) };
        let index_buffer = check_com!(hr, index_buffer)?;

        let vertex_stride = std::mem::size_of::<V>() as u32;

        let vertex_count = vertices.len() as u32;
        debug_assert_eq!(vertex_count as usize, vertices.len());

        let index_count = indices.len() as u32;
        debug_assert_eq!(index_count as usize, indices.len());

        Some(Self {
            vertex_buffer,
            index_buffer,

            vertex_count,
            index_count,

            vertex_stride,
            topology: D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
        })
    }

    pub fn draw(
        &mut self,
        device_context: &mut ID3D11DeviceContext,
        topology: Option<D3D11_PRIMITIVE>,
    ) {
        unsafe {
            let offset = 0;
            device_context.IASetVertexBuffers(
                0,
                1,
                &self.vertex_buffer.as_raw_mut(),
                &self.vertex_stride,
                &offset,
            );

            let topology = topology.unwrap_or(self.topology);
            device_context.IASetPrimitiveTopology(topology);

            device_context.Draw(self.vertex_count, 0);
        }
    }

    pub fn draw_indexed(
        &mut self,
        device_context: &mut ID3D11DeviceContext,
        topology: Option<D3D11_PRIMITIVE>,
    ) {
        unsafe {
            let offset = 0;
            device_context.IASetVertexBuffers(
                0,
                1,
                &self.vertex_buffer.as_raw_mut(),
                &self.vertex_stride,
                &offset,
            );

            let topology = topology.unwrap_or(self.topology);
            device_context.IASetPrimitiveTopology(topology);

            device_context.IASetIndexBuffer(
                self.index_buffer.as_raw_mut(),
                DXGI_FORMAT_R32_UINT,
                0,
            );

            device_context.DrawIndexed(self.index_count, 0, 0);
        }
    }
}

pub struct ConstantBuffer<T> {
    buffer: ComPtr<ID3D11Buffer>,
    phantom: std::marker::PhantomData<T>,
}

impl<T> ConstantBuffer<T> {
    pub fn new(device: &mut ID3D11Device, count: usize) -> Option<Self> {
        let stride = (std::mem::size_of::<T>() + 15) & !15;
        let total_size = stride * count;

        assert_eq!(std::mem::size_of::<T>(), stride);

        let bd = D3D11_BUFFER_DESC {
            Usage: D3D11_USAGE_DYNAMIC,
            ByteWidth: total_size as _,
            BindFlags: D3D11_BIND_CONSTANT_BUFFER,
            CPUAccessFlags: D3D11_CPU_ACCESS_WRITE,
            MiscFlags: 0,
            StructureByteStride: stride as _,
        };

        let mut buffer = ptr::null_mut();
        let hr = unsafe { device.CreateBuffer(&bd, ptr::null(), &mut buffer) };
        let buffer = check_com!(hr, buffer)?;

        let phantom = std::marker::PhantomData;
        Some(Self { buffer, phantom })
    }

    pub fn set(&mut self, device_context: &mut ID3D11DeviceContext, data: T) {
        let mut ms = D3D11_MAPPED_SUBRESOURCE::default();
        let hr = unsafe {
            device_context.Map(
                self.buffer.as_raw_mut() as _,
                0,
                D3D11_MAP_WRITE_DISCARD,
                0,
                &mut ms,
            )
        };

        if SUCCEEDED(hr) {
            unsafe {
                let ptr: *mut T = ms.pData as _;
                *ptr = data;

                device_context.Unmap(self.buffer.as_raw_mut() as _, 0);
            }
        }
    }

    pub fn bind_vs(&mut self, device_context: &mut ID3D11DeviceContext, slot: u32) {
        unsafe {
            device_context.VSSetConstantBuffers(slot, 1, &self.buffer.as_raw_mut());
        }
    }

    pub fn bind_ps(&mut self, device_context: &mut ID3D11DeviceContext, slot: u32) {
        unsafe {
            device_context.PSSetConstantBuffers(slot, 1, &self.buffer.as_raw_mut());
        }
    }
}
