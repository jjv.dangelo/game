use crate::windows::{com::ComPtr, renderer::d3d};
use game::{
    math::{Vec2, Vec3},
    renderer::Color4,
};

use winapi::{
    shared::dxgiformat::{
        DXGI_FORMAT_R32G32B32A32_FLOAT, DXGI_FORMAT_R32G32B32_FLOAT, DXGI_FORMAT_R32G32_FLOAT,
        DXGI_FORMAT_R32_UINT,
    },
    um::d3d11::{
        ID3D11Device, ID3D11DeviceContext, ID3D11InputLayout, ID3D11PixelShader,
        ID3D11VertexShader, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_ELEMENT_DESC,
        D3D11_INPUT_PER_VERTEX_DATA,
    },
};

use std::{ffi::CString, ptr};

#[derive(Copy, Clone, PartialEq)]
pub enum Shader {
    NotBound,
    Color,
    Font,
    Highlighter,
    Advanced,
}

pub struct ShaderProgram {
    vertex_shader: ComPtr<ID3D11VertexShader>,
    pixel_shader: ComPtr<ID3D11PixelShader>,
    input_layout: ComPtr<ID3D11InputLayout>,
}

impl ShaderProgram {
    pub fn create_advanced_shader(device: &mut ComPtr<ID3D11Device>) -> Option<Self> {
        let vertex_shader_blob = d3d::compile_shader("shaders.hlsl", "VShader", "vs_5_0")?;
        let vertex_shader = {
            let mut vertex_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreateVertexShader(
                    vertex_shader_blob.GetBufferPointer(),
                    vertex_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut vertex_shader,
                )
            };
            check_com!(hr, vertex_shader)?
        };

        let pixel_shader_blob = d3d::compile_shader("shaders.hlsl", "PShader", "ps_5_0")?;
        let pixel_shader = {
            let mut pixel_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreatePixelShader(
                    pixel_shader_blob.GetBufferPointer(),
                    pixel_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut pixel_shader,
                )
            };
            check_com!(hr, pixel_shader)?
        };

        let position_name = CString::new("POSITION").unwrap();
        let norm_name = CString::new("NORMAL").unwrap();
        let tex_name = CString::new("TEXCOORD").unwrap();
        let material_name = CString::new("MATERIAL").unwrap();

        let input_element_desc = [
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: position_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: 0,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: norm_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: tex_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: material_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32_UINT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
        ];

        let mut input_layout = ptr::null_mut();
        let hr = unsafe {
            device.CreateInputLayout(
                input_element_desc.as_ptr(),
                input_element_desc.len() as _,
                vertex_shader_blob.GetBufferPointer(),
                vertex_shader_blob.GetBufferSize(),
                &mut input_layout,
            )
        };
        let input_layout = check_com!(hr, input_layout)?;

        Some(Self {
            input_layout,
            vertex_shader,
            pixel_shader,
        })
    }

    pub fn create_highlight_shader(device: &mut ComPtr<ID3D11Device>) -> Option<Self> {
        let vertex_shader_blob = d3d::compile_shader("highlight_shader.hlsl", "VShader", "vs_5_0")?;
        let vertex_shader = {
            let mut vertex_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreateVertexShader(
                    vertex_shader_blob.GetBufferPointer(),
                    vertex_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut vertex_shader,
                )
            };
            check_com!(hr, vertex_shader)?
        };

        let pixel_shader_blob = d3d::compile_shader("highlight_shader.hlsl", "PShader", "ps_5_0")?;
        let pixel_shader = {
            let mut pixel_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreatePixelShader(
                    pixel_shader_blob.GetBufferPointer(),
                    pixel_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut pixel_shader,
                )
            };
            check_com!(hr, pixel_shader)?
        };

        let position_name = CString::new("POSITION").unwrap();
        let color_name = CString::new("COLOR").unwrap();

        let input_element_desc = [
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: position_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: 0,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: color_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32A32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
        ];

        let mut input_layout = ptr::null_mut();
        let hr = unsafe {
            device.CreateInputLayout(
                input_element_desc.as_ptr(),
                input_element_desc.len() as _,
                vertex_shader_blob.GetBufferPointer(),
                vertex_shader_blob.GetBufferSize(),
                &mut input_layout,
            )
        };
        let input_layout = check_com!(hr, input_layout)?;

        Some(Self {
            input_layout,
            vertex_shader,
            pixel_shader,
        })
    }

    pub fn create_color_shader(device: &mut ComPtr<ID3D11Device>) -> Option<Self> {
        let vertex_shader_blob = d3d::compile_shader("color_shader.hlsl", "VShader", "vs_5_0")?;
        let vertex_shader = {
            let mut vertex_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreateVertexShader(
                    vertex_shader_blob.GetBufferPointer(),
                    vertex_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut vertex_shader,
                )
            };
            check_com!(hr, vertex_shader)?
        };

        let pixel_shader_blob = d3d::compile_shader("color_shader.hlsl", "PShader", "ps_5_0")?;
        let pixel_shader = {
            let mut pixel_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreatePixelShader(
                    pixel_shader_blob.GetBufferPointer(),
                    pixel_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut pixel_shader,
                )
            };
            check_com!(hr, pixel_shader)?
        };

        let position_name = CString::new("POSITION").unwrap();
        let color_name = CString::new("COLOR").unwrap();

        let input_element_desc = [
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: position_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: 0,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: color_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32A32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
        ];

        let mut input_layout = ptr::null_mut();
        let hr = unsafe {
            device.CreateInputLayout(
                input_element_desc.as_ptr(),
                input_element_desc.len() as _,
                vertex_shader_blob.GetBufferPointer(),
                vertex_shader_blob.GetBufferSize(),
                &mut input_layout,
            )
        };
        let input_layout = check_com!(hr, input_layout)?;

        Some(Self {
            input_layout,
            vertex_shader,
            pixel_shader,
        })
    }

    pub fn create_font_shader(device: &mut ComPtr<ID3D11Device>) -> Option<Self> {
        let vertex_shader_blob = d3d::compile_shader("font_shader.hlsl", "VShader", "vs_5_0")?;
        let vertex_shader = {
            let mut vertex_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreateVertexShader(
                    vertex_shader_blob.GetBufferPointer(),
                    vertex_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut vertex_shader,
                )
            };
            check_com!(hr, vertex_shader)?
        };

        let pixel_shader_blob = d3d::compile_shader("font_shader.hlsl", "PShader", "ps_5_0")?;
        let pixel_shader = {
            let mut pixel_shader = ptr::null_mut();
            let hr = unsafe {
                device.CreatePixelShader(
                    pixel_shader_blob.GetBufferPointer(),
                    pixel_shader_blob.GetBufferSize(),
                    ptr::null_mut(),
                    &mut pixel_shader,
                )
            };
            check_com!(hr, pixel_shader)?
        };

        let position_name = CString::new("POSITION").unwrap();
        let color_name = CString::new("COLOR").unwrap();
        let uv_name = CString::new("TEX_COORD").unwrap();

        let input_element_desc = [
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: position_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: 0,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: color_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32B32A32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
            D3D11_INPUT_ELEMENT_DESC {
                SemanticName: uv_name.as_ptr(),
                SemanticIndex: 0,
                Format: DXGI_FORMAT_R32G32_FLOAT,
                InputSlot: 0,
                AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
                InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
                InstanceDataStepRate: 0,
            },
        ];

        let mut input_layout = ptr::null_mut();
        let hr = unsafe {
            device.CreateInputLayout(
                input_element_desc.as_ptr(),
                input_element_desc.len() as _,
                vertex_shader_blob.GetBufferPointer(),
                vertex_shader_blob.GetBufferSize(),
                &mut input_layout,
            )
        };
        let input_layout = check_com!(hr, input_layout)?;

        Some(Self {
            input_layout,
            vertex_shader,
            pixel_shader,
        })
    }

    pub fn activate(&mut self, device_context: &mut ComPtr<ID3D11DeviceContext>) {
        unsafe {
            device_context.IASetInputLayout(self.input_layout.as_raw_mut());
            device_context.VSSetShader(self.vertex_shader.as_raw_mut(), ptr::null(), 0);
            device_context.PSSetShader(self.pixel_shader.as_raw_mut(), ptr::null(), 0);
        }
    }
}

#[repr(C)]
#[derive(Debug)]
pub struct Vertex {
    pos: Vec3,
    norm: Vec3,
    uv: Vec2,
    material: u32,
}

impl Vertex {
    pub const fn new(pos: Vec3, norm: Vec3, u: f32, v: f32, material: u32) -> Self {
        Self {
            pos,
            norm,
            uv: Vec2::new(u, v),
            material,
        }
    }
}

#[repr(C)]
pub struct ColorVertex {
    pos: Vec3,
    color: Color4,
}

impl ColorVertex {
    pub fn new(pos: Vec3, color: Color4) -> Self {
        let pre_mul_alpha_color = Color4::new(
            color.r * color.a,
            color.g * color.a,
            color.b * color.a,
            color.a,
        );

        Self {
            pos,
            color: pre_mul_alpha_color,
        }
    }
}

#[repr(C)]
pub struct FontVertex {
    pos: Vec3,
    color: Color4,
    uv: Vec2,
}

impl FontVertex {
    pub const fn new(pos: Vec3, color: Color4, uv: Vec2) -> Self {
        Self { pos, color, uv }
    }
}
