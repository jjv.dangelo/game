use crate::windows::com::ComPtr;
use game::renderer::{png::Png, Color4};

use winapi::{
    shared::{dxgiformat::DXGI_FORMAT_R32G32B32A32_FLOAT, dxgitype::DXGI_SAMPLE_DESC},
    um::{
        d3d11::{
            ID3D11Device, ID3D11ShaderResourceView, D3D11_BIND_SHADER_RESOURCE,
            D3D11_SHADER_RESOURCE_VIEW_DESC, D3D11_SUBRESOURCE_DATA, D3D11_TEXTURE2D_DESC,
            D3D11_USAGE_DEFAULT,
        },
        d3dcommon::D3D11_SRV_DIMENSION_TEXTURE2D,
    },
};

use std::ptr;

pub struct Texture {
    pub view: ComPtr<ID3D11ShaderResourceView>,
}

impl Texture {
    pub fn new(
        device: &mut ID3D11Device,
        data: Vec<Color4>,
        width: usize,
        height: usize,
    ) -> Option<Self> {
        let bpp = std::mem::size_of::<Color4>();

        let desc = D3D11_TEXTURE2D_DESC {
            Width: width as _,
            Height: height as _,
            MipLevels: 1,
            ArraySize: 1,
            Format: DXGI_FORMAT_R32G32B32A32_FLOAT,
            SampleDesc: DXGI_SAMPLE_DESC {
                Count: 1,
                Quality: 0,
            },
            Usage: D3D11_USAGE_DEFAULT,
            BindFlags: D3D11_BIND_SHADER_RESOURCE,
            CPUAccessFlags: 0,
            MiscFlags: 0,
        };

        let data_desc = D3D11_SUBRESOURCE_DATA {
            pSysMem: data.as_ptr() as _,
            SysMemPitch: (bpp * width) as _,
            SysMemSlicePitch: (bpp * width * height) as _,
        };

        let mut texture = ptr::null_mut();
        let hr = unsafe { device.CreateTexture2D(&desc, &data_desc, &mut texture) };
        let mut texture = check_com!(hr, texture)?;

        let mut resource_desc = D3D11_SHADER_RESOURCE_VIEW_DESC {
            Format: desc.Format,
            ViewDimension: D3D11_SRV_DIMENSION_TEXTURE2D,
            u: Default::default(),
        };

        unsafe {
            let tex2d = &mut resource_desc.u.Texture2D_mut();
            tex2d.MostDetailedMip = 0;
            tex2d.MipLevels = 1;
        }

        let mut view = ptr::null_mut();
        let hr = unsafe {
            device.CreateShaderResourceView(texture.as_raw_mut() as _, &resource_desc, &mut view)
        };
        let view = check_com!(hr, view)?;

        Some(Self { view })
    }

    #[allow(dead_code)]
    pub fn from_png(device: &mut ID3D11Device, png: Png) -> Option<Self> {
        Self::new(device, png.data, png.width, png.height)
    }

    pub fn init_solid(device: &mut ID3D11Device, color: Color4) -> Option<Self> {
        let width = 32;
        let height = 32;
        let wh = width * height;

        let data: Vec<Color4> = std::iter::repeat(color).take(wh).collect();

        Self::new(device, data, width, height)
    }
}

#[allow(dead_code)]
pub fn load_png(target: &str) -> Option<Png> {
    use std::{fs::File, io::Read, path::PathBuf};
    let path: PathBuf = ["assets", target].iter().collect();
    if let Ok(mut file) = File::open(path.as_path()) {
        let mut contents = Vec::new();
        if let Ok(len) = file.read_to_end(&mut contents) {
            assert!(len > 0);

            let bytes = Png::from_bytes(contents);
            bytes
        } else {
            None
        }
    } else {
        None
    }
}
