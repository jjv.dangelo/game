mod buffers;
mod shaders;
mod texture;

use crate::windows::com::ComPtr;

use game::{
    assets::AssetManager,
    math::{Matrix4x4, Vec2, Vec3},
    renderer::{color, Color4, Material, RenderCommand, RenderCommands},
};

use self::{
    buffers::{ConstantBuffer, VertexBuffer},
    shaders::{ColorVertex, FontVertex, Shader, ShaderProgram, Vertex},
};

use winapi::{
    shared::{
        dxgi::{
            IDXGIAdapter, IDXGIDevice, IDXGIFactory, IDXGISwapChain, DXGI_SWAP_CHAIN_DESC,
            DXGI_SWAP_EFFECT_DISCARD,
        },
        dxgiformat::{DXGI_FORMAT_D24_UNORM_S8_UINT, DXGI_FORMAT_R8G8B8A8_UNORM},
        dxgitype::{
            DXGI_MODE_DESC, DXGI_MODE_SCALING_UNSPECIFIED, DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
            DXGI_RATIONAL, DXGI_SAMPLE_DESC, DXGI_USAGE_RENDER_TARGET_OUTPUT,
        },
        minwindef::TRUE,
        windef::HWND,
    },
    um::{
        d3d11::{
            D3D11CreateDevice, ID3D11BlendState, ID3D11DepthStencilView, ID3D11Device,
            ID3D11DeviceContext, ID3D11RenderTargetView, ID3D11SamplerState, ID3D11Texture2D,
            D3D11_BIND_DEPTH_STENCIL, D3D11_BLEND_DESC, D3D11_BLEND_INV_SRC_ALPHA, D3D11_BLEND_ONE,
            D3D11_BLEND_OP_ADD, D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_ZERO, D3D11_CLEAR_DEPTH,
            D3D11_CLEAR_STENCIL, D3D11_COLOR_WRITE_ENABLE_ALL, D3D11_COMPARISON_ALWAYS,
            D3D11_CREATE_DEVICE_DEBUG, D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_SAMPLER_DESC,
            D3D11_SDK_VERSION, D3D11_TEXTURE2D_DESC, D3D11_TEXTURE_ADDRESS_CLAMP,
            D3D11_USAGE_DEFAULT, D3D11_VIEWPORT,
        },
        d3dcommon::{
            D3D11_PRIMITIVE_TOPOLOGY_LINELIST, D3D_DRIVER_TYPE_HARDWARE, D3D_FEATURE_LEVEL_11_0,
        },
    },
    Interface,
};

use std::{collections::HashMap, ptr};

pub struct Renderer {
    device: ComPtr<ID3D11Device>,
    device_context: ComPtr<ID3D11DeviceContext>,
    swapchain: ComPtr<IDXGISwapChain>,
    // depth_stencil_buffer: ComPtr<ID3D11Texture2D>,
    render_target_view: ComPtr<ID3D11RenderTargetView>,
    depth_stencil_view: ComPtr<ID3D11DepthStencilView>,
    blend_state: ComPtr<ID3D11BlendState>,
    viewport: D3D11_VIEWPORT,

    advanced_shader: ShaderProgram,
    color_shader: ShaderProgram,
    highlight_shader: ShaderProgram,
    font_shader: ShaderProgram,
    bound_shader: Shader,

    per_frame: ConstantBuffer<PerFrame>,
    per_obj: ConstantBuffer<PerObj>,
    lights: ConstantBuffer<Lights>,
    materials: ConstantBuffer<[ShaderMaterial; 16]>,

    white_text: texture::Texture,
    font_text: texture::Texture,
    sampler_state: ComPtr<ID3D11SamplerState>,
    models: HashMap<String, VertexBuffer>,
}

impl Renderer {
    pub fn initialize(hwnd: HWND, width: u32, height: u32) -> Option<Self> {
        let mut feature_level = Default::default();
        let mut device = ptr::null_mut();
        let mut device_context = ptr::null_mut();
        let hr = unsafe {
            D3D11CreateDevice(
                ptr::null_mut(),
                D3D_DRIVER_TYPE_HARDWARE,
                ptr::null_mut(),
                D3D11_CREATE_DEVICE_DEBUG,
                ptr::null(),
                0,
                D3D11_SDK_VERSION,
                &mut device,
                &mut feature_level,
                &mut device_context,
            )
        };

        let mut device = check_com!(hr, device as *mut ID3D11Device)?;
        let device_context = check_com!(hr, device_context)?;

        if feature_level != D3D_FEATURE_LEVEL_11_0 {
            return None;
        }

        // Create swapchain
        let swapchain = {
            let mut sd = DXGI_SWAP_CHAIN_DESC {
                BufferDesc: DXGI_MODE_DESC {
                    Width: width,
                    Height: height,
                    RefreshRate: DXGI_RATIONAL {
                        Numerator: 60,
                        Denominator: 1,
                    },
                    Format: DXGI_FORMAT_R8G8B8A8_UNORM,
                    ScanlineOrdering: DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
                    Scaling: DXGI_MODE_SCALING_UNSPECIFIED,
                },
                SampleDesc: DXGI_SAMPLE_DESC {
                    Count: 1,
                    Quality: 0,
                },
                BufferUsage: DXGI_USAGE_RENDER_TARGET_OUTPUT,
                BufferCount: 1,
                OutputWindow: hwnd,
                Windowed: TRUE,
                SwapEffect: DXGI_SWAP_EFFECT_DISCARD,
                Flags: 0,
            };

            let dxgi_device = device.cast::<IDXGIDevice>()?;
            let mut dxgi_adapter = ptr::null_mut();
            let hr = unsafe { dxgi_device.GetParent(&IDXGIAdapter::uuidof(), &mut dxgi_adapter) };
            let dxgi_adapter = check_com!(hr, dxgi_adapter as *mut IDXGIAdapter)?;

            let mut dxgi_factory = ptr::null_mut();
            let hr = unsafe { dxgi_adapter.GetParent(&IDXGIFactory::uuidof(), &mut dxgi_factory) };
            let dxgi_factory = check_com!(hr, dxgi_factory as *mut IDXGIFactory)?;

            let mut swapchain = ptr::null_mut();
            let hr = unsafe {
                dxgi_factory.CreateSwapChain(device.as_unknown_mut(), &mut sd, &mut swapchain)
            };

            check_com!(hr, swapchain as *mut IDXGISwapChain)?
        };

        let mut render_target_view = {
            let mut back_buffer = ptr::null_mut();
            let hr =
                unsafe { swapchain.GetBuffer(0, &ID3D11Texture2D::uuidof(), &mut back_buffer) };
            let mut back_buffer = check_com!(hr, back_buffer as *mut ID3D11Texture2D)?;

            let mut render_target_view = ptr::null_mut();
            let hr = unsafe {
                device.CreateRenderTargetView(
                    back_buffer.as_raw_mut() as _,
                    ptr::null(),
                    &mut render_target_view,
                )
            };
            check_com!(hr, render_target_view as *mut ID3D11RenderTargetView)?
        };

        let depth_stencil_desc = D3D11_TEXTURE2D_DESC {
            Width: width,
            Height: height,
            MipLevels: 1,
            ArraySize: 1,
            Format: DXGI_FORMAT_D24_UNORM_S8_UINT,
            SampleDesc: DXGI_SAMPLE_DESC {
                Count: 1,
                Quality: 0,
            },
            Usage: D3D11_USAGE_DEFAULT,
            BindFlags: D3D11_BIND_DEPTH_STENCIL,
            CPUAccessFlags: 0,
            MiscFlags: 0,
        };
        let mut depth_stencil_buffer = ptr::null_mut();
        let hr = unsafe {
            device.CreateTexture2D(&depth_stencil_desc, ptr::null(), &mut depth_stencil_buffer)
        };
        let mut depth_stencil_buffer =
            check_com!(hr, depth_stencil_buffer as *mut ID3D11Texture2D)?;

        let mut depth_stencil_view = ptr::null_mut();
        let hr = unsafe {
            device.CreateDepthStencilView(
                depth_stencil_buffer.as_raw_mut() as _,
                ptr::null(),
                &mut depth_stencil_view,
            )
        };
        let mut depth_stencil_view =
            check_com!(hr, depth_stencil_view as *mut ID3D11DepthStencilView)?;

        unsafe {
            device_context.OMSetRenderTargets(
                1,
                &mut render_target_view.as_raw_mut(),
                depth_stencil_view.as_raw_mut(),
            )
        };

        let mut blend_desc = D3D11_BLEND_DESC::default();
        let render_target = &mut blend_desc.RenderTarget[0];
        render_target.BlendEnable = TRUE;
        render_target.SrcBlend = D3D11_BLEND_SRC_ALPHA;
        render_target.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
        render_target.BlendOp = D3D11_BLEND_OP_ADD;
        render_target.SrcBlendAlpha = D3D11_BLEND_ONE;
        render_target.DestBlendAlpha = D3D11_BLEND_ZERO;
        render_target.BlendOpAlpha = D3D11_BLEND_OP_ADD;
        render_target.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL as _;

        let mut blend_state = ptr::null_mut();
        let hr = unsafe { device.CreateBlendState(&blend_desc, &mut blend_state) };
        let blend_state = check_com!(hr, blend_state)?;

        let viewport = D3D11_VIEWPORT {
            TopLeftX: 0.,
            TopLeftY: 0.,
            Width: width as f32,
            Height: height as f32,
            MinDepth: 0.,
            MaxDepth: 1.,
        };

        let per_frame = ConstantBuffer::new(&mut device, 4096)?;
        let per_obj = ConstantBuffer::new(&mut device, 4096)?;
        let lights = ConstantBuffer::new(&mut device, 1)?;
        let materials = ConstantBuffer::new(&mut device, 16)?;

        let white_text = texture::Texture::init_solid(&mut device, Color4::new(1., 1., 1., 1.))?;

        let font_bytes = std::fs::read("temp.txt").unwrap();
        let mut font_image = Vec::new();

        for color in font_bytes.as_slice().chunks(4) {
            match color {
                &[r, g, b, a] => {
                    let alpha = a as f32 / 256.;
                    font_image.push(Color4::new(
                        r as f32 / 256. * alpha,
                        g as f32 / 256. * alpha,
                        b as f32 / 256. * alpha,
                        alpha,
                    ));
                }

                _ => {}
            }
        }
        let font_text = texture::Texture::new(&mut device, font_image, 448, 512)?;

        let sampler_desc = D3D11_SAMPLER_DESC {
            Filter: D3D11_FILTER_MIN_MAG_MIP_LINEAR,
            AddressU: D3D11_TEXTURE_ADDRESS_CLAMP,
            AddressV: D3D11_TEXTURE_ADDRESS_CLAMP,
            AddressW: D3D11_TEXTURE_ADDRESS_CLAMP,
            MipLODBias: 0.,
            MaxAnisotropy: 1,
            ComparisonFunc: D3D11_COMPARISON_ALWAYS,
            BorderColor: [0., 0., 0., 0.],
            MinLOD: 0.,
            MaxLOD: std::f32::MAX,
        };

        let mut sampler_state = ptr::null_mut();
        let hr = unsafe { device.CreateSamplerState(&sampler_desc, &mut sampler_state) };
        let sampler_state = check_com!(hr, sampler_state)?;

        let advanced_shader = ShaderProgram::create_advanced_shader(&mut device)?;
        let color_shader = ShaderProgram::create_color_shader(&mut device)?;
        let highlight_shader = ShaderProgram::create_highlight_shader(&mut device)?;
        let font_shader = ShaderProgram::create_font_shader(&mut device)?;

        Some(Self {
            device,
            device_context,
            swapchain,
            // depth_stencil_buffer,
            render_target_view,
            depth_stencil_view,
            blend_state,
            viewport,

            advanced_shader,
            color_shader,
            highlight_shader,
            font_shader,
            bound_shader: Shader::NotBound,

            per_frame,
            per_obj,
            lights,
            materials,

            white_text,
            font_text,
            sampler_state,
            models: HashMap::new(),
        })
    }

    // @TODO: This doesn't seem to work well.
    pub fn resize_window(&mut self, width: u32, height: u32) -> Option<()> {
        // unsafe {
        //     self.render_target_view.Release();
        //     self.device_context.OMSetRenderTargets(0, ptr::null_mut(), ptr::null_mut());

        //     let hr = self.swapchain.ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
        //     if !SUCCEEDED(hr) { return None; }

        //     let mut buffer = ptr::null_mut();
        //     let hr = self.swapchain.GetBuffer(0, &ID3D11Texture2D::uuidof(), &mut buffer);
        //     let mut buffer = check_com!(hr, buffer as *mut ID3D11Texture2D)?;

        //     let mut render_target_view = ptr::null_mut();
        //     let hr = self.device.CreateRenderTargetView(buffer.as_raw_mut() as _, ptr::null(), &mut render_target_view);
        //     self.render_target_view = check_com!(hr, render_target_view as _)?;
        // }

        self.viewport.Width = width as f32;
        self.viewport.Height = height as f32;

        Some(())
    }

    fn bind_shader(&mut self, target: Shader) {
        if target != self.bound_shader {
            let shader = match target {
                Shader::Color => &mut self.color_shader,
                Shader::Font => &mut self.font_shader,
                Shader::Highlighter => &mut self.highlight_shader,
                Shader::Advanced => &mut self.advanced_shader,
                Shader::NotBound => {
                    // @TODO: Should this be an error instead?
                    self.bound_shader = target;
                    return;
                }
            };
            let device_context = &mut self.device_context;

            shader.activate(device_context);

            unsafe {
                self.device_context.RSSetViewports(1, &self.viewport);

                self.device_context
                    .PSSetSamplers(0, 1, &self.sampler_state.as_raw_mut());

                self.device_context.OMSetBlendState(
                    self.blend_state.as_raw_mut(),
                    &[1., 1., 1., 1.],
                    0xFFFFFFFF,
                );
            }

            self.bound_shader = target;
        }
    }

    pub fn process_commands(
        &mut self,
        tick_info: game::clock::TickInfo,
        commands: &mut RenderCommands,
        asset_manager: &mut AssetManager,
    ) -> Option<()> {
        let mut directional_count = 0;
        let mut point_count = 0;
        let mut lights = Lights::default();
        let mut per_frame = PerFrame::default();

        for command in commands.commands() {
            match command {
                RenderCommand::PushDirectionalLight(light) => {
                    lights.directional[directional_count] = ShaderDirectional::new(*light);
                    directional_count = (directional_count + 1) % 8;
                }

                RenderCommand::PushPointLight(light) => {
                    lights.point[point_count] = ShaderPoint::new(*light);
                    point_count = (point_count + 1) % 8;
                }

                RenderCommand::BakeLights => {
                    self.lights.set(&mut self.device_context, lights);
                }

                RenderCommand::ClearWindow(color) => unsafe {
                    self.bind_shader(Shader::Color);

                    self.device_context.ClearRenderTargetView(
                        self.render_target_view.as_raw_mut(),
                        &[color.r, color.g, color.b, color.a],
                    );

                    self.device_context.ClearDepthStencilView(
                        self.depth_stencil_view.as_raw_mut(),
                        D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
                        1.,
                        0,
                    );
                },

                RenderCommand::SetPerspective(view_proj, eye_pos) => {
                    per_frame = PerFrame {
                        view_proj: view_proj.transpose(),
                        eye_pos: *eye_pos,
                        elapsed: tick_info.t as f32,
                        dt: tick_info.dt,
                        padding: [0; 3],
                    };
                    self.per_frame.set(&mut self.device_context, per_frame);
                }

                RenderCommand::PushModelHighlight(model_name, color, world) => {
                    asset_manager
                        .models
                        .get_asset(model_name.as_str(), |model| {
                            self.bind_shader(Shader::Highlighter);

                            let model_vertices = &model.vertices;
                            let model_triangles = &model.faces;

                            let mut vertices = Vec::with_capacity(model_triangles.len() * 3);
                            for tri in model_triangles.iter() {
                                for p in [&tri.0, &tri.1, &tri.1, &tri.2, &tri.2, &tri.0].iter() {
                                    let vert = model_vertices[p.vertex];

                                    vertices.push(ColorVertex::new(vert, color.pre_mul_alpha()));
                                }
                            }

                            let indices = vec![0, 1, 2];
                            let mut buffer =
                                VertexBuffer::new(&mut self.device, vertices, indices).unwrap();

                            let mut materials = [ShaderMaterial::default(); 16];
                            for (i, material) in model.materials.iter().take(16).enumerate() {
                                materials[i] = ShaderMaterial::from(*material);
                            }

                            self.materials.set(&mut self.device_context, materials);

                            let per_obj = PerObj::new(*world);
                            self.per_obj.set(&mut self.device_context, per_obj);

                            unsafe {
                                self.device_context.PSSetShaderResources(
                                    0,
                                    1,
                                    &self.white_text.view.as_raw_mut(),
                                );
                            }

                            let topology = Some(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
                            buffer.draw(&mut self.device_context, topology);
                        });
                }

                RenderCommand::PushModel(model_name, world) => {
                    asset_manager
                        .models
                        .get_asset(model_name.as_str(), |model| {
                            self.bind_shader(Shader::Advanced);

                            let models = &mut self.models;
                            let device = &mut self.device;

                            let buffer =
                                models.entry(model.filename.clone()).or_insert_with(|| {
                                    let model_vertices = &model.vertices;
                                    let model_normals = &model.normals;
                                    let model_triangles = &model.faces;

                                    let mut vertices =
                                        Vec::with_capacity(model_triangles.len() * 3);
                                    for tri in model_triangles.iter() {
                                        for &p in [&tri.0, &tri.1, &tri.2].iter() {
                                            let normal = if let Some(normal) = p.normal {
                                                model_normals[normal]
                                            } else {
                                                Vec3::Y_AXIS
                                            };

                                            let vert = model_vertices[p.vertex];

                                            vertices.push(Vertex::new(
                                                vert,
                                                normal,
                                                0.,
                                                1.,
                                                p.material as u32,
                                            ));
                                        }
                                    }

                                    let indices = vec![0, 1, 2];
                                    let buffer =
                                        VertexBuffer::new(device, vertices, indices).unwrap();
                                    buffer
                                });

                            let mut materials = [ShaderMaterial::default(); 16];
                            if model.materials.len() > 0 {
                                for (i, material) in model.materials.iter().take(16).enumerate() {
                                    materials[i] = ShaderMaterial::from(*material);
                                }
                            } else {
                                materials[0] = ShaderMaterial::from(Material::PEARL);
                            }

                            self.materials.set(&mut self.device_context, materials);

                            let per_obj = PerObj::new(*world);
                            self.per_obj.set(&mut self.device_context, per_obj);

                            unsafe {
                                self.device_context.PSSetShaderResources(
                                    0,
                                    1,
                                    &self.white_text.view.as_raw_mut(),
                                );
                            }

                            buffer.draw(&mut self.device_context, None);
                        });
                }

                RenderCommand::PushRect(color, world) => {
                    self.bind_shader(Shader::Color);

                    let vertices = vec![
                        ColorVertex::new(Vec3::new(-1., 0., 1.), color.pre_mul_alpha()),
                        ColorVertex::new(Vec3::new(1., 0., 1.), color.pre_mul_alpha()),
                        ColorVertex::new(Vec3::new(1., 0., -1.), color.pre_mul_alpha()),
                        ColorVertex::new(Vec3::new(-1., 0., -1.), color.pre_mul_alpha()),
                    ];

                    let indices = vec![0, 1, 2, 2, 3, 0];

                    let mut buffer =
                        VertexBuffer::new(&mut self.device, vertices, indices).unwrap();

                    self.per_obj
                        .set(&mut self.device_context, PerObj::new(*world));

                    unsafe {
                        self.device_context.PSSetShaderResources(
                            0,
                            1,
                            &self.font_text.view.as_raw_mut(),
                        );
                    }

                    buffer.draw_indexed(&mut self.device_context, None);
                }

                RenderCommand::PushUiRect(color, pos, size) => {
                    self.bind_shader(Shader::Color);
                    // @TODO: Create a different shader so we don't have to do this...
                    let yuck = PerFrame {
                        view_proj: Matrix4x4::IDENTITY,
                        eye_pos: Vec3::ZERO,
                        elapsed: tick_info.t as f32,
                        dt: tick_info.dt,
                        padding: [0; 3],
                    };
                    self.per_frame.set(&mut self.device_context, yuck);

                    let (width, height) = (self.viewport.Width as f32, self.viewport.Height as f32);
                    let (left_x, left_y) =
                        (pos.x / width * 2. - 1., (1. - (pos.y / height)) * 2. - 1.);
                    let (right_x, right_y) = (
                        (pos.x + size.x) / width * 2. - 1.,
                        (1. - (pos.y + size.y) / height) * 2. - 1.,
                    );

                    let pre_mul_color = color.pre_mul_alpha();
                    let vertices = vec![
                        ColorVertex::new(Vec3::new(left_x, left_y, 0.), pre_mul_color),
                        ColorVertex::new(Vec3::new(right_x, left_y, 0.), pre_mul_color),
                        ColorVertex::new(Vec3::new(right_x, right_y, 0.), pre_mul_color),
                        ColorVertex::new(Vec3::new(left_x, right_y, 0.), pre_mul_color),
                    ];

                    let indices = vec![0, 1, 2, 2, 3, 0];

                    let mut buffer =
                        VertexBuffer::new(&mut self.device, vertices, indices).unwrap();

                    self.per_obj
                        .set(&mut self.device_context, PerObj::new(Matrix4x4::IDENTITY));

                    buffer.draw_indexed(&mut self.device_context, None);
                    self.per_frame.set(&mut self.device_context, per_frame);
                }

                RenderCommand::PushString(_s, pos, _color) => {
                    self.bind_shader(Shader::Font);
                    // @TODO: Create a different shader so we don't have to do this...
                    let yuck = PerFrame {
                        view_proj: Matrix4x4::IDENTITY,
                        eye_pos: Vec3::ZERO,
                        elapsed: tick_info.t as f32,
                        dt: tick_info.dt,
                        padding: [0; 3],
                    };
                    self.per_frame.set(&mut self.device_context, yuck);

                    let (width, height) = (self.viewport.Width as f32, self.viewport.Height as f32);
                    let (mut left_x, left_y) =
                        (pos.x / width * 2. - 1., (1. - (pos.y / height)) * 2. - 1.);
                    let (mut right_x, right_y) = (
                        (pos.x + 14. * 8.) / width * 2. - 1.,
                        (1. - (pos.y + 16. * 8.) / height) * 2. - 1.,
                    );

                    let f_width = 448.;
                    let f_height = 512.;

                    let mut vertices = vec![];
                    let mut indices = vec![];
                    let mut curr = 0;

                    for s in _s.chars() {
                        let c = s as usize;
                        let x = c % 32;
                        let y = (c - x) / 32;

                        let u = (x as f32 * 14. - 10.) / f_width;
                        let v = y as f32 * 16. / f_height;

                        let u_2 = (x as f32 * 14. + 14. - 7.) / f_width;
                        let v_2 = (y as f32 * 16. + 16.) / f_height;

                        vertices.push(FontVertex::new(
                            Vec3::new(left_x, left_y, 0.),
                            color::WHITE,
                            Vec2::new(u, v),
                        ));
                        vertices.push(FontVertex::new(
                            Vec3::new(right_x, left_y, 0.),
                            color::WHITE,
                            Vec2::new(u_2, v),
                        ));
                        vertices.push(FontVertex::new(
                            Vec3::new(right_x, right_y, 0.),
                            color::WHITE,
                            Vec2::new(u_2, v_2),
                        ));
                        vertices.push(FontVertex::new(
                            Vec3::new(left_x, right_y, 0.),
                            color::WHITE,
                            Vec2::new(u, v_2),
                        ));

                        indices.push(curr + 0);
                        indices.push(curr + 1);
                        indices.push(curr + 2);
                        indices.push(curr + 2);
                        indices.push(curr + 3);
                        indices.push(curr + 0);
                        curr += 6;

                        left_x += 20. * 8. + 250.;
                        right_x += 20. * 8. + 250.;
                    }

                    let mut buffer =
                        VertexBuffer::new(&mut self.device, vertices, indices).unwrap();

                    unsafe {
                        self.device_context.PSSetShaderResources(
                            0,
                            1,
                            &self.font_text.view.as_raw_mut(),
                        );
                    }

                    self.per_obj
                        .set(&mut self.device_context, PerObj::new(Matrix4x4::IDENTITY));

                    buffer.draw_indexed(&mut self.device_context, None);
                    self.per_frame.set(&mut self.device_context, per_frame);
                }
            }
        }

        self.per_obj.bind_vs(&mut self.device_context, 0);
        self.per_frame.bind_vs(&mut self.device_context, 1);
        self.lights.bind_ps(&mut self.device_context, 2);
        self.materials.bind_ps(&mut self.device_context, 3);

        let hr = unsafe { self.swapchain.Present(0, 0) };

        if hr == 0 {
            Some(())
        } else {
            None
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
struct Lights {
    directional: [ShaderDirectional; 8],
    point: [ShaderPoint; 8],
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
struct PerFrame {
    view_proj: Matrix4x4,
    elapsed: f32,
    dt: f32,
    eye_pos: Vec3,
    padding: [u32; 3],
}

#[repr(C)]
#[derive(Clone, Copy)]
struct PerObj {
    world: Matrix4x4,
    world_inv_trans: Matrix4x4,
}

impl PerObj {
    fn new(world: Matrix4x4) -> Self {
        Self {
            world: world.transpose(),
            world_inv_trans: world.transpose().inverse().transpose(),
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
struct ShaderDirectional {
    directional: game::renderer::lights::Directional,
    enabled: i32,
}

impl ShaderDirectional {
    const fn new(directional: game::renderer::lights::Directional) -> Self {
        Self {
            directional,
            enabled: 1,
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
struct ShaderPoint {
    point: game::renderer::lights::Point,
    enabled: i32,
}

impl ShaderPoint {
    const fn new(point: game::renderer::lights::Point) -> Self {
        Self { point, enabled: 1 }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
pub struct ShaderMaterial {
    material: Material,
}

impl std::convert::From<Material> for ShaderMaterial {
    fn from(material: Material) -> Self {
        Self { material }
    }
}
