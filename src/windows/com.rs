#![macro_use]

extern crate winapi;

use self::winapi::shared::winerror::SUCCEEDED;
use self::winapi::um::unknwnbase::IUnknown;
use self::winapi::Interface;

/// A wraper for COM objects that helps ensure safe RAII-style release.
pub struct ComPtr<T: Interface>(*mut T);

impl<T> ComPtr<T>
where
    T: Interface,
{
    pub fn try_create(ptr: *mut T) -> Option<Self> {
        if ptr.is_null() {
            None
        } else {
            Some(ComPtr(ptr))
        }
    }

    /// Wraps a new COM pointer for safe access and release.
    /// Wrapped pointer must not be null.
    fn from_raw(ptr: *mut T) -> Self {
        assert!(!ptr.is_null());
        ComPtr(ptr)
    }

    /// Returns the raw underlying pointer held by the ComPtr.
    #[allow(dead_code)] // @TODO: This should be removed or the method deleted.
    pub fn as_raw(&self) -> *const T {
        self.0
    }

    /// Returns the raw underlying pointer, as mutable, held by the ComPtr.
    pub fn as_raw_mut(&mut self) -> *mut T {
        self.0
    }

    /// Returns a reference to the underlying ComPtr's IUnknown interface.
    pub fn as_unknown(&self) -> &IUnknown {
        unsafe { &*(self.0 as *mut IUnknown) }
    }

    /// Returns a mutable reference to the underlying ComPtr's IUnknown interface.
    pub fn as_unknown_mut(&self) -> &mut IUnknown {
        unsafe { &mut *(self.0 as *mut IUnknown) }
    }

    /// Uses IUknown's QueryInterface to check for the available target interface.
    pub fn cast<U>(&self) -> Option<ComPtr<U>>
    where
        U: Interface,
    {
        use std::ptr;

        let mut result = ptr::null_mut();

        let hr = unsafe { self.as_unknown().QueryInterface(&U::uuidof(), &mut result) };

        if SUCCEEDED(hr) {
            Some(ComPtr::from_raw(result as *mut U))
        } else {
            None
        }
    }
}

impl<T> Drop for ComPtr<T>
where
    T: Interface,
{
    fn drop(&mut self) {
        unsafe { self.as_unknown().Release() };
    }
}

impl<T> std::ops::Deref for ComPtr<T>
where
    T: Interface,
{
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.0 }
    }
}

impl<T> std::ops::DerefMut for ComPtr<T>
where
    T: Interface,
{
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.0 }
    }
}

impl<T> Clone for ComPtr<T>
where
    T: Interface,
{
    fn clone(&self) -> Self {
        unsafe {
            self.as_unknown().AddRef();
            let ptr = self.0;
            Self::from_raw(ptr)
        }
    }
}

impl<T> std::convert::AsMut<T> for ComPtr<T>
where
    T: Interface,
{
    fn as_mut(&mut self) -> &mut T {
        unsafe { &mut (*self.0) }
    }
}

/// Equality on ComPtr is based on referential equality of the underlying pointers.
impl<T> PartialEq<ComPtr<T>> for ComPtr<T>
where
    T: Interface,
{
    fn eq(&self, other: &ComPtr<T>) -> bool {
        self.0 == other.0
    }
}

macro_rules! check_com {
    ($hr:expr, $ptr:expr) => {
        match winapi::shared::winerror::SUCCEEDED($hr) {
            true => crate::windows::com::ComPtr::try_create($ptr),
            false => None,
        }
    };
}
