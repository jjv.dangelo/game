#![allow(dead_code)] // @TODO: dead_code

extern crate winapi;

use self::winapi::shared::{
    minwindef::{FALSE, LPARAM, LRESULT, UINT, WPARAM},
    windef::{HWND, RECT},
};
use self::winapi::um::{debugapi, handleapi, profileapi, synchapi, winnt, winuser};
use self::winuser::{
    GetAsyncKeyState, ReleaseCapture, SetCapture, CS_HREDRAW, CS_OWNDC, CS_VREDRAW,
    GET_WHEEL_DELTA_WPARAM, WM_CLOSE, WM_DESTROY, WM_MOUSEWHEEL, WM_QUIT, WNDCLASSW,
};

use std::ptr;

#[macro_export]
macro_rules! debug {
    ($fmt:expr) => {
        $crate::windows::win32::output_debug_str($fmt);
    };

    ($fmt:expr, $args:tt) => {{
        use ::std::fmt::Write;
        let mut buffer = String::with_capacity($fmt.len());
        write!(buffer, $fmt, $args).unwrap();
        $crate::windows::win32::output_debug_str(&buffer);
    }};
}

#[inline]
pub fn to_os_str(str: &str) -> Vec<u16> {
    use std::ffi::OsStr;
    use std::os::windows::ffi::OsStrExt;

    OsStr::new(str)
        .encode_wide()
        .chain(Some(0).into_iter())
        .collect()
}

#[inline]
pub fn output_debug_str(str: &str) {
    let str = to_os_str(str);
    unsafe { debugapi::OutputDebugStringW(str.as_ptr()) }
}

#[inline]
pub fn peek_msg(hwnd: HWND) -> Option<winuser::MSG> {
    unsafe {
        let mut msg = Default::default();
        if winuser::PeekMessageW(&mut msg, hwnd, 0, 0, winuser::PM_REMOVE) != 0 {
            Some(msg)
        } else {
            None
        }
    }
}

#[inline]
pub fn translate_msg(msg: &mut winuser::MSG) {
    unsafe { winuser::TranslateMessage(msg) };
}

#[inline]
pub fn dispatch_msg(msg: &winuser::MSG) {
    unsafe { winuser::DispatchMessageW(msg) };
}

#[inline]
pub fn post_quit_msg() {
    unsafe { winuser::PostQuitMessage(0) };
}

#[inline]
pub fn release_capture() {
    unsafe { ReleaseCapture() };
}

#[inline]
pub fn set_capture(hwnd: HWND) {
    unsafe { SetCapture(hwnd) };
}

#[inline]
pub fn get_clock_freq() -> f64 {
    unsafe {
        let mut result = Default::default();
        self::profileapi::QueryPerformanceFrequency(&mut result);
        let result = *result.QuadPart();

        assert!(result != 0);

        result as f64
    }
}

#[inline]
pub fn get_wall_clock() -> f64 {
    unsafe {
        let mut result = Default::default();
        self::profileapi::QueryPerformanceCounter(&mut result);
        (*result.QuadPart()) as f64
    }
}

pub struct WindowClass(u16);

pub fn register_class(class_name: &str) -> WindowClass {
    let class_name = to_os_str(class_name);
    let class = WNDCLASSW {
        style: CS_HREDRAW | CS_VREDRAW | CS_OWNDC,
        lpfnWndProc: Some(wnd_proc),
        cbClsExtra: 0,
        cbWndExtra: 0,
        hInstance: ptr::null_mut(),
        hIcon: ptr::null_mut(),
        hCursor: ptr::null_mut(),
        hbrBackground: ptr::null_mut(),
        lpszMenuName: ptr::null_mut(),
        lpszClassName: class_name.as_ptr(),
    };

    let window_class = unsafe { winuser::RegisterClassW(&class) };
    WindowClass(window_class)
}

#[inline]
pub fn create_wnd(WindowClass(window_class): WindowClass, window_text: &str) -> HWND {
    // use std::cmp;
    use winapi::um::winuser;

    let window_text = to_os_str(window_text);

    let screen_width = unsafe { winuser::GetSystemMetrics(winuser::SM_CXSCREEN) as u32 };
    let screen_height = unsafe { winuser::GetSystemMetrics(winuser::SM_CYSCREEN) as u32 };

    let mut window_rect = RECT {
        left: 0,
        top: 0,
        right: screen_width as _,
        bottom: screen_height as _,
    };

    unsafe {
        winuser::AdjustWindowRect(&mut window_rect, winuser::WS_OVERLAPPEDWINDOW, FALSE);

        let window_width = window_rect.get_width() * 5 / 6;
        let window_height = window_rect.get_height() * 5 / 6;

        // let window_x = std::cmp::max(0, (screen_width - window_width) / 2);
        // let window_y = std::cmp::max(0, (screen_height - window_height) / 2);

        winuser::CreateWindowExW(
            0,
            window_class as _,
            window_text.as_ptr(),
            winuser::WS_OVERLAPPEDWINDOW,
            winuser::CW_USEDEFAULT, //window_x as _,
            winuser::CW_USEDEFAULT, //window_y as _,
            window_width as _,
            window_height as _,
            ptr::null_mut(),
            ptr::null_mut(),
            ptr::null_mut(),
            ptr::null_mut(),
        )
    }
}

#[inline]
pub fn show_wnd(hwnd: HWND) -> i32 {
    unsafe { winuser::ShowWindow(hwnd, winuser::SW_SHOW) }
}

/// WinProc for the main window.
pub unsafe extern "system" fn wnd_proc(
    hwnd: HWND,
    msg: UINT,
    wparam: WPARAM,
    lparam: LPARAM,
) -> LRESULT {
    use self::winuser::{DefWindowProcW, PostMessageW};

    match msg {
        WM_CLOSE | WM_DESTROY | WM_MOUSEWHEEL | WM_QUIT => {
            if PostMessageW(hwnd, msg, wparam, lparam) == 0 {
                output_debug_str("Couldn't post message.")
            }
            0
        }
        _ => DefWindowProcW(hwnd, msg, wparam, lparam),
    }
}

pub trait Measurable {
    fn get_width(&self) -> u32;
    fn get_height(&self) -> u32;
}

impl Measurable for RECT {
    #[inline]
    fn get_width(&self) -> u32 {
        (self.right - self.left) as _
    }

    #[inline]
    fn get_height(&self) -> u32 {
        (self.bottom - self.top) as _
    }
}

pub trait Window {
    fn get_client_rect(self) -> Option<RECT>;
}

impl Window for HWND {
    #[inline]
    fn get_client_rect(self) -> Option<RECT> {
        unsafe {
            let mut client_rect = Default::default();
            if winuser::GetClientRect(self, &mut client_rect) != 0 {
                Some(client_rect)
            } else {
                None
            }
        }
    }
}

#[inline]
pub fn create_handle() -> winnt::HANDLE {
    unsafe { synchapi::CreateEventW(ptr::null_mut(), FALSE, FALSE, ptr::null()) }
}

#[inline]
pub fn wait_for_single_object(fence_event: winnt::HANDLE, duration_ms: u32) {
    unsafe { synchapi::WaitForSingleObject(fence_event, duration_ms) };
}

#[inline]
pub fn close_handle(handle: winnt::HANDLE) {
    unsafe { handleapi::CloseHandle(handle) };
}

#[inline]
pub fn get_wheel_delta(wparam: WPARAM) -> i32 {
    GET_WHEEL_DELTA_WPARAM(wparam) as i32
}

#[inline]
pub fn get_async_key_state(key: i32) -> bool {
    unsafe { GetAsyncKeyState(key) & (1 << 15) != 0 }
}
