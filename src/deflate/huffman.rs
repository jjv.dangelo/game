use crate::stream::ByteStream;

#[derive(Default, Clone, Copy)]
pub struct Code {
    pub len: u16,
    pub sym: u16,
}

pub struct HuffmanTable {
    max_len: u16,
    codes: Vec<Code>,
}

impl HuffmanTable {
    pub fn new(lens: &[u32]) -> Option<Self> {
        let max_len = *lens.iter().max()? as u16;
        let max_count = 1 << max_len;

        let mut counts = vec![0; max_count];
        for &len in lens.iter() {
            counts[len as usize] += 1;
        }
        counts[0] = 0;

        let mut code = 0;
        let mut next_code = vec![0; max_count];
        for bits in 1..max_count {
            code = (code + counts[bits as usize - 1]) << 1;
            next_code[bits as usize] = code;
        }

        let mut codes = vec![Code::default(); max_count];
        for (sym, &len) in lens.iter().enumerate() {
            if len != 0 {
                let code = next_code[len as usize];
                next_code[len as usize] += 1;

                codes[code as usize].len = len as u16;
                codes[code as usize].sym = sym as u16;
            }
        }

        Some(Self { max_len, codes })
    }

    pub fn decode<I: IntoIterator<Item = u8>>(&self, stream: &mut ByteStream<I>) -> Option<u32> {
        let mut code = 0;

        for len in 1..=self.max_len {
            code |= stream.read_bits(1)?;

            if self.codes[code as usize].len == len {
                return Some(self.codes[code as usize].sym as u32);
            }

            code <<= 1;
        }

        None
    }
}
