use super::{Matrix4x4, Ray, Vec3, Vec4};
use std::ops::Mul;

/// Describes an axis-aligned bounding box (AABB) using left-hand rules.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct BoundingBox {
    pub min: Vec3,
    pub max: Vec3,
}

impl BoundingBox {
    #[inline]
    const fn new(min: Vec3, max: Vec3) -> Self {
        Self { min, max }
    }

    #[inline]
    pub fn contains_point(&self, point: Vec3) -> bool {
        self.min.le(&point) && self.max.ge(&point)
    }

    #[inline]
    /// Checks to see if there is any overlap between the two bounding boxes.
    pub fn overlaps(&self, other: &Self) -> bool {
        self.min <= other.max && self.max >= other.min
    }

    #[inline]
    /// Checks to see if the supplied bounding box is fully contained by this one.
    pub fn contains(&self, other: &Self) -> bool {
        other.min <= self.max
            && other.min >= self.min
            && other.max <= self.max
            && other.max >= self.min
    }

    #[inline]
    /// Checks to see if the ray intersects the bounding box. Algorithm taken from (Fast,
    /// Branchless Ray/Bounding Box Intersections, Part
    /// 2: NaNs)[https://tavianator.com/fast-branchless-raybounding-box-intersections-part-2-nans/].
    pub fn intersects(&self, ray: Ray) -> Option<f32> {
        let min = self.min - ray.orig;
        let max = self.max - ray.orig;

        let t1 = min.x * ray.dir_inv.x;
        let t2 = max.x * ray.dir_inv.x;

        let mut tmin = t1.min(t2);
        let mut tmax = t1.max(t2);

        let t1 = min.y * ray.dir_inv.y;
        let t2 = max.y * ray.dir_inv.y;

        tmin = tmin.max(t1.min(t2));
        tmax = tmax.min(t1.max(t2));

        let t1 = min.z * ray.dir_inv.z;
        let t2 = max.z * ray.dir_inv.z;

        tmin = tmin.max(t1.min(t2));
        tmax = tmax.min(t1.max(t2));

        if tmax > tmin.max(0.) {
            Some(tmin.max(0.))
        } else {
            None
        }
    }

    #[inline]
    pub fn width(&self) -> f32 {
        self.max.x - self.min.x
    }

    #[inline]
    pub fn height(&self) -> f32 {
        self.max.y - self.min.y
    }

    #[inline]
    pub fn depth(&self) -> f32 {
        self.max.z - self.min.z
    }

    #[inline]
    pub fn perimeter(&self) -> f32 {
        4. * (self.width() + self.height() + self.depth())
    }

    pub fn combine(&self, other: &Self) -> Self {
        let min = {
            let x = self.min.x.min(other.min.x);
            let y = self.min.y.min(other.min.y);
            let z = self.min.z.min(other.min.z);

            Vec3::new(x, y, z)
        };

        let max = {
            let x = self.max.x.max(other.max.x);
            let y = self.max.y.max(other.max.y);
            let z = self.max.z.max(other.max.z);

            Vec3::new(x, y, z)
        };

        Self::new(min, max)
    }

    pub fn change_origin(&mut self, new_origin: Vec3) -> &mut Self {
        self.min -= new_origin;
        self.max -= new_origin;
        self
    }

    pub fn scale(&mut self, scale: Vec3) -> &mut Self {
        self.min.x *= scale.x;
        self.min.y *= scale.y;
        self.min.z *= scale.z;

        self.max.x *= scale.x;
        self.max.y *= scale.y;
        self.max.z *= scale.z;

        self
    }

    pub fn wrap<'a, V: Iterator<Item = &'a Vec3> + 'a>(mut points: V) -> Self {
        let first = points.next().unwrap();
        let (min, max) = points.fold((*first, *first), |(mut min, mut max), next| {
            if next.x < min.x {
                min.x = next.x
            } else if next.x > max.x {
                max.x = next.x
            }

            if next.y < min.y {
                min.y = next.y
            } else if next.y > max.y {
                max.y = next.y
            }

            if next.z < min.z {
                min.z = next.z
            } else if next.z > max.z {
                max.z = next.z
            }

            (min, max)
        });

        Self::new(min, max)
    }

    pub fn min(&self) -> Vec3 {
        self.min
    }

    pub fn max(&self) -> Vec3 {
        self.max
    }
}

impl std::iter::FromIterator<Vec3> for Option<BoundingBox> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = Vec3>,
    {
        let mut points = iter.into_iter();
        points.next().map(|first| {
            let (min, max) = points.fold((first, first), |(min, max), next| {
                (min.min(next), max.max(next))
            });

            BoundingBox::new(min, max)
        })
    }
}

impl Mul<Matrix4x4> for BoundingBox {
    type Output = Self;

    fn mul(self, transform: Matrix4x4) -> Self::Output {
        let min = self.min;
        let max = self.max;

        let new_bb: Option<Self> = [
            self.min.to_vec4_point(),
            Vec4::point(min.x, min.y, max.z),
            Vec4::point(min.x, max.y, max.z),
            Vec4::point(min.x, max.y, min.z),
            self.max.to_vec4_point(),
            Vec4::point(max.x, min.y, max.z),
            Vec4::point(max.x, min.y, min.z),
            Vec4::point(max.x, max.y, min.z),
        ]
        .iter()
        .map(|v| v * transform)
        .map(|v| v.to_vec3())
        .collect();

        new_bb.unwrap()
    }
}

impl Mul<f32> for BoundingBox {
    type Output = Self;

    fn mul(self, scalar: f32) -> Self::Output {
        Self::new(self.min * scalar, self.max * scalar)
    }
}

impl Mul<f32> for &BoundingBox {
    type Output = BoundingBox;

    fn mul(self, scalar: f32) -> BoundingBox {
        BoundingBox::new(self.min * scalar, self.max * scalar)
    }
}

impl Mul<BoundingBox> for f32 {
    type Output = BoundingBox;

    fn mul(self, rhs: BoundingBox) -> Self::Output {
        rhs * self
    }
}

impl Mul<&BoundingBox> for f32 {
    type Output = BoundingBox;

    fn mul(self, rhs: &BoundingBox) -> Self::Output {
        rhs * self
    }
}
