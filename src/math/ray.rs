use super::Vec3;

/// Describes a Ray cast via the ray's origin and direction.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Ray {
    pub orig: Vec3,
    pub dir_inv: Vec3,
}

impl Ray {
    #[inline]
    pub fn new(orig: Vec3, dir: Vec3) -> Self {
        let dir_inv = dir.inv();
        Self { orig, dir_inv }
    }
}
