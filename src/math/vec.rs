use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

/// Represents a vector in two-dimensional space.
#[repr(C)]
#[derive(Clone, Copy)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    #[inline]
    pub const fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    #[inline]
    pub fn dot(&self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y
    }
}

impl From<[f32; 2]> for Vec2 {
    #[inline]
    fn from([x, y]: [f32; 2]) -> Self {
        Self::new(x, y)
    }
}

impl From<(f32, f32)> for Vec2 {
    #[inline]
    fn from((x, y): (f32, f32)) -> Self {
        Self::new(x, y)
    }
}

impl<I: Into<Self>> Add<I> for Vec2 {
    type Output = Self;

    #[inline]
    fn add(self, i: I) -> Self::Output {
        let other = i.into();
        Self::new(self.x + other.x, self.y + other.y)
    }
}

impl<I: Into<Self>> AddAssign<I> for Vec2 {
    #[inline]
    fn add_assign(&mut self, i: I) {
        let other = i.into();
        self.x += other.x;
        self.y += other.y;
    }
}

impl<I: Into<Self>> Sub<I> for Vec2 {
    type Output = Self;

    #[inline]
    fn sub(self, i: I) -> Self::Output {
        let other = i.into();
        Self::new(self.x - other.x, self.y - other.y)
    }
}

impl<I: Into<Self>> SubAssign<I> for Vec2 {
    #[inline]
    fn sub_assign(&mut self, i: I) {
        let other = i.into();
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl Mul<f32> for Vec2 {
    type Output = Self;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Self::new(self.x * scalar, self.y * scalar)
    }
}

impl MulAssign<f32> for Vec2 {
    fn mul_assign(&mut self, scalar: f32) {
        self.x *= scalar;
        self.y *= scalar;
    }
}

impl Mul<Vec2> for f32 {
    type Output = Vec2;

    fn mul(self, vec: Vec2) -> Self::Output {
        Vec2::new(vec.x * self, vec.y * self)
    }
}

impl std::fmt::Debug for Vec2 {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(fmt, "<{:.2}, {:.2}>", self.x, self.y)
    }
}

/// Represents a vector in three-dimensional space.
#[repr(C)]
#[derive(Clone, Copy)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vec3 {
    pub const ZERO: Vec3 = Vec3::new(0., 0., 0.);
    pub const ONE: Vec3 = Vec3::new(1., 1., 1.);
    pub const X_AXIS: Vec3 = Vec3::new(1., 0., 0.);
    pub const Y_AXIS: Vec3 = Vec3::new(0., 1., 0.);
    pub const Z_AXIS: Vec3 = Vec3::new(0., 0., 1.);

    /// Creates a new Vec3 with the given x, y, and z values.
    #[inline]
    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }

    /// Creates a new Vec3 with the given value repeated.
    #[inline]
    pub const fn repeat(v: f32) -> Self {
        Self::new(v, v, v)
    }

    #[inline]
    pub const fn to_vec4_point(self) -> Vec4 {
        Vec4::new(self.x, self.y, self.z, 1.)
    }

    #[inline]
    pub const fn to_vec4(self) -> Vec4 {
        Vec4::new(self.x, self.y, self.z, 0.)
    }

    /// Returns the vector with unit length.
    #[inline]
    pub fn normalized(self) -> Self {
        self / self.len()
    }

    /// Calculates the magnitude of the vector, squared. This is useful to avoid
    /// computing the square root when being used to compare the magnitude of
    /// two vectors.
    ///
    /// ```
    /// let x = Vec3::new(1., 1., 1.);
    /// assert_eq!(x.mag_sqrd(), 3.);
    /// ```
    #[inline]
    pub fn mag_sqrd(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    /// Calculates the magnitude of the vector.
    #[inline]
    pub fn len(&self) -> f32 {
        self.mag_sqrd().sqrt()
    }

    /// Calculates the dot product between two vectors.
    #[inline]
    pub fn dot(self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    /// Calculates the cross product between two vectors using left-hand rule.
    #[inline]
    pub fn cross(self, other: Self) -> Self {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;
        Self { x, y, z }
    }

    /// Returns a new Vec3 with each component set to 1. divided by the component of the original
    /// Vec3.
    #[inline]
    pub fn inv(self) -> Self {
        let x = 1. / self.x;
        let y = 1. / self.y;
        let z = 1. / self.z;
        Self { x, y, z }
    }

    pub fn max(self, other: Self) -> Self {
        Self {
            x: self.x.max(other.x),
            y: self.y.max(other.y),
            z: self.z.max(other.z),
        }
    }

    pub fn min(self, other: Self) -> Self {
        Self {
            x: self.x.min(other.x),
            y: self.y.min(other.y),
            z: self.z.min(other.z),
        }
    }
}

impl std::fmt::Debug for Vec3 {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(fmt, "<{:.2}, {:.2}, {:.2}>", self.x, self.y, self.z)
    }
}

impl From<[f32; 3]> for Vec3 {
    #[inline]
    fn from([x, y, z]: [f32; 3]) -> Self {
        Self { x, y, z }
    }
}

impl From<(f32, f32, f32)> for Vec3 {
    #[inline]
    fn from((x, y, z): (f32, f32, f32)) -> Self {
        Self { x, y, z }
    }
}

impl From<(Vec2, f32)> for Vec3 {
    #[inline]
    fn from((Vec2 { x, y }, z): (Vec2, f32)) -> Self {
        Self::new(x, y, z)
    }
}

impl Default for Vec3 {
    #[inline]
    fn default() -> Self {
        Vec3::ZERO
    }
}

impl<I: Into<Self>> Add<I> for Vec3 {
    type Output = Self;

    #[inline]
    fn add(self, i: I) -> Self::Output {
        let other = i.into();
        Self::new(self.x + other.x, self.y + other.y, self.z + other.z)
    }
}

impl<I: Into<Self>> AddAssign<I> for Vec3 {
    #[inline]
    fn add_assign(&mut self, i: I) {
        let other = i.into();
        self.x = self.x + other.x;
        self.y = self.y + other.y;
        self.z = self.z + other.z;
    }
}

impl<I: Into<Self>> Sub<I> for Vec3 {
    type Output = Self;

    #[inline]
    fn sub(self, i: I) -> Self {
        let other = i.into();
        Self::new(self.x - other.x, self.y - other.y, self.z - other.z)
    }
}

impl<I: Into<Self>> SubAssign<I> for Vec3 {
    #[inline]
    fn sub_assign(&mut self, i: I) {
        let other = i.into();
        self.x -= other.x;
        self.y -= other.y;
        self.z -= other.z;
    }
}

impl Neg for Vec3 {
    type Output = Self;

    #[inline]
    fn neg(self) -> Self::Output {
        self * -1.
    }
}

impl Div<f32> for Vec3 {
    type Output = Self;

    #[inline]
    fn div(self, scalar: f32) -> Self::Output {
        Self::new(self.x / scalar, self.y / scalar, self.z / scalar)
    }
}

impl DivAssign<f32> for Vec3 {
    #[inline]
    fn div_assign(&mut self, scalar: f32) {
        self.x = self.x / scalar;
        self.y = self.y / scalar;
        self.z = self.z / scalar;
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Self::new(self.x * scalar, self.y * scalar, self.z * scalar)
    }
}

impl Mul<Vec3> for f32 {
    type Output = Vec3;

    #[inline]
    fn mul(self, other: Vec3) -> Self::Output {
        other * self
    }
}

impl MulAssign<f32> for Vec3 {
    #[inline]
    fn mul_assign(&mut self, scalar: f32) {
        self.x = self.x * scalar;
        self.y = self.y * scalar;
        self.z = self.z * scalar;
    }
}

impl std::cmp::PartialEq for Vec3 {
    fn eq(&self, other: &Self) -> bool {
        use std::f32;

        (self.x - other.x).abs() < f32::EPSILON
            && (self.y - other.y).abs() < f32::EPSILON
            && (self.z - other.z).abs() < f32::EPSILON
    }
}

impl std::cmp::PartialOrd for Vec3 {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.lt(other) {
            Some(std::cmp::Ordering::Less)
        } else if self.gt(other) {
            Some(std::cmp::Ordering::Greater)
        } else {
            Some(std::cmp::Ordering::Equal)
        }
    }

    #[inline]
    fn lt(&self, other: &Self) -> bool {
        self.x < other.x && self.y < other.y && self.z < other.z
    }

    #[inline]
    fn gt(&self, other: &Self) -> bool {
        self.x > other.x && self.y > other.y && self.z > other.z
    }

    #[inline]
    fn le(&self, other: &Self) -> bool {
        self.x <= other.x && self.y <= other.y && self.z <= other.z
    }

    #[inline]
    fn ge(&self, other: &Self) -> bool {
        self.x >= other.x && self.y >= other.y && self.z >= other.z
    }
}

/// Represents a vector in four-dimensional space.
#[repr(C)]
#[derive(Clone, Copy)]
pub struct Vec4 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32,
}

impl std::fmt::Debug for Vec4 {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            fmt,
            "<{:.2}, {:.2}, {:.2}, {:.2}>",
            self.x, self.y, self.z, self.w
        )
    }
}

impl Default for Vec4 {
    #[inline]
    fn default() -> Self {
        Vec4::ZERO
    }
}

impl Vec4 {
    pub const ZERO: Self = Vec4::new(0., 0., 0., 0.);

    /// Creates a new Vec4 with the given x, y, z, and w values.
    pub const fn new(x: f32, y: f32, z: f32, w: f32) -> Self {
        Self { x, y, z, w }
    }

    /// Creates a new Vec4 with the given x, y, z, and w set to 1.
    pub const fn point(x: f32, y: f32, z: f32) -> Self {
        Self::new(x, y, z, 1.)
    }

    /// Creates a new Vec4 with the given x, y, z, and w set to 0.
    pub const fn vec(x: f32, y: f32, z: f32) -> Self {
        Self::new(x, y, z, 0.)
    }

    pub const fn to_vec3(self) -> Vec3 {
        Vec3::new(self.x, self.y, self.z)
    }

    pub fn to_vec3_w(self) -> Vec3 {
        let w = 1. / self.w;
        Vec3::new(self.x, self.y, self.z) * w
    }

    /// Calculates the magnitude of the vector, squared. f32is is usefull to avoid
    /// computing the square root when being used to compare the magnitude of
    /// two vectors.
    ///
    /// ```
    /// let x = Vec4::new(1., 1., 1., 1.);
    /// assert_eq!(x.mag_sqrd(), 4.);
    /// ```
    pub fn mag_sqrd(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w
    }

    /// Calculates the magnitude of the vector.
    pub fn len(&self) -> f32 {
        self.mag_sqrd().sqrt()
    }

    /// Returns the vector with unit length.
    pub fn normalized(&self) -> Self {
        *self / self.len()
    }

    /// Normalizes the vector to unit length.
    pub fn normalize(&mut self) {
        *self = *self / self.len();
    }

    /// Calculates the dot product between two vectors.
    pub fn dot(&self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }
}

impl From<[f32; 4]> for Vec4 {
    #[inline]
    fn from([x, y, z, w]: [f32; 4]) -> Self {
        Self::new(x, y, z, w)
    }
}

impl From<(f32, f32, f32, f32)> for Vec4 {
    #[inline]
    fn from((x, y, z, w): (f32, f32, f32, f32)) -> Self {
        Self::new(x, y, z, w)
    }
}

impl From<(Vec2, f32, f32)> for Vec4 {
    #[inline]
    fn from((Vec2 { x, y }, z, w): (Vec2, f32, f32)) -> Self {
        Self::new(x, y, z, w)
    }
}

impl From<(Vec3, f32)> for Vec4 {
    #[inline]
    fn from((Vec3 { x, y, z}, w): (Vec3, f32)) -> Self {
        Self::new(x, y, z, w)
    }
}

impl std::iter::Sum for Vec4 {
    #[inline]
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(Vec4::ZERO, |l, r| l + r)
    }
}

impl<I: Into<Self>> Add<I> for Vec4 {
    type Output = Self;

    #[inline]
    fn add(self, i: I) -> Self {
        let other = i.into();
        Self::new(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
            self.w + other.w,
        )
    }
}

impl<I: Into<Self>> AddAssign<I> for Vec4 {
    #[inline]
    fn add_assign(&mut self, i: I) {
        let other = i.into();
        self.x = self.x + other.x;
        self.y = self.y + other.y;
        self.z = self.z + other.z;
        self.w = self.w + other.w;
    }
}

impl<I: Into<Self>> Sub<I> for Vec4 {
    type Output = Self;

    #[inline]
    fn sub(self, i: I) -> Self::Output {
        let other = i.into();

        Self::new(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
            self.w - other.w,
        )
    }
}

impl<I: Into<Self>> SubAssign<I> for Vec4 {
    #[inline]
    fn sub_assign(&mut self, i: I) {
        let other = i.into();

        self.x = self.x - other.x;
        self.y = self.y - other.y;
        self.z = self.z - other.z;
        self.w = self.w - other.w;
    }
}

impl Neg for Vec4 {
    type Output = Self;

    #[inline]
    fn neg(self) -> Self::Output {
        Vec4::new(-self.x, -self.y, -self.z, -self.w)
    }
}

impl Div<f32> for Vec4 {
    type Output = Self;

    #[inline]
    fn div(self, scalar: f32) -> Self {
        Self::new(
            self.x / scalar,
            self.y / scalar,
            self.z / scalar,
            self.w / scalar,
        )
    }
}

impl DivAssign<f32> for Vec4 {
    #[inline]
    fn div_assign(&mut self, scalar: f32) {
        self.x = self.x / scalar;
        self.y = self.y / scalar;
        self.z = self.z / scalar;
        self.w = self.w / scalar;
    }
}

impl Mul<f32> for Vec4 {
    type Output = Self;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Self::new(
            self.x * scalar,
            self.y * scalar,
            self.z * scalar,
            self.w * scalar,
        )
    }
}

impl Mul<Vec4> for f32 {
    type Output = Vec4;

    #[inline]
    fn mul(self, other: Vec4) -> Self::Output {
        other * self
    }
}

impl MulAssign<f32> for Vec4 {
    #[inline]
    fn mul_assign(&mut self, scalar: f32) {
        self.x = self.x * scalar;
        self.y = self.y * scalar;
        self.z = self.z * scalar;
        self.w = self.w * scalar;
    }
}

impl std::cmp::PartialEq for Vec4 {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        use std::f32;

        (self.x - other.x).abs() < f32::EPSILON
            && (self.y - other.y).abs() < f32::EPSILON
            && (self.z - other.z).abs() < f32::EPSILON
            && (self.w - other.w).abs() < f32::EPSILON
    }
}

impl std::cmp::PartialOrd for Vec4 {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.lt(other) {
            Some(std::cmp::Ordering::Less)
        } else if self.gt(other) {
            Some(std::cmp::Ordering::Greater)
        } else {
            Some(std::cmp::Ordering::Equal)
        }
    }

    #[inline]
    fn lt(&self, other: &Self) -> bool {
        self.x < other.x && self.y < other.y && self.z < other.z && self.w < other.w
    }

    #[inline]
    fn gt(&self, other: &Self) -> bool {
        self.x > other.x && self.y > other.y && self.z > other.z && self.w > other.w
    }

    #[inline]
    fn le(&self, other: &Self) -> bool {
        self.x <= other.x && self.y <= other.y && self.z <= other.z && self.w <= other.w
    }

    #[inline]
    fn ge(&self, other: &Self) -> bool {
        self.x >= other.x && self.y >= other.y && self.z >= other.z && self.w >= other.w
    }
}
