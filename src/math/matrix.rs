use super::{Quaternion, Vec3, Vec4};
use std::ops::{Add, Div, Mul, MulAssign};

/// Calculates the determinant of a 3x3 matrix.
#[inline]
fn det3x3(m: [f32; 9]) -> f32 {
    //     [ 0, 1, 2 ]
    // det [ 3, 4, 5 ]
    //     [ 6, 7, 8 ]
    m[0] * (m[4] * m[8] - m[5] * m[7]) - m[1] * (m[3] * m[8] - m[5] * m[6])
        + m[2] * (m[3] * m[7] - m[4] * m[6])
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Matrix4x4([f32; 16]);

impl Matrix4x4 {
    pub const IDENTITY: Matrix4x4 = Matrix4x4::identity();

    /// Generates a new 4x4 row-major matrix.
    pub const fn new(rows: [Vec4; 4]) -> Self {
        Matrix4x4([
            rows[0].x, rows[0].y, rows[0].z, rows[0].w, rows[1].x, rows[1].y, rows[1].z, rows[1].w,
            rows[2].x, rows[2].y, rows[2].z, rows[2].w, rows[3].x, rows[3].y, rows[3].z, rows[3].w,
        ])
    }

    /// Creates a 4x4 identity matrix.
    pub const fn identity() -> Self {
        Matrix4x4([
            1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.,
        ])
    }

    /// Creates a 4x4 of zeros.
    pub const fn zero() -> Self {
        Matrix4x4([
            0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
        ])
    }

    #[inline]
    /// Calculates the 4x4 matrix's determinant.
    pub fn det(&self) -> f32 {
        // Sloppy and hard-coded. Works for now. :)
        let m = &self.0;

        // [ 00, 01, 02, 03 ]
        // [ 04, 05, 06, 07 ]
        // [ 08, 09, 10, 11 ]
        // [ 12, 13, 14, 15 ]

        //      [ 05, 06, 07 ]
        // 00 * [ 09, 10, 11 ]
        //      [ 13, 14, 15 ]
        let det_1 = det3x3([m[5], m[6], m[7], m[9], m[10], m[11], m[13], m[14], m[15]]);

        //      [ 04, 06, 07 ]
        // 01 * [ 08, 10, 11 ]
        //      [ 12, 14, 15 ]
        let det_2 = det3x3([m[4], m[6], m[7], m[8], m[10], m[11], m[12], m[14], m[15]]);

        //      [ 04, 05, 07 ]
        // 02 * [ 08, 09, 11 ]
        //      [ 12, 13, 15 ]
        let det_3 = det3x3([m[4], m[5], m[7], m[8], m[9], m[11], m[12], m[13], m[15]]);

        //      [ 04, 05, 06 ]
        // 03 * [ 08, 09, 10 ]
        //      [ 12, 13, 14 ]
        let det_4 = det3x3([m[4], m[5], m[6], m[8], m[9], m[10], m[12], m[13], m[14]]);

        m[0] * det_1 - m[1] * det_2 + m[2] * det_3 - m[3] * det_4
    }

    #[inline]
    /// Calculates the 4x4 matrix's cofactor.
    pub fn cofactor(&self) -> Self {
        // Sloppy and hard-coded. Works for now. :)

        // [ 00, 01, 02, 03 ]
        // [ 04, 05, 06, 07 ]   determine if value is negative: i = (-1)^(i+j)
        // [ 08, 09, 10, 11 ]
        // [ 12, 13, 14, 15 ]
        let m = &self.0;

        //     [ 05, 06, 07 ]      [ 04, 06, 07 ]      [ 04, 05, 07 ]      [ 04, 05, 06 ]
        // det [ 09, 10, 11 ]  det [ 08, 10, 11 ]  det [ 08, 09, 11 ]  det [ 08, 09, 10 ]
        //     [ 13, 14, 15 ]      [ 12, 14, 15 ]      [ 12, 13, 15 ]      [ 12, 13, 14 ]
        let row_1 = {
            let _00 = det3x3([m[5], m[6], m[7], m[9], m[10], m[11], m[13], m[14], m[15]]);
            let _01 = -det3x3([m[4], m[6], m[7], m[8], m[10], m[11], m[12], m[14], m[15]]);
            let _02 = det3x3([m[4], m[5], m[7], m[8], m[9], m[11], m[12], m[13], m[15]]);
            let _03 = -det3x3([m[4], m[5], m[6], m[8], m[9], m[10], m[12], m[13], m[14]]);
            Vec4::new(_00, _01, _02, _03)
        };

        //     [ 01, 02, 03 ]      [ 00, 02, 03 ]      [ 00, 01, 03 ]      [ 00, 01, 02 ]
        // det [ 09, 10, 11 ]  det [ 08, 10, 11 ]  det [ 08, 09, 11 ]  det [ 08, 09, 10 ]
        //     [ 13, 14, 15 ]      [ 12, 14, 15 ]      [ 12, 13, 15 ]      [ 12, 13, 14 ]
        let row_2 = {
            let _10 = -det3x3([m[1], m[2], m[3], m[9], m[10], m[11], m[13], m[14], m[15]]);
            let _11 = det3x3([m[0], m[2], m[3], m[8], m[10], m[11], m[12], m[14], m[15]]);
            let _12 = -det3x3([m[0], m[1], m[3], m[8], m[9], m[11], m[12], m[13], m[15]]);
            let _13 = det3x3([m[0], m[1], m[2], m[8], m[9], m[10], m[12], m[13], m[14]]);
            Vec4::new(_10, _11, _12, _13)
        };

        //     [ 01, 02, 03 ]      [ 00, 02, 03 ]      [ 00, 01, 03 ]      [ 00, 01, 02 ]
        // det [ 05, 06, 07 ]  det [ 04, 06, 07 ]  det [ 04, 05, 07 ]  det [ 04, 05, 06 ]
        //     [ 13, 14, 15 ]      [ 12, 14, 15 ]      [ 12, 13, 15 ]      [ 12, 13, 14 ]
        let row_3 = {
            let _20 = det3x3([m[1], m[2], m[3], m[5], m[6], m[07], m[13], m[14], m[15]]);
            let _21 = -det3x3([m[0], m[2], m[3], m[4], m[6], m[7], m[12], m[14], m[15]]);
            let _22 = det3x3([m[0], m[1], m[3], m[4], m[5], m[7], m[12], m[13], m[15]]);
            let _23 = -det3x3([m[0], m[1], m[2], m[4], m[5], m[6], m[12], m[13], m[14]]);
            Vec4::new(_20, _21, _22, _23)
        };

        //     [ 01, 02, 03 ]      [ 00, 02, 03 ]      [ 00, 01, 03 ]      [ 00, 01, 02 ]
        // det [ 05, 06, 07 ]  det [ 04, 06, 07 ]  det [ 04, 05, 07 ]  det [ 04, 05, 06 ]
        //     [ 09, 10, 11 ]      [ 08, 10, 11 ]      [ 08, 09, 11 ]      [ 08, 09, 10 ]
        let row_4 = {
            let _30 = -det3x3([m[1], m[2], m[3], m[5], m[6], m[7], m[9], m[10], m[11]]);
            let _31 = det3x3([m[0], m[2], m[3], m[4], m[6], m[7], m[8], m[10], m[11]]);
            let _32 = -det3x3([m[0], m[1], m[3], m[4], m[5], m[7], m[8], m[9], m[11]]);
            let _33 = det3x3([m[0], m[1], m[2], m[4], m[5], m[6], m[8], m[9], m[10]]);
            Vec4::new(_30, _31, _32, _33)
        };

        Matrix4x4::new([row_1, row_2, row_3, row_4])
    }

    #[inline]
    pub fn transpose(&self) -> Self {
        // Sloppy and hard-coded. Works for now. :)

        // [ 00, 01, 02, 03 ]     [ 00, 04, 08, 12 ]
        // [ 04, 05, 06, 07 ]     [ 01, 05, 09, 13 ]
        // [ 08, 09, 10, 11 ]     [ 02, 06, 10, 14 ]
        // [ 12, 13, 14, 15 ]     [ 03, 07, 11, 15 ]
        let m = &self.0;

        Matrix4x4::new([
            Vec4::new(m[00], m[04], m[08], m[12]),
            Vec4::new(m[01], m[05], m[09], m[13]),
            Vec4::new(m[02], m[06], m[10], m[14]),
            Vec4::new(m[03], m[07], m[11], m[15]),
        ])
    }

    #[inline]
    pub fn adjoint(&self) -> Self {
        //       T
        // A* = C
        //       A
        self.cofactor().transpose()
    }

    #[inline]
    pub fn inverse(&self) -> Self {
        //         A*
        // A^-1 = ----
        //        detA
        let adjoint = self.adjoint();
        let determinant = self.det();
        adjoint / determinant
    }

    #[inline]
    /// Creates a rotational matrix around the x-axis.
    pub fn rotation_x(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        let matrix = [
            1., 0., 0., 0., 0., cos, sin, 0., 0., -sin, cos, 0., 0., 0., 0., 1.,
        ];

        Matrix4x4(matrix)
    }

    #[inline]
    /// Creates a rotational matrix around the y-axis.
    pub fn rotation_y(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        let matrix = [
            cos, 0., -sin, 0., 0., 1., 0., 0., sin, 0., cos, 0., 0., 0., 0., 1.,
        ];

        Matrix4x4(matrix)
    }

    #[inline]
    /// Creates a rotational matrix around the z-axis.
    pub fn rotation_z(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        let matrix = [
            cos, sin, 0., 0., -sin, cos, 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.,
        ];

        Matrix4x4(matrix)
    }

    #[inline]
    pub const fn translation(pos: Vec3) -> Self {
        Matrix4x4([
            1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., pos.x, pos.y, pos.z, 1.,
        ])
    }

    #[inline]
    pub fn translation_inv(pos: Vec3) -> Self {
        Matrix4x4::translation(-pos)
    }

    #[inline]
    pub const fn scale(x: f32, y: f32, z: f32) -> Self {
        Matrix4x4([x, 0., 0., 0., 0., y, 0., 0., 0., 0., z, 0., 0., 0., 0., 1.])
    }

    #[inline]
    /// Creates a combined matrix for Scale * Rotate * Translate.
    pub fn srt(scale: Vec3, rotate: Vec3, translate: Vec3) -> Self {
        let scale = Matrix4x4::scale(scale.x, scale.y, scale.z);
        let rotate = {
            Matrix4x4::rotation_x(rotate.x)
                * Matrix4x4::rotation_y(rotate.y)
                * Matrix4x4::rotation_z(rotate.z)
        };
        let translate = Matrix4x4::translation(translate);

        scale * rotate * translate
    }

    #[inline]
    pub fn rotation_axis(axis: Vec3, radians: f32) -> Self {
        let axis = axis.normalized();
        let (s, c) = (radians.sin(), radians.cos());
        let (x, y, z) = (axis.x, axis.y, axis.z);
        let (x2, y2, z2) = (x.powi(2), y.powi(2), z.powi(2));
        let (xy, xz, yz) = (x * y, x * z, y * z);
        let (sx, sy, sz) = (s * x, s * y, s * z);

        let matrix = [
            c + (1. - c) * x2,
            (1. - c) * xy + sz,
            (1. - c) * xz - sy,
            0.,
            (1. - c) * xy - sz,
            c + (1. - c) * y2,
            (1. - c) * yz + sx,
            0.,
            (1. - c) * xz + sy,
            (1. - c) * yz - sx,
            c + (1. - c) * z2,
            0.,
            0.,
            0.,
            0.,
            1.,
        ];

        Matrix4x4(matrix)
    }

    #[inline]
    pub fn view_projection(camera_loc: Vec3, up: Vec3, look_at: Vec3) -> Self {
        let w = (look_at - camera_loc).normalized();
        let u = up.cross(w).normalized();
        let v = w.cross(u);
        let mq = -camera_loc;

        Matrix4x4([
            u.x,
            v.x,
            w.x,
            0.,
            u.y,
            v.y,
            w.y,
            0.,
            u.z,
            v.z,
            w.z,
            0.,
            mq.dot(u),
            mq.dot(v),
            mq.dot(w),
            1.,
        ])
    }

    #[inline]
    pub fn fov_projection(fov_angle_y: f32, aspect: f32, near_z: f32, far_z: f32) -> Self {
        let a = fov_angle_y / 2.;
        let t = a.tan();

        Matrix4x4([
            1. / (aspect * t),
            0.,
            0.,
            0.,
            0.,
            1. / t,
            0.,
            0.,
            0.,
            0.,
            far_z / (far_z - near_z),
            1.,
            0.,
            0.,
            (-near_z * far_z) / (far_z - near_z),
            0.,
        ])
    }

    #[inline]
    pub fn affine_transformation(
        scale: Vec3,
        rotation_origin: Vec3,
        rotation: Quaternion,
        translation: Vec3,
    ) -> Self {
        let scale = Matrix4x4::scale(scale.x, scale.y, scale.z);

        let origin = Vec3::ZERO;

        let translate_before = Matrix4x4::translation(origin - rotation_origin);
        let rotation = Matrix4x4::from(rotation);
        let translate_back = Matrix4x4::translation(rotation_origin);
        let translate = Matrix4x4::translation(translation);

        scale * translate_before * rotation * translate_back * translate
    }
}

impl Default for Matrix4x4 {
    fn default() -> Self {
        Self::IDENTITY
    }
}

impl Add for Matrix4x4 {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        let m1 = self.0;
        let m2 = other.0;

        let matrix = [
            m1[00] + m2[00],
            m1[01] + m2[01],
            m1[02] + m2[02],
            m1[03] + m2[03],
            m1[04] + m2[04],
            m1[05] + m2[05],
            m1[06] + m2[06],
            m1[07] + m2[07],
            m1[08] + m2[08],
            m1[09] + m2[09],
            m1[10] + m2[10],
            m1[11] + m2[11],
            m1[12] + m2[12],
            m1[13] + m2[13],
            m1[14] + m2[14],
            m1[15] + m2[15],
        ];

        Matrix4x4(matrix)
    }
}

impl Add for &Matrix4x4 {
    type Output = Matrix4x4;

    fn add(self, other: Self) -> Self::Output {
        let m1 = &self.0;
        let m2 = &other.0;

        let matrix = [
            m1[00] + m2[00],
            m1[01] + m2[01],
            m1[02] + m2[02],
            m1[03] + m2[03],
            m1[04] + m2[04],
            m1[05] + m2[05],
            m1[06] + m2[06],
            m1[07] + m2[07],
            m1[08] + m2[08],
            m1[09] + m2[09],
            m1[10] + m2[10],
            m1[11] + m2[11],
            m1[12] + m2[12],
            m1[13] + m2[13],
            m1[14] + m2[14],
            m1[15] + m2[15],
        ];

        Matrix4x4(matrix)
    }
}

impl Mul<f32> for Matrix4x4 {
    type Output = Self;

    fn mul(self, s: f32) -> Self::Output {
        let m1 = self.0;

        let matrix = [
            m1[00] * s,
            m1[01] * s,
            m1[02] * s,
            m1[03] * s,
            m1[04] * s,
            m1[05] * s,
            m1[06] * s,
            m1[07] * s,
            m1[08] * s,
            m1[09] * s,
            m1[10] * s,
            m1[11] * s,
            m1[12] * s,
            m1[13] * s,
            m1[14] * s,
            m1[15] * s,
        ];

        Matrix4x4(matrix)
    }
}

impl Mul<f32> for &Matrix4x4 {
    type Output = Matrix4x4;

    fn mul(self, s: f32) -> Self::Output {
        let m1 = self.0;

        let matrix = [
            m1[00] * s,
            m1[01] * s,
            m1[02] * s,
            m1[03] * s,
            m1[04] * s,
            m1[05] * s,
            m1[06] * s,
            m1[07] * s,
            m1[08] * s,
            m1[09] * s,
            m1[10] * s,
            m1[11] * s,
            m1[12] * s,
            m1[13] * s,
            m1[14] * s,
            m1[15] * s,
        ];

        Matrix4x4(matrix)
    }
}

impl Mul<Matrix4x4> for f32 {
    type Output = Matrix4x4;

    #[inline]
    fn mul(self, m: Matrix4x4) -> Self::Output {
        m * self
    }
}

impl Mul<&Matrix4x4> for f32 {
    type Output = Matrix4x4;

    #[inline]
    fn mul(self, m: &Matrix4x4) -> Self::Output {
        m * self
    }
}

impl Div<f32> for Matrix4x4 {
    type Output = Self;

    fn div(self, s: f32) -> Self::Output {
        // assert!(s != 0.);  // @TODO: Divide by zero?!
        let m = self.0;

        let matrix = [
            m[00] / s,
            m[01] / s,
            m[02] / s,
            m[03] / s,
            m[04] / s,
            m[05] / s,
            m[06] / s,
            m[07] / s,
            m[08] / s,
            m[09] / s,
            m[10] / s,
            m[11] / s,
            m[12] / s,
            m[13] / s,
            m[14] / s,
            m[15] / s,
        ];

        Matrix4x4(matrix)
    }
}

impl Div<f32> for &Matrix4x4 {
    type Output = Matrix4x4;

    fn div(self, s: f32) -> Self::Output {
        assert!(s != 0.);
        let m = self.0;

        let matrix = [
            m[00] / s,
            m[01] / s,
            m[02] / s,
            m[03] / s,
            m[04] / s,
            m[05] / s,
            m[06] / s,
            m[07] / s,
            m[08] / s,
            m[09] / s,
            m[10] / s,
            m[11] / s,
            m[12] / s,
            m[13] / s,
            m[14] / s,
            m[15] / s,
        ];

        Matrix4x4(matrix)
    }
}

impl MulAssign<f32> for Matrix4x4 {
    fn mul_assign(&mut self, s: f32) {
        let mut matrix = self.0;

        for i in 0..16 {
            matrix[i] *= s;
        }
    }
}

impl MulAssign<f32> for &Matrix4x4 {
    fn mul_assign(&mut self, s: f32) {
        let mut matrix = self.0;

        for i in 0..16 {
            matrix[i] *= s;
        }
    }
}

impl Mul<Matrix4x4> for Matrix4x4 {
    type Output = Self;

    #[inline]
    fn mul(self, other: Self) -> Self::Output {
        let m1 = self.0;
        let m2 = other.0;

        let mut matrix = [0.; 16];

        for i in 0..4 {
            let m1_start = i * 4;

            for j in 0..4 {
                matrix[m1_start + j] = m1[m1_start + 0] * m2[j + 0]
                    + m1[m1_start + 1] * m2[j + 4]
                    + m1[m1_start + 2] * m2[j + 8]
                    + m1[m1_start + 3] * m2[j + 12];
            }
        }

        Matrix4x4(matrix)
    }
}

impl Mul<Matrix4x4> for Vec4 {
    type Output = Vec4;

    #[inline]
    fn mul(self: Self, matrix: Matrix4x4) -> Self::Output {
        let v = [self.x, self.y, self.z, self.w];
        matrix.0[..]
            .chunks(4) // Group into rows
            .map(|row| Vec4::new(row[0], row[1], row[2], row[3])) // Convert to Vec4
            .zip(v.iter()) // Zip with the vector components
            .map(|(l, r)| l * *r) // Multiply zipped components (x * [a11, a12, a13, a14]), etc
            .sum() // Sum resulting vectors
    }
}

impl<'a> Mul<Matrix4x4> for &'a Vec4 {
    type Output = Vec4;

    #[inline]
    fn mul(self: Self, matrix: Matrix4x4) -> Self::Output {
        let v = [self.x, self.y, self.z, self.w];
        matrix.0[..]
            .chunks(4) // Group into rows
            .map(|row| {
                assert_eq!(row.len(), 4);
                Vec4::new(row[0], row[1], row[2], row[3]) // Convert to Vec4
            })
            .zip(v.iter()) // Zip with the vector components
            .map(|(l, r)| l * *r) // Multiply zipped components (x * [a11, a12, a13, a14]), etc
            .sum() // Sum resulting vectors
    }
}

impl std::ops::Index<(usize, usize)> for Matrix4x4 {
    type Output = f32;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        let index = y * 4 + x;
        assert!(index < 16);
        &self.0[index]
    }
}
