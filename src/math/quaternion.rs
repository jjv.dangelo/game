use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

use super::vec::*;
use super::{Lerp, Matrix4x4};

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Quaternion {
    pub u: Vec3,
    pub w: f32,
}

#[allow(dead_code)]
impl Quaternion {
    pub const IDENTITY: Quaternion = Self::raw(Vec3::ZERO, 1.);
    pub const I: Quaternion = Self::raw(Vec3::new(1., 0., 0.), 0.);
    pub const J: Quaternion = Self::raw(Vec3::new(0., 1., 0.), 0.);
    pub const K: Quaternion = Self::raw(Vec3::new(0., 0., 1.), 0.);

    #[inline]
    pub const fn raw(u: Vec3, w: f32) -> Self {
        Self { u, w }
    }

    #[inline]
    pub fn new(n: Vec3, theta: f32) -> Self {
        Self::raw((theta / 2.).sin() * n, (theta / 2.).cos())
    }

    #[inline]
    pub fn rotate_vec(self, v: Vec3) -> Vec3 {
        let p = Self::raw(v, 0.);
        let q = self;
        let q_conj = self.conjugate();

        (q * p * q_conj).u
    }

    #[inline]
    pub fn conjugate(self) -> Self {
        Self::raw(self.u * -1., self.w)
    }

    #[inline]
    pub fn mag(&self) -> f32 {
        self.mag_sqrd().sqrt()
    }

    #[inline]
    pub fn mag_sqrd(&self) -> f32 {
        self.u.mag_sqrd() + self.w.powi(2)
    }

    #[inline]
    pub fn inverse(self) -> Self {
        self.conjugate() / self.mag_sqrd()
    }

    #[inline]
    pub fn dot(self, other: Self) -> f32 {
        self.u.dot(other.u) + self.w * other.w
    }

    #[inline]
    pub fn normalize(self) -> Self {
        self / self.mag()
    }

    #[inline]
    pub fn slerp(self, other: Self, t: f32) -> Self {
        let other = if (self - other).mag_sqrd() > (self + other).mag_sqrd() {
            -other
        } else {
            other
        };

        let cos_theta = self.dot(other);

        if cos_theta > 1. - 0.001 {
            self.lerp(other, t)
        } else {
            let theta = cos_theta.acos();
            let sin_theta = theta.sin();
            self * ((theta * (1. - t)).sin() / sin_theta) + other * ((theta * t).sin() / sin_theta)
        }
    }
}

impl std::convert::From<Quaternion> for Matrix4x4 {
    #[inline]
    fn from(q: Quaternion) -> Self {
        let w = q.w;
        let u = q.u;
        Matrix4x4::new([
            Vec4::new(
                1. - 2. * u.y.powi(2) - 2. * u.z.powi(2),
                2. * u.x * u.y + 2. * u.z * w,
                2. * u.x * u.z - 2. * u.y * w,
                0.,
            ),
            Vec4::new(
                2. * u.x * u.y - 2. * u.z * w,
                1. - 2. * u.x.powi(2) - 2. * u.z.powi(2),
                2. * u.y * u.z + 2. * u.x * w,
                0.,
            ),
            Vec4::new(
                2. * u.x * u.z + 2. * u.y * w,
                2. * u.y * u.z - 2. * u.x * w,
                1. - 2. * u.x.powi(2) - 2. * u.y.powi(2),
                0.,
            ),
            Vec4::new(0., 0., 0., 1.),
        ])
    }
}

impl Add for Quaternion {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self::raw(self.u + other.u, self.w + other.w)
    }
}

impl AddAssign for Quaternion {
    fn add_assign(&mut self, other: Self) {
        self.u += other.u;
        self.w += other.w;
    }
}

impl Sub for Quaternion {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self::raw(self.u - other.u, self.w - other.w)
    }
}

impl SubAssign for Quaternion {
    fn sub_assign(&mut self, other: Self) {
        self.u -= other.u;
        self.w -= other.w;
    }
}

impl Mul<Self> for Quaternion {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        Self::raw(
            self.w * other.u + other.w * self.u + self.u.cross(other.u),
            self.w * other.w - self.u.dot(other.u),
        )
    }
}

impl MulAssign<Self> for Quaternion {
    fn mul_assign(&mut self, other: Self) {
        self.u = self.w * other.u + other.w * self.u + self.u.cross(other.u);
        self.w = self.w * other.w - self.u.dot(other.u);
    }
}

impl Mul<f32> for Quaternion {
    type Output = Self;

    fn mul(self, other: f32) -> Self::Output {
        Self::raw(self.u * other, self.w * other)
    }
}

impl Mul<Quaternion> for f32 {
    type Output = Quaternion;

    fn mul(self, q: Quaternion) -> Self::Output {
        q * self
    }
}

impl MulAssign<f32> for Quaternion {
    fn mul_assign(&mut self, other: f32) {
        self.u *= other;
        self.w *= other;
    }
}

impl Div<f32> for Quaternion {
    type Output = Self;

    fn div(self, other: f32) -> Self::Output {
        Self::raw(self.u / other, self.w / other)
    }
}

impl DivAssign<f32> for Quaternion {
    fn div_assign(&mut self, other: f32) {
        self.u /= other;
        self.w /= other;
    }
}

impl Neg for Quaternion {
    type Output = Self;

    fn neg(self) -> Self::Output {
        self * -1.
    }
}
