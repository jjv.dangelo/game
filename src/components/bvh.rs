#![allow(dead_code)]

use crate::math::BoundingBox;

pub mod avl;

#[derive(Clone, Copy, Debug)]
struct Leaf<T> {
    bb: BoundingBox,
    height: i32,
    entry: T,
}

#[derive(Clone, Copy, Debug)]
struct Node {
    bb: BoundingBox,
    height: i32,
    left: usize,
    right: usize,
}

#[derive(Clone, Copy, Debug)]
enum Aabb<T> {
    Leaf(Leaf<T>),
    Node(Node),
}

#[derive(Debug)]
pub enum Error {
    CorruptTree,
}

impl std::convert::From<Error> for std::option::NoneError {
    #[inline]
    fn from(_: Error) -> Self {
        std::option::NoneError
    }
}

impl std::convert::From<std::option::NoneError> for Error {
    #[inline]
    fn from(_: std::option::NoneError) -> Self {
        Error::CorruptTree
    }
}

pub type Result<T> = std::result::Result<T, Error>;

impl<T> Aabb<T> {
    fn bb(&self) -> &BoundingBox {
        match self {
            Aabb::Leaf(ref leaf) => &leaf.bb,
            Aabb::Node(ref node) => &node.bb,
        }
    }

    fn height(&self) -> i32 {
        match self {
            Aabb::Leaf(ref leaf) => leaf.height,
            Aabb::Node(ref node) => node.height,
        }
    }
}

/// Axis-aligned bounding box tree that is currently insert-only and unbalanced.
// In no particular order:
// @TODO: Add ability to cast rays and find all intersecting entities.
// @TODO: Add ability to give a different kind of bounding volume to find all bounding boxes that
//        intersect with the shape.
// @TODO: Add ability to remove nodes.
// @TODO: Consider holding onto the geometry here as well and returning an identifier to get back
//        to it to help with deletions and querying.
#[derive(Debug)]
pub struct Tree<T> {
    /// The memory used to hold the nodes/leaves in the tree.
    tree: Vec<Option<Aabb<T>>>,

    /// The number of leaf nodes in the tree.
    len: usize,
}

impl<T: Clone> Tree<T> {
    fn grow_if_needed(&mut self) {
        // We need count_of(leaf) * 2 - 1 total nodes for the BST along with an extra row in case
        // we have an initial insert that causes the tree to be temporarily unbalanced.
        let min_size = (self.len * 2 - 1) + (self.len * 2);

        // Grow if we're not big enough.
        if min_size >= self.tree.len() {
            self.tree.resize(min_size * 2, None);
        }
    }

    fn safe_insert(&mut self, index: usize, aabb: Aabb<T>) {
        if self.tree.len() < index {
            self.tree.resize(index * 3 / 2, None);
        }

        self.tree[index] = Some(aabb);
    }

    /// Creates a new AABB tree.
    pub fn new() -> Self {
        // Start with 128 slots.
        // @NOTE: The initial size of the tree must be greater than 0 for our resize method.
        let tree = std::iter::repeat(None).take(128).collect();

        Self { tree, len: 0 }
    }

    fn move_nodes(&mut self, mut chain: Vec<(usize, Aabb<T>)>) -> Result<()> {
        while let Some((new_location, node)) = chain.pop() {
            match node {
                Aabb::Leaf(leaf) => self.safe_insert(new_location, Aabb::Leaf(leaf)),

                Aabb::Node(mut node) => {
                    let left_child = self.tree[node.left].take()?;
                    let right_child = self.tree[node.right].take()?;

                    node.left = new_location * 2 + 1;
                    node.right = new_location * 2 + 2;

                    chain.push((node.left, left_child));
                    chain.push((node.right, right_child));

                    self.safe_insert(new_location, Aabb::Node(node));
                }
            }
        }

        Ok(())
    }

    /// Rotates the (left) child of the parent to the right. This will make the parent become the
    /// right child of the (old) child. The parent will be put back into the tree after this
    /// operation completes, while the (old) child will not in case there is another rotation to
    /// make.
    fn rotate_right(&mut self, mut parent: Node, child: &mut Node) -> Result<()> {
        let parent_right = self.tree[parent.right].take()?;
        let child_right = self.tree[child.right].take()?;
        let child_left = self.tree[child.left].take()?;

        let old_parent_bb = parent_right.bb().combine(child_right.bb());

        child.bb = old_parent_bb.combine(child_left.bb());
        child.left = parent.left;
        child.right = parent.right;

        let new_parent_location = parent.right;
        parent.bb = old_parent_bb;
        parent.left = new_parent_location * 2 + 1;
        parent.right = new_parent_location * 2 + 2;

        parent.height = std::cmp::max(child_right.height(), parent_right.height()) + 1;
        child.height = std::cmp::max(child_left.height(), parent.height) + 1;

        self.tree[child.right] = Some(Aabb::Node(parent));

        // The moves we need to make. (new_location, node)
        // These are backwards so they can be popped off the stack in the correct order.
        let chain = vec![
            (child.left, child_left),
            (parent.left, child_right),
            (parent.right, parent_right),
        ];
        self.move_nodes(chain)
    }

    fn rotate_left(&mut self, mut parent: Node, child: &mut Node) -> Result<()> {
        let parent_left = self.tree[parent.left].take()?;
        let child_left = self.tree[child.left].take()?;
        let child_right = self.tree[child.right].take()?;

        let old_parent_bb = child_left.bb().combine(&parent_left.bb());

        child.bb = old_parent_bb.combine(&child_right.bb());
        child.left = parent.left;
        child.right = parent.right;

        let new_parent_location = child.left;
        parent.bb = old_parent_bb;
        parent.left = new_parent_location * 2 + 1;
        parent.right = new_parent_location * 2 + 2;

        parent.height = std::cmp::max(child_left.height(), parent_left.height()) + 1;
        child.height = std::cmp::max(child_right.height(), parent.height) + 1;

        self.tree[child.left] = Some(Aabb::Node(parent));

        // The moves we need to make. (new_location, node)
        // These are backwards so they can be popped off the stack in the correct order.
        let chain = vec![
            (child.right, child_right),
            (parent.right, child_left),
            (parent.left, parent_left),
        ];
        self.move_nodes(chain)
    }

    fn rebalance(&mut self, mut family: Vec<usize>) -> Result<&mut Self> {
        // Start at the current node's parent and move up the tree.
        // We know that a single node is balanced and so we need to look to the grandparent.
        // We also know that we will only be visiting nodes as well.
        // We won't actually use the direct parent's id and, instead, rely on the grandparent.
        while let Some(parent_id) = family.pop() {
            match self.tree[parent_id].take()? {
                Aabb::Node(mut parent) => {
                    let left_child = self.tree[parent.left].take()?;
                    let right_child = self.tree[parent.right].take()?;
                    let balance = left_child.height() - right_child.height();

                    parent.height = std::cmp::max(left_child.height(), right_child.height()) + 1;

                    if balance > 1 {
                        self.tree[parent.right] = Some(right_child);

                        if let Aabb::Node(mut left_child) = left_child {
                            match self.tree[left_child.right].take()? {
                                Aabb::Leaf(leaf) => {
                                    self.tree[left_child.right] = Some(Aabb::Leaf(leaf));
                                    return Err(Error::CorruptTree);
                                }

                                Aabb::Node(mut right_grandchild) => {
                                    let left_child_balance = self
                                        .tree
                                        .get(left_child.left)
                                        .and_then(|left_grandchild| {
                                            left_grandchild.as_ref().map(|left_grandchild| {
                                                left_grandchild.height() - right_grandchild.height
                                            })
                                        })?;

                                    if left_child_balance < 0 {
                                        self.rotate_left(left_child, &mut right_grandchild)?;
                                        self.rotate_right(parent, &mut right_grandchild)?;
                                        self.tree[parent_id] = Some(Aabb::Node(right_grandchild));
                                    } else {
                                        self.tree[left_child.right] =
                                            Some(Aabb::Node(right_grandchild));
                                        self.rotate_right(parent, &mut left_child)?;
                                        self.tree[parent_id] = Some(Aabb::Node(left_child));
                                    }
                                }
                            }
                        }
                    } else if balance < -1 {
                        self.tree[parent.left] = Some(left_child);

                        if let Aabb::Node(mut right_child) = right_child {
                            match self.tree[right_child.left].take()? {
                                Aabb::Leaf(leaf) => {
                                    self.tree[right_child.left] = Some(Aabb::Leaf(leaf));
                                    return Err(Error::CorruptTree);
                                }

                                Aabb::Node(mut left_grandchild) => {
                                    let right_child_balance = self
                                        .tree
                                        .get(right_child.right)
                                        .and_then(|right_grandchild| {
                                            right_grandchild.as_ref().map(|right_grandchild| {
                                                left_grandchild.height - right_grandchild.height()
                                            })
                                        })?;

                                    if right_child_balance > 0 {
                                        self.rotate_right(right_child, &mut left_grandchild)?;
                                        self.rotate_left(parent, &mut left_grandchild)?;
                                        self.tree[parent_id] = Some(Aabb::Node(left_grandchild));
                                    } else {
                                        self.tree[right_child.left] =
                                            Some(Aabb::Node(left_grandchild));
                                        self.rotate_left(parent, &mut right_child)?;
                                        self.tree[parent_id] = Some(Aabb::Node(right_child));
                                    }
                                }
                            }
                        }
                    } else {
                        // Put everything back
                        self.tree[parent.left] = Some(left_child);
                        self.tree[parent.right] = Some(right_child);
                        self.tree[parent_id] = Some(Aabb::Node(parent));
                    }
                }

                leaf => {
                    self.tree[parent_id] = Some(leaf);
                    return Err(Error::CorruptTree);
                }
            }
        }

        Ok(self)
    }

    /// Inserts a BoundingBox into the tree for a given entity.
    pub fn insert(&mut self, bb: BoundingBox, entry: T) -> Result<&mut Self> {
        self.len += 1;

        // Make sure there's enough room to append.
        self.grow_if_needed();

        // Keep track of the inserted node's family tree so we can rebalance.
        let mut family = vec![];

        // Create a new leaf to insert. All leaves have height of 0.
        let new_leaf = Leaf {
            bb,
            entry,
            height: 0,
        };
        let mut current_node = 0; // Start at the root.

        loop {
            match self.tree[current_node].take() {
                // Empty tree, just insert a leaf node.
                None if current_node == 0 => {
                    self.tree[current_node] = Some(Aabb::Leaf(new_leaf));

                    break; // We're done
                }

                // Corrupt tree, should never get here.
                None => unreachable!(),

                // We have a leaf so make a new parent node for the leaf we're inserting and the existing one.
                Some(Aabb::Leaf(existing_leaf)) => {
                    let bb = existing_leaf.bb.combine(&new_leaf.bb);
                    let left = current_node * 2 + 1;
                    let right = current_node * 2 + 2;
                    let height = 1;
                    let new_node = Node {
                        bb,
                        left,
                        right,
                        height,
                    };

                    self.safe_insert(new_node.left, Aabb::Leaf(existing_leaf));
                    self.safe_insert(new_node.right, Aabb::Leaf(new_leaf));
                    self.tree[current_node] = Some(Aabb::Node(new_node));

                    break; // We're done
                }

                // Found a node, now determine if we should continue down the left or right child.
                Some(Aabb::Node(mut node)) => {
                    let left = self.tree[node.left].take()?;
                    let right = self.tree[node.right].take()?;

                    // Keep track of where we currently are so we can put the node back.
                    let parent = current_node;

                    // Push the current node to the family tree.
                    family.push(current_node);

                    // Determine which side we should go down.
                    let left_perimeter = left.bb().perimeter();
                    let right_perimeter = right.bb().perimeter();

                    if left_perimeter < right_perimeter {
                        node.bb = left.bb().combine(&new_leaf.bb);
                        current_node = node.left;
                    } else {
                        node.bb = right.bb().combine(&new_leaf.bb);
                        current_node = node.right;
                    };

                    // Put the current node back since we won't change it.
                    self.tree[node.left] = Some(left);
                    self.tree[node.right] = Some(right);
                    self.tree[parent] = Some(Aabb::Node(node));
                }
            }
        }

        self.rebalance(family)
    }

    /// Finds all entities in the tree whose bounding boxes intersect the target bounding box.
    pub fn find_overlapping(&self, target: &BoundingBox) -> Vec<&T> {
        let mut to_check = vec![0]; // Start at the root node and work our way down the tree.
        let mut found = vec![];

        while let Some(search) = to_check.pop() {
            match &self.tree[search] {
                // We found an overlapping leaf node.
                Some(Aabb::Leaf(leaf)) if leaf.bb.overlaps(target) => {
                    found.push(&leaf.entry);
                }

                // No overlap
                Some(Aabb::Leaf(_)) => (),

                // We found an overlapping node. Check left and right child nodes.
                Some(Aabb::Node(node)) if node.bb.overlaps(target) => {
                    to_check.push(node.right);
                    to_check.push(node.left);
                }

                // No overlap
                Some(Aabb::Node(_)) => (),

                // We should never get to this state. It means our tree is corrupt!
                None => unreachable!(),
            }
        }

        found
    }

    pub fn iter(&self) -> impl Iterator<Item = &BoundingBox> {
        self.tree.iter().filter_map(|s| match s {
            Some(node) => Some(node.bb()),
            None => None,
        })
    }
}

#[cfg(test)]
mod tree_tests {
    use super::Tree;
    use crate::array::GenerationalIndexAllocator;
    use crate::math::{BoundingBox, Vec3};

    #[test]
    fn can_insert_into_empty_tree() {
        let index = GenerationalIndexAllocator::with_capacity(1).allocate();
        let bb = BoundingBox::wrap([Vec3::ZERO, Vec3::new(1., 1., 1.)].iter());
        Tree::new().insert(bb, index).unwrap();
    }

    #[test]
    fn can_insert_two_items_into_empty_tree() {
        let mut allocator = GenerationalIndexAllocator::with_capacity(2);
        let (i_1, i_2) = (allocator.allocate(), allocator.allocate());
        let bb_1 = BoundingBox::wrap([Vec3::ZERO, Vec3::new(1., 1., 1.)].iter());
        let bb_2 = BoundingBox::wrap([Vec3::new(1., 1., 1.), Vec3::new(2., 2., 2.)].iter());
        let mut tree = Tree::new();
        tree.insert(bb_1, i_1).unwrap().insert(bb_2, i_2).unwrap();
    }

    #[test]
    fn can_insert_multiple_items_into_empty_tree() {
        let test_size = 4096;
        let mut allocator = GenerationalIndexAllocator::with_capacity(test_size);
        let mut tree = Tree::new();
        for i in 0..test_size {
            let entity = allocator.allocate();
            let min = i as f32;
            let max = min * 2.;
            let bb = BoundingBox::wrap([Vec3::new(min, min, min), Vec3::new(max, max, max)].iter());
            tree.insert(bb, entity).unwrap();
        }
    }

    #[test]
    fn can_find_intersections_from_tree() {
        let mut allocator = GenerationalIndexAllocator::with_capacity(10);
        let entities = std::iter::repeat_with(|| allocator.allocate())
            .take(10)
            .collect::<Vec<_>>();

        let mut tree = Tree::new();
        for entity in entities.iter() {
            let bb = BoundingBox::wrap([Vec3::ZERO, Vec3::new(5., 5., 5.)].iter());
            tree.insert(bb, *entity).unwrap();
        }

        let target_bb = BoundingBox::wrap([Vec3::ZERO, Vec3::new(3., 3., 3.)].iter());
        let found = tree
            .find_overlapping(&target_bb)
            .iter()
            .map(|i| (*i).clone())
            .collect::<Vec<_>>();

        assert_eq!(found[..], entities[..]);

        let target_bb = BoundingBox::wrap([Vec3::ZERO, Vec3::new(10., 10., 10.)].iter());
        let found = tree
            .find_overlapping(&target_bb)
            .iter()
            .map(|i| (*i).clone())
            .collect::<Vec<_>>();

        // The entities are reversed.
        assert_eq!(found[..], entities[..]);
    }
}
