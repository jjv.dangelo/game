//! An auto-balancing bounding volume hierarchy (BVH) using an AVL-like tree structure, heavily inspired by
//! (box2d)[https://github.com/erincatto/Box2D/tree/master/Box2D/Collision].

use crate::math::{BoundingBox, Ray};

#[derive(Clone, Copy)]
/// A height-aware node the contains a (fat, axis-aligned) bounding box, and points to its two children.
struct Node {
    /// A bounding box that contains its children.
    bb: BoundingBox,

    /// The height of the node, taken as the max height between the left and right nodes.
    height: i32,

    /// Entry in the tree to the left child.
    left: usize,

    /// Entry in the tree to the right child.
    right: usize,

    /// The Node's parent node.
    parent: Option<usize>,
}

#[derive(Clone, Copy)]
/// A struct that defines a leaf node, its associated bounding box, and its contents.
struct Leaf<T: Copy> {
    /// The Leaf's (axis-aligned) bounding box.
    bb: BoundingBox,

    /// The Leaf's parent node.
    parent: Option<usize>,

    /// The value of the Leaf node.
    entry: T,
}

#[derive(Clone, Copy)]
/// A struct that points to the next free node, if one exists; otherwise None.
struct Empty {
    /// The next free node, if one exists.
    next: Option<usize>,
}

#[derive(Clone, Copy)]
/// An enum that describes the state an entry into the tree can take.
enum Entry<T: Copy> {
    Node(Node),
    Empty(Empty),
    Leaf(Leaf<T>),
}

#[derive(Clone, Copy, Debug)]
/// A new-type that wraps the index into the tree of an inserted Entry::Leaf.
pub struct Index(usize);

/// A tree structure for storing a BVH.
pub struct Tree<T: Copy> {
    /// The number of internal entries in the tree.
    nodes: usize,

    /// The total count of leafs (inserted items) in the tree.
    count: usize,

    /// The entries that comprise the tree.
    tree: Vec<Entry<T>>,

    /// The index to the root node, if it exists.
    root: Option<usize>,

    /// The index, if it exists, to the next free node.
    free: Option<usize>,
}

impl<T: Copy> Entry<T> {
    #[inline]
    fn new_leaf(bb: BoundingBox, entry: T, parent: Option<usize>) -> Self {
        Entry::Leaf(Leaf { bb, entry, parent })
    }

    #[inline]
    fn compute_cost(&self, bb: &BoundingBox, inheritance_cost: f32) -> f32 {
        match self {
            Entry::Empty(_) => panic!("Invalid tree."),

            Entry::Leaf(ref leaf) => {
                let bb = leaf.bb.combine(&bb);
                bb.perimeter() + inheritance_cost
            }

            Entry::Node(ref node) => {
                let bb = node.bb.combine(&bb);
                let old_perimeter = node.bb.perimeter();
                let new_perimeter = bb.perimeter();
                new_perimeter - old_perimeter + inheritance_cost
            }
        }
    }

    #[inline]
    fn parent(&self) -> Option<usize> {
        match self {
            Entry::Empty(_) => panic!("Invalid tree."),

            Entry::Leaf(ref leaf) => leaf.parent,
            Entry::Node(ref node) => node.parent,
        }
    }

    #[inline]
    fn bb(&self) -> &BoundingBox {
        match self {
            Entry::Node(node) => &node.bb,
            Entry::Leaf(leaf) => &leaf.bb,
            Entry::Empty(_) => panic!("No bounding box found for empty node."),
        }
    }

    #[inline]
    fn height(&self) -> i32 {
        match self {
            Entry::Node(node) => node.height,
            Entry::Leaf(_) => 0,
            Entry::Empty(_) => -1,
        }
    }

    #[inline]
    /// Sets the parent of a given node. Panics if the entry is Empty.
    fn set_parent(&mut self, parent: Option<usize>) {
        match self {
            Entry::Leaf(ref mut leaf) => {
                leaf.parent = parent;
            }

            Entry::Node(ref mut node) => {
                node.parent = parent;
            }

            Entry::Empty(_) => panic!("Cannot set parent of empty node."),
        }
    }
}

impl<T: Copy + std::fmt::Debug> std::fmt::Debug for Entry<T> {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Entry::Node(node) => write!(
                fmt,
                "Node\n\tParent: {:?}\n\t{:?}\n\tHeight: {}\tLeft: {}\tRight: {}",
                node.parent, node.bb, node.height, node.left, node.right
            ),

            Entry::Leaf(leaf) => write!(
                fmt,
                "Leaf\n\tParent: {:?}\n\t{:?}\n\tValue: {:?}",
                leaf.parent, leaf.bb, leaf.entry
            ),

            Entry::Empty(empty) => match &empty.next {
                Some(next) => write!(fmt, "Empty\tNext: {}", next),
                None => write!(fmt, "Empty\tEnd"),
            },
        }
    }
}

impl<T: std::fmt::Debug + Copy> std::fmt::Debug for Tree<T> {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(fmt, "Total Nodes: {}", self.nodes)?;
        writeln!(fmt, "Count: {}", self.count)?;
        writeln!(fmt, "Capacity: {}", self.tree.len())?;
        writeln!(fmt, "Root: {:?}", self.root)?;
        writeln!(fmt, "Free: {:?}", self.free)?;

        for (i, e) in self.tree.iter().enumerate() {
            writeln!(fmt, "{:3} {:?}", i, e)?;
        }

        Ok(())
    }
}

impl<T: Copy> Tree<T> {
    /// Creates a new tree.
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    /// Creates a new tree with the given capacity.
    pub fn with_capacity(capacity: usize) -> Self {
        let capacity = capacity * 2 - 1;
        let mut tree = Vec::with_capacity(capacity);
        for i in 0..capacity - 1 {
            tree.push(Entry::Empty(Empty { next: Some(i + 1) }));
        }
        tree.push(Entry::Empty(Empty { next: None }));

        Self {
            nodes: 0,
            count: 0,
            root: None,
            free: Some(0),
            tree,
        }
    }

    /// Allocates a new node, growing the tree if needed.
    fn allocate(&mut self) -> usize {
        self.nodes += 1;

        match self.free.take() {
            Some(index) => {
                match self.tree[index] {
                    Entry::Empty(empty) => {
                        self.free = empty.next;
                    }

                    _ => unreachable!("Invalid tree structure."),
                }

                index
            }

            None => {
                let current_len = self.tree.len();
                let new_len = current_len * 2;
                self.tree.append(
                    &mut (current_len..new_len)
                        .map(|i| Entry::Empty(Empty { next: Some(i + 1) }))
                        .collect(),
                );
                self.tree[new_len - 1] = Entry::Empty(Empty { next: None });
                self.free = Some(current_len + 1);

                current_len
            }
        }
    }

    /// Inserts a new item with its bounding box into the tree.
    pub fn insert(&mut self, entry: T, bb: BoundingBox) -> Index {
        self.count += 1;
        let new_node_id = self.allocate();

        let sibling_id = match self.root {
            // This is the first entry into the tree. Set as root and return.
            None => {
                debug_assert!(
                    self.nodes == 1,
                    "Invalid AVL tree. Inserting at root but have len {}.",
                    self.nodes
                );

                self.root = Some(new_node_id);
                self.tree[new_node_id] = Entry::new_leaf(bb, entry, None);

                return Index(new_node_id);
            }

            // Walk the tree to find the cheapest sibling next to which we should insert.
            Some(root_id) => {
                let mut index = root_id;
                loop {
                    match &self.tree[index] {
                        Entry::Node(ref node) => {
                            let current_perimeter = node.bb.perimeter();
                            let combined = node.bb.combine(&bb);
                            let combined_perimeter = combined.perimeter();
                            let cost = 2. * combined_perimeter;
                            let inheritance_cost = 2. * (combined_perimeter - current_perimeter);

                            let l_child_cost =
                                self.tree[node.left].compute_cost(&bb, inheritance_cost);
                            let r_child_cost =
                                self.tree[node.right].compute_cost(&bb, inheritance_cost);

                            if cost < l_child_cost && cost < r_child_cost {
                                break index;
                            } else if l_child_cost < r_child_cost {
                                index = node.left
                            } else {
                                index = node.right
                            }
                        }

                        Entry::Leaf(_) => {
                            break index;
                        }

                        Entry::Empty(_) => panic!("Invalid tree structure!"),
                    }
                }
            }
        };

        let old_parent = self.tree[sibling_id].parent();

        let new_parent_id = self.allocate();
        let new_parent = Node {
            bb: bb,    // This will be set when we walk back up the tree.
            height: 0, // This will also be set when we walk back up the tree.
            left: sibling_id,
            right: new_node_id,
            parent: old_parent,
        };

        self.set_parent(sibling_id, Some(new_parent_id));
        self.tree[new_node_id] = Entry::new_leaf(bb, entry, Some(new_parent_id));

        // Point the old parent to the new node, if the sibling wasn't the root node.
        match old_parent {
            // The sibling was the root node.
            None => self.root = Some(new_parent_id),

            // The sibling has a parent, so it isn't the root node.
            Some(old_parent_id) => self.update_child(old_parent_id, sibling_id, new_parent_id),
        }

        self.tree[new_parent_id] = Entry::Node(new_parent);

        // Walk back up the tree and fix heights
        let mut next = new_parent_id;
        loop {
            let (left_id, right_id) = self.get_children(next);
            let height = self.calc_height(left_id, right_id);
            let bb = self.calc_bb(left_id, right_id);
            let mut node = self.get_node_mut(next);
            node.height = height;
            node.bb = bb;

            let index = self.balance(next);
            match self.tree[index].parent() {
                Some(parent_id) => next = parent_id,
                None => break Index(new_node_id),
            }
        }
    }

    // @TODO: We could change this to be a generational index and make multiple deletes of the same
    // generational index safe.
    pub fn remove(&mut self, Index(index): Index) {
        let parent = self.get_parent_id(index);
        self.empty_node(index);
        self.count -= 1;

        // We need to remove the parent and rotate the sibling into the parent's location, then
        // rebalance.
        if let Some(parent_id) = parent {
            match self.tree[parent_id] {
                Entry::Node(parent) => {
                    let sibling_id = if parent.left == index {
                        parent.right
                    } else {
                        debug_assert_eq!(parent.right, index);
                        parent.left
                    };

                    match parent.parent {
                        // We have a grandparent and so we need to move the sibling to the parent's
                        // location.
                        Some(grandparent_id) => {
                            self.replace_child(parent_id, sibling_id);
                            self.set_parent(sibling_id, Some(grandparent_id));
                            self.empty_node(parent_id);

                            let mut next = grandparent_id;
                            loop {
                                let (left_id, right_id) = self.get_children(next);
                                let height = self.calc_height(left_id, right_id);
                                let bb = self.calc_bb(left_id, right_id);
                                let mut node = self.get_node_mut(next);
                                node.height = height;
                                node.bb = bb;

                                let index = self.balance(next);
                                match self.tree[index].parent() {
                                    Some(parent_id) => next = parent_id,
                                    None => break,
                                }
                            }
                        }

                        // This was the root. Move the sibling to it's position.
                        None => {
                            assert_eq!(self.root, Some(parent_id));
                            self.root = Some(sibling_id);
                            self.set_parent(sibling_id, None);
                            self.empty_node(parent_id);
                        }
                    }
                }

                Entry::Leaf(_) | Entry::Empty(_) => unreachable!(),
            }
        } else {
            // This must be the root node.
            assert_eq!(self.root, Some(index));
            self.root = None;
        }
    }

    fn query<C, F>(&self, bb: BoundingBox, check: C, mut callback: F)
    where
        C: Fn(&BoundingBox, &BoundingBox) -> bool,
        F: FnMut(&T) -> bool,
    {
        if let Some(root_id) = self.root {
            let mut nodes = vec![root_id];

            while let Some(node_id) = nodes.pop() {
                match &self.tree[node_id] {
                    Entry::Leaf(ref leaf) if check(&bb, &leaf.bb) => {
                        if !callback(&leaf.entry) {
                            return;
                        }
                    }

                    Entry::Node(ref node) if bb.overlaps(&node.bb) => {
                        nodes.push(node.right);
                        nodes.push(node.left);
                    }

                    _ => {}
                }
            }
        }
    }

    /// Walks through the tree to find entries that overlap with the supplied (axis-aligned)
    /// bounding box.
    pub fn query_overlaps<F>(&self, bb: BoundingBox, callback: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.query(bb, |left, right| left.overlaps(right), callback)
    }

    /// Walks through the tree to find entries that are contained by the supplied (axis-aligned)
    /// bounding box.
    pub fn query_contains<F>(&self, bb: BoundingBox, callback: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.query(bb, |left, right| left.contains(right), callback)
    }

    /// Casts a ray to see if any of the bounding boxes are hit.
    pub fn cast<F>(&self, ray: Ray, mut callback: F)
    where
        F: FnMut(&T, f32) -> bool,
    {
        if let Some(root_id) = self.root {
            let mut nodes = vec![root_id];

            while let Some(node_id) = nodes.pop() {
                match &self.tree[node_id] {
                    Entry::Leaf(ref leaf) => {
                        if let Some(tmax) = leaf.bb.intersects(ray) {
                            if !callback(&leaf.entry, tmax) {
                                return;
                            }
                        }
                    }

                    Entry::Node(ref node) => {
                        if let Some(_) = node.bb.intersects(ray) {
                            nodes.push(node.right);
                            nodes.push(node.left);
                        }
                    }

                    _ => {}
                }
            }
        }
    }

    /// Casts a ray and picks the closest target hit.
    pub fn cast_closest(&self, ray: Ray) -> Option<(&T, f32)> {
        let mut found = None;

        if let Some(root_id) = self.root {
            let mut nodes = vec![root_id];

            while let Some(node_id) = nodes.pop() {
                match &self.tree[node_id] {
                    Entry::Leaf(ref leaf) => {
                        if let Some(tmax) = leaf.bb.intersects(ray) {
                            match found {
                                None => found = Some((&leaf.entry, tmax)),
                                Some((_, current)) if current > tmax => {
                                    found = Some((&leaf.entry, tmax))
                                }
                                _ => (),
                            }
                        }
                    }

                    Entry::Node(ref node) => {
                        if let Some(_) = node.bb.intersects(ray) {
                            nodes.push(node.right);
                            nodes.push(node.left);
                        }
                    }

                    _ => {}
                }
            }
        }

        found
    }

    fn calc_height(&self, left: usize, right: usize) -> i32 {
        let left_height = self.tree[left].height();
        let right_height = self.tree[right].height();
        let result = 1 + std::cmp::max(left_height, right_height);
        result
    }

    fn calc_balance(&self, left: usize, right: usize) -> i32 {
        let left_height = self.tree[left].height();
        let right_height = self.tree[right].height();
        let result = left_height - right_height;
        result
    }

    fn calc_bb(&self, left: usize, right: usize) -> BoundingBox {
        let left_bb = self.tree[left].bb();
        let right_bb = self.tree[right].bb();
        let result = left_bb.combine(right_bb);
        result
    }

    fn set_parent(&mut self, node_id: usize, parent: Option<usize>) {
        let node = &mut self.tree[node_id];
        node.set_parent(parent);
    }

    fn get_parent_id(&mut self, node_id: usize) -> Option<usize> {
        match &self.tree[node_id] {
            Entry::Empty(_) => None,
            Entry::Node(ref node) => node.parent,
            Entry::Leaf(ref leaf) => leaf.parent,
        }
    }

    /// Replaces the target child (old_child) with the new child (new_child). Panics if the old
    /// child isn't actually a child of parent.
    fn update_child(&mut self, parent: usize, old_child: usize, new_child: usize) {
        let parent = self.get_node_mut(parent);
        if parent.left == old_child {
            parent.left = new_child;
        } else {
            if parent.right != old_child {
                debug_assert_eq!(parent.right, old_child);
            }

            parent.right = new_child;
        }
    }

    #[allow(dead_code)]
    /// Updates the child's parent node to point to the new child.
    fn replace_child(&mut self, child_id: usize, new_child_id: usize) {
        match self.tree[child_id].parent() {
            Some(parent) => self.update_child(parent, child_id, new_child_id),
            None => match self.root {
                Some(root_id) if root_id == child_id => {
                    self.root = Some(new_child_id);
                }

                Some(_) => panic!("Node doesn't have a parent and doesn't match the root."),
                None => panic!("Invalid tree. No root node."),
            },
        }
    }

    /// Get a mutable reference to a Node. Panics if entry is not an Entry::Node.
    fn get_node_mut(&mut self, node_id: usize) -> &mut Node {
        match &mut self.tree[node_id] {
            Entry::Node(ref mut node) => node,
            Entry::Leaf(_) => panic!("Expected Node at {}; found Leaf.", node_id),
            Entry::Empty(_) => panic!("Expected Node at {}; found Empty.", node_id),
        }
    }

    /// Get a reference to a Node. Panics if entry is not an Entry::Node.
    fn get_node_ref(&self, node_id: usize) -> &Node {
        match &self.tree[node_id] {
            Entry::Node(ref node) => node,
            Entry::Leaf(_) => panic!("Expected Node at {}; found Leaf.", node_id),
            Entry::Empty(_) => panic!("Expected Node at {}; found Empty.", node_id),
        }
    }

    fn get_children(&self, node_id: usize) -> (usize, usize) {
        let node = self.get_node_ref(node_id);
        (node.left, node.right)
    }

    /// Changes the node to an Empty node. Does not move children!
    fn empty_node(&mut self, node_id: usize) {
        let empty = Empty { next: self.free };
        self.tree[node_id] = Entry::Empty(empty);
        self.free = Some(node_id);
        self.nodes -= 1;
    }

    /// Rotates a given node to the left, moving its right child to its place.
    ///
    ///   P            R
    /// L   R   =>   P   B
    ///    A B      L A
    ///
    /// Returns the new parent id (R).
    fn rotate_left(&mut self, node_id: usize) -> usize {
        let (left_id, right_id) = self.get_children(node_id);
        let (left_grandchild_id, right_grandchild_id) = self.get_children(right_id);

        let parent = self.tree[node_id].parent();
        let new_height = self.calc_height(left_id, left_grandchild_id);
        let new_bb = self.calc_bb(left_id, left_grandchild_id);

        self.set_parent(left_grandchild_id, Some(node_id));
        self.set_parent(node_id, Some(right_id));

        let node = self.get_node_mut(node_id);
        node.right = left_grandchild_id;
        node.height = new_height;
        node.bb = new_bb;

        // Update the node's parent to point to the right child.
        match parent {
            // The node isn't the root, so we just update the parent.
            Some(grandparent) => self.update_child(grandparent, node_id, right_id),

            // The node doesn't have a parent, so is likely the root node.
            None => match self.root {
                Some(root_id) if root_id == node_id => {
                    self.root = Some(right_id);
                }

                Some(_) => panic!("Node doesn't have a parent, and doesn't match the root."),

                // We don't have a root node, but we're working with nodes.
                None => panic!("Invalid tree. No root node."),
            },
        }

        // Set the right child's parent to be the node's parent;
        let right_height = self.calc_height(node_id, right_grandchild_id);
        let new_bb = self.calc_bb(node_id, right_grandchild_id);
        let right = self.get_node_mut(right_id);
        right.parent = parent;
        right.height = right_height;
        right.bb = new_bb;

        // Move the left grandchild to the node's right child;
        right.left = node_id;

        right_id
    }

    /// Rotates a given node to the right, moving its left child to its place.
    ///
    ///      P            L
    ///    L   R   =>   A   P
    ///   A B              B R
    ///
    /// Returns the new parent id (L).
    fn rotate_right(&mut self, node_id: usize) -> usize {
        let (left_id, right_id) = self.get_children(node_id);
        let (left_grandchild_id, right_grandchild_id) = self.get_children(left_id);

        let parent = self.tree[node_id].parent();
        let new_height = self.calc_height(right_grandchild_id, right_id);
        let new_bb = self.calc_bb(right_grandchild_id, right_id);

        self.set_parent(right_grandchild_id, Some(node_id));
        self.set_parent(node_id, Some(left_id));

        let node = self.get_node_mut(node_id);
        node.left = right_grandchild_id;
        node.height = new_height;
        node.bb = new_bb;

        // Update the node's parent to point to the right child.
        match parent {
            // The node isn't the root, so we just update the parent.
            Some(grandparent) => self.update_child(grandparent, node_id, left_id),

            // The node doesn't have a parent, so is likely the root node.
            None => match self.root {
                Some(root_id) if root_id == node_id => {
                    self.root = Some(left_id);
                }

                Some(_) => panic!("Node doesn't have a parent, and doesn't match the root."),

                // We don't have a root node, but we're working with nodes.
                None => panic!("Invalid tree. No root node."),
            },
        }

        // Set the right child's parent to be the node's parent;
        let left_height = self.calc_height(left_grandchild_id, node_id);
        let new_bb = self.calc_bb(left_grandchild_id, node_id);
        let left = self.get_node_mut(left_id);
        left.parent = parent;
        left.height = left_height;
        left.bb = new_bb;

        // Move the left grandchild to the node's right child;
        left.right = node_id;

        left_id
    }

    /// Balances the current node and returns the new parent if there is a rotation.
    fn balance(&mut self, parent_id: usize) -> usize {
        println!("Balancing: {:3}", parent_id);
        let (left_id, right_id) = self.get_children(parent_id);
        let balance = self.calc_balance(left_id, right_id);

        // Rotate parent to the right
        if balance > 1 {
            debug_assert_eq!(balance, 2);

            // Check to see if we need to rotate the left child first.
            let (left_grandchild_id, right_grandchild_id) = self.get_children(left_id);
            let left_balance = self.calc_balance(left_grandchild_id, right_grandchild_id);
            if left_balance < 0 {
                self.rotate_left(left_id);
            }

            self.rotate_right(parent_id)
        }
        // rotate parent to the left
        else if balance < -1 {
            debug_assert_eq!(balance, -2);

            // Check to see if we need to rotate the right child first.
            let (left_grandchild_id, right_grandchild_id) = self.get_children(right_id);
            let right_balance = self.calc_balance(left_grandchild_id, right_grandchild_id);
            if right_balance > 0 {
                self.rotate_right(right_id);
            }

            self.rotate_left(parent_id)
        }
        // No rotation needed; update bounding box, height, and then return the parent.
        else {
            parent_id
        }
    }

    /// Gets the number of items that have been added to the tree.
    pub fn len(&self) -> usize {
        self.count
    }
}

#[cfg(test)]
mod tree_tests {
    use super::Tree;
    use crate::math::{BoundingBox, Vec3};

    #[test]
    fn can_insert_into_a_new_tree() {
        let bb = BoundingBox::wrap([Vec3::ZERO, Vec3::new(1., 1., 1.)].iter());
        let mut tree = Tree::new();
        let result = tree.insert((), bb);

        assert_eq!(Some(result.0), tree.root);
    }

    #[test]
    fn can_insert_two_items_into_new_tree() {
        let bb_1 = BoundingBox::wrap([Vec3::ZERO, Vec3::new(1., 1., 1.)].iter());
        let bb_2 = BoundingBox::wrap([Vec3::new(1., 1., 1.), Vec3::new(2., 2., 2.)].iter());

        let mut tree = Tree::new();

        let result = tree.insert((), bb_1);
        assert_eq!(Some(result.0), tree.root);

        let _result = tree.insert((), bb_2);
    }

    #[test]
    fn can_insert_four_items_into_new_tree() {
        let bb_1 = BoundingBox::wrap([Vec3::ZERO, Vec3::new(1., 1., 1.)].iter());
        let bb_2 = BoundingBox::wrap([Vec3::new(1., 1., 1.), Vec3::new(2., 2., 2.)].iter());
        let bb_3 = BoundingBox::wrap([Vec3::new(2., 2., 2.), Vec3::new(3., 3., 3.)].iter());
        let bb_4 = BoundingBox::wrap([Vec3::new(3., 3., 3.), Vec3::new(4., 4., 4.)].iter());

        let mut tree = Tree::new();

        let result = tree.insert((), bb_1);
        assert_eq!(Some(result.0), tree.root);

        let _result = tree.insert((), bb_2);
        let _result = tree.insert((), bb_3);
        let _result = tree.insert((), bb_4);
    }
}
