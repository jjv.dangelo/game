#![allow(dead_code)]

mod huffman;

use self::huffman::HuffmanTable;
use crate::stream::ByteStream;

const COMPRESSION_NONE: u32 = 0b00;
const COMPRESSION_FIXED_CODES: u32 = 0b01;
const COMPRESSION_DYN_CODES: u32 = 0b10;
const COMPRESSION_ERROR: u32 = 0b11;

pub fn reverse(data: Vec<u8>) -> Option<Vec<u8>> {
    let mut stream = ByteStream::new(data);

    let cmf = stream.read_byte()?;
    let flg = stream.read_byte()?;

    let cm = cmf & 0x0F;

    if cm != 8 {
        return None;
    }

    let fdict = flg & 0b0010_0000;
    if fdict != 0 {
        return None;
    }

    let mut output = Vec::new();

    let mut is_final = 0;
    while is_final == 0 {
        is_final = stream.read_bits(1)?;
        let compression_method = stream.read_bits(2)?;

        let (lit, dist) = match compression_method {
            COMPRESSION_NONE => {
                stream.flush();
                let len = stream.read_bits(16)? as u16;
                let nlen = stream.read_bits(16)? as u16;

                if len != !nlen {
                    return None;
                }

                for _ in 0..len {
                    output.push(stream.read()?);
                }

                continue;
            }

            COMPRESSION_FIXED_CODES => {
                let bit_counts = [(143, 8), (255, 9), (279, 7), (287, 8)];

                let mut index = 0;
                let mut lens = [0; 288];
                for &(max, bits) in bit_counts.iter() {
                    while index <= max {
                        lens[index] = bits;
                    }
                    index = max + 1;
                }

                let lit = HuffmanTable::new(&lens[..])?;

                let dists = [5; 32];
                let dist = HuffmanTable::new(&dists[..])?;
                (lit, dist)
            }

            COMPRESSION_DYN_CODES => {
                let hlit = stream.read_bits(5)? as usize + 257;
                let hdist = stream.read_bits(5)? as usize + 1;
                let hclen = stream.read_bits(4)? as usize + 4;

                if hlit > 286 {
                    return None;
                }
                if hdist > 32 {
                    return None;
                }
                if hclen > 19 {
                    return None;
                }

                let dict = {
                    const ORDER_LEN: usize = 19;
                    let order: [usize; ORDER_LEN] = [
                        16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15,
                    ];

                    let mut hclen_table = [0; ORDER_LEN];
                    for i in 0..hclen {
                        hclen_table[order[i]] = stream.read_bits(3)?;
                    }

                    HuffmanTable::new(&hclen_table[..])?
                };

                let mut count = 0;
                let mut lens = Vec::with_capacity(hlit + hdist);
                while count < (hlit + hdist) {
                    let value = dict.decode(&mut stream)?;
                    match value {
                        0...15 => {
                            lens.push(value);
                            count += 1;
                        }
                        16 => {
                            let len = stream.read_bits(2)? + 3;
                            let value = *lens.last()?;

                            for _ in 0..len {
                                lens.push(value);
                                count += 1;
                            }
                        }
                        17 => {
                            let len = stream.read_bits(3)? + 3;
                            for _ in 0..len {
                                lens.push(0);
                                count += 1;
                            }
                        }
                        18 => {
                            let len = stream.read_bits(7)? + 11;
                            for _ in 0..len {
                                lens.push(0);
                                count += 1;
                            }
                        }
                        _ => unreachable!(),
                    }
                }

                let lit = HuffmanTable::new(&lens[..hlit])?;

                assert_eq!(lens[hlit..].len(), hdist);
                let dist = HuffmanTable::new(&lens[hlit..])?;
                (lit, dist)
            }

            COMPRESSION_ERROR => {
                return None;
            }

            _ => unreachable!(),
        };

        inflate(&mut stream, lit, dist, &mut output)?;
    }

    Some(output)
}

fn inflate<I: IntoIterator<Item = u8>>(
    stream: &mut ByteStream<I>,
    lit: HuffmanTable,
    dist: HuffmanTable,
    output: &mut Vec<u8>,
) -> Option<()> {
    let lens = [
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115,
        131, 163, 195, 227, 258,
    ];

    let len_extra = [
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0,
    ];

    let dists = [
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537,
        2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577,
    ];

    let dist_extra = [
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12,
        13, 13,
    ];

    loop {
        let code = lit.decode(stream)?;
        match code {
            0...255 => {
                output.push(code as u8);
            }

            256 => {
                break;
            }

            257...285 => {
                let len_index = code as usize - 257;
                let len_bits = len_extra[len_index];
                let len = lens[len_index] + stream.read_bits(len_bits)?;

                let dist_index = dist.decode(stream)? as usize;
                let dist_bits = dist_extra[dist_index];
                let distance = dists[dist_index] + stream.read_bits(dist_bits)?;

                let start = output.len() - distance as usize;
                let end = start + len as usize;

                for i in start..end {
                    let byte = output[i];
                    output.push(byte);
                }
            }

            _ => {
                unreachable!();
            }
        }
    }

    Some(())
}
