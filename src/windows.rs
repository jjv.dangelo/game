#![cfg(windows)]

extern crate winapi;

#[macro_use]
mod win32;
mod com;
mod renderer;

use game::{
    assets::AssetManager,
    clock::Clock,
    controls::{Controller, Keyboard, Mouse},
    platform::{GameInput, GameOutput, PlatformApi},
    renderer::RenderCommands,
    GameState,
};

use self::win32::{
    create_wnd, dispatch_msg, get_async_key_state, get_clock_freq, get_wall_clock, get_wheel_delta,
    peek_msg, post_quit_msg, register_class, release_capture, set_capture, show_wnd, translate_msg,
    Measurable, Window,
};

use self::winapi::{
    shared::{
        windef::HWND,
        windowsx::{GET_X_LPARAM, GET_Y_LPARAM},
    },
    um::winuser::{
        MK_LBUTTON, MK_RBUTTON, WM_CLOSE, WM_DESTROY, WM_EXITSIZEMOVE, WM_KEYDOWN, WM_KEYUP,
        WM_LBUTTONDOWN, WM_LBUTTONUP, WM_MOUSEMOVE, WM_MOUSEWHEEL, WM_QUIT, WM_RBUTTONDOWN,
        WM_RBUTTONUP, WM_SYSKEYDOWN, WM_SYSKEYUP,
    },
};

use crate::windows::renderer::d3d11::Renderer;

pub struct Platform {
    hwnd: HWND,
    running: bool,
    window_width: u32,
    window_height: u32,
}

impl PlatformApi for Platform {
    fn load_blob(&self, asset_name: &str) -> Option<Vec<u8>> {
        let path: std::path::PathBuf = ["assets", asset_name].iter().collect();
        match std::fs::read(path) {
            Ok(bytes) => Some(bytes),
            Err(_) => None,
        }
    }

    fn load_text(&self, asset_name: &str) -> Option<String> {
        let path: std::path::PathBuf = ["assets", asset_name].iter().collect();
        match std::fs::read_to_string(path) {
            Ok(text) => Some(text),
            Err(_) => None,
        }
    }
}

impl Platform {
    pub fn new() -> Option<Self> {
        let class = register_class("Direct-X-Window-Class");
        let hwnd = create_wnd(class, "DirectX Test");
        if hwnd.is_null() {
            debug!("Unable to create window.\n");
        }

        let client_rect = hwnd.get_client_rect()?;
        let width = client_rect.get_width();
        let height = client_rect.get_height();

        Some(Platform {
            hwnd,
            running: true,
            window_width: width,
            window_height: height,
        })
    }

    fn process_windows_messages(
        &mut self,
        renderer: &mut Renderer,
        mouse: &mut Mouse,
        controller: &mut Controller,
        keyboard: &mut Keyboard,
    ) {
        while let Some(mut msg) = peek_msg(self.hwnd) {
            translate_msg(&mut msg);

            match msg.message {
                WM_LBUTTONDOWN | WM_RBUTTONDOWN => {
                    set_capture(self.hwnd);

                    mouse.x = GET_X_LPARAM(msg.lParam) as _;
                    mouse.y = GET_Y_LPARAM(msg.lParam) as _;
                    mouse.left_button.set_is_down(msg.wParam & MK_LBUTTON != 0);
                    mouse.right_button.set_is_down(msg.wParam & MK_RBUTTON != 0);
                }

                WM_LBUTTONUP | WM_RBUTTONUP => {
                    release_capture();

                    mouse.x = GET_X_LPARAM(msg.lParam) as _;
                    mouse.y = GET_Y_LPARAM(msg.lParam) as _;
                    mouse.left_button.set_is_down(msg.wParam & MK_LBUTTON != 0);
                    mouse.right_button.set_is_down(msg.wParam & MK_RBUTTON != 0);
                }

                WM_MOUSEMOVE => {
                    mouse.x = GET_X_LPARAM(msg.lParam) as _;
                    mouse.y = GET_Y_LPARAM(msg.lParam) as _;
                    mouse.left_button.set_is_down(msg.wParam & MK_LBUTTON != 0);
                    mouse.right_button.set_is_down(msg.wParam & MK_RBUTTON != 0);
                }

                WM_MOUSEWHEEL => {
                    mouse.scroll_delta = get_wheel_delta(msg.wParam);
                }

                WM_KEYDOWN | WM_KEYUP | WM_SYSKEYDOWN | WM_SYSKEYUP => {
                    let was_down = (msg.lParam & (1 << 30)) != 0;
                    let is_down = (msg.lParam & (1 << 31)) == 0;

                    keyboard.control.set_is_down(get_async_key_state(0x11));

                    keyboard.shift.set_is_down(get_async_key_state(0x10));

                    const A: usize = 0x41;

                    // Only check the keys if the press has transitioned from up->down or down->up.
                    if was_down != is_down {
                        match msg.wParam {
                            0x30...0x39 => {
                                let index = (msg.wParam as usize) - 0x30;
                                keyboard.numeric[index].set_is_down(is_down);
                            } // 1-0

                            0x41...0x5A => {
                                let index = (msg.wParam as usize) - A;
                                keyboard.alpha[index].set_is_down(is_down);

                                match msg.wParam {
                                    0x57 => controller.move_up.set_is_down(is_down), // w
                                    0x41 => controller.move_left.set_is_down(is_down), // a
                                    0x53 => controller.move_down.set_is_down(is_down), // s
                                    0x44 => controller.move_right.set_is_down(is_down), // d
                                    0x51 => controller.bumper_left.set_is_down(is_down), // q
                                    0x45 => controller.bumper_right.set_is_down(is_down), // e
                                    _ => {}
                                }
                            } // a-z

                            0x2E => keyboard.delete.set_is_down(is_down), // delete

                            0x26 => {
                                controller.action_up.set_is_down(is_down);
                                keyboard.arrow_up.set_is_down(is_down);
                            } // up arrow

                            0x28 => {
                                controller.action_down.set_is_down(is_down);
                                keyboard.arrow_down.set_is_down(is_down);
                            } // down arrow

                            0x25 => {
                                controller.action_left.set_is_down(is_down);
                                keyboard.arrow_left.set_is_down(is_down);
                            } // left arrow

                            0x27 => {
                                controller.action_right.set_is_down(is_down);
                                keyboard.arrow_right.set_is_down(is_down);
                            } // right arrow

                            0x20 => {
                                controller.select.set_is_down(is_down);
                                keyboard.space.set_is_down(is_down);
                            } // spacebar

                            0x0D => {
                                controller.start.set_is_down(is_down);
                                keyboard.enter.set_is_down(is_down);
                            } // enter

                            0x70...0x7B => {
                                let index = (msg.wParam - 0x70 + 1) as usize;
                                keyboard.f[index].set_is_down(is_down);
                            } // F1 - F12

                            0x1B if is_down => {
                                keyboard.escape.set_is_down(is_down);

                                // @TODO: Move this to the game code.
                                self.running = false;
                                post_quit_msg();
                                break;
                            } // escape

                            _ => (),
                        }
                    }
                }

                WM_EXITSIZEMOVE => {
                    // @TODO: This doesn't seem to work well.
                    if let Some(rect) = self.hwnd.get_client_rect() {
                        let width = rect.get_width();
                        let height = rect.get_height();

                        // @TODO: Error handling...
                        renderer.resize_window(width, height).unwrap();

                        self.window_width = width;
                        self.window_height = height;
                    }
                }

                WM_QUIT | WM_DESTROY | WM_CLOSE => {
                    self.running = false;
                    post_quit_msg();
                    break;
                }

                _ => dispatch_msg(&msg),
            }
        }
    }

    pub fn run(&mut self) -> Option<()> {
        let mut renderer = Renderer::initialize(self.hwnd, self.window_width, self.window_height)?;

        let mut asset_manager = AssetManager::new();
        let mut game_state = GameState::default();
        let mut controller = Controller::default();
        let mut keyboard = Keyboard::default();
        let mut mouse = Mouse::default();
        let mut render_commands = RenderCommands::with_capacity(1024);
        let mut clock = Clock::new(0.01, get_wall_clock, get_clock_freq);

        show_wnd(self.hwnd);
        while self.running {
            // let previous_state = current_state;

            // Using "Fix Your Timestep" loop
            // @TODO: Should the _frame_alpha be used? or just loop and avoid the lerp?
            let _frame_alpha = clock.run_frame(|tick_info| {
                controller.new_frame();
                mouse.new_frame();
                keyboard.new_frame();

                self.process_windows_messages(
                    &mut renderer,
                    &mut mouse,
                    &mut controller,
                    &mut keyboard,
                );

                let game_input = GameInput {
                    tick_info,
                    controller,
                    keyboard,
                    mouse,
                    window_width: self.window_width,
                    window_height: self.window_height,
                };

                match game_state.update_frame(
                    self,
                    &game_input,
                    &mut render_commands,
                    &mut asset_manager,
                ) {
                    GameOutput::Error | GameOutput::Quit => self.running = false,
                    GameOutput::Continue => (),
                }

                renderer.process_commands(tick_info, &mut render_commands, &mut asset_manager);
                render_commands.reset();

                // current_state.integrate(time_info);
            });

            // current_state = current_state.interpolate(previous_state, frame_alpha);
        }

        Some(())
    }
}
