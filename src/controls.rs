#[derive(Clone, Copy, Debug, Default)]
pub struct Button {
    ended_down: bool,
    half_transition_count: u32,
}

impl Button {
    #[inline]
    pub fn was_down(&self) -> bool {
        let was_down =
            self.half_transition_count > 1 || (self.half_transition_count == 1 && self.ended_down);
        was_down
    }

    #[inline]
    pub fn is_down(&self) -> bool {
        self.ended_down
    }

    #[inline]
    pub fn set_is_down(&mut self, is_down: bool) {
        if self.ended_down != is_down {
            self.ended_down = is_down;
            self.half_transition_count += 1;
        }
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct Controller {
    pub move_left: Button,
    pub move_right: Button,
    pub move_down: Button,
    pub move_up: Button,

    pub action_left: Button,
    pub action_right: Button,
    pub action_down: Button,
    pub action_up: Button,

    pub bumper_left: Button,
    pub bumper_right: Button,

    pub select: Button,
    pub start: Button,
}

impl Controller {
    #[inline]
    fn game_buttons(&mut self) -> [&mut Button; 12] {
        [
            &mut self.move_left,
            &mut self.move_right,
            &mut self.move_down,
            &mut self.move_up,
            &mut self.action_left,
            &mut self.action_right,
            &mut self.action_down,
            &mut self.action_up,
            &mut self.bumper_left,
            &mut self.bumper_right,
            &mut self.select,
            &mut self.start,
        ]
    }

    #[inline]
    pub fn new_frame(&mut self) {
        for button in self.game_buttons().iter_mut() {
            button.half_transition_count = 0;
        }
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct Mouse {
    pub x: i32,
    pub y: i32,

    pub scroll_delta: i32,

    pub left_button: Button,
    pub right_button: Button,
}

impl Mouse {
    #[inline]
    pub fn new_frame(&mut self) {
        self.left_button.half_transition_count = 0;
        self.right_button.half_transition_count = 0;
        self.scroll_delta = 0;
    }
}

#[derive(Clone, Copy, Default)]
pub struct Keyboard {
    pub alpha: [Button; 26],
    pub numeric: [Button; 10],
    pub f: [Button; 13],

    pub tab: Button,
    pub tilde: Button,
    pub enter: Button,
    pub space: Button,
    pub escape: Button,
    pub delete: Button,

    pub arrow_up: Button,
    pub arrow_right: Button,
    pub arrow_down: Button,
    pub arrow_left: Button,

    pub shift: Button,
    pub control: Button,
}

impl Keyboard {
    pub fn new_frame(&mut self) {
        for b in self
            .alpha
            .iter_mut()
            .chain(self.numeric.iter_mut())
            .chain(self.f.iter_mut())
        {
            b.half_transition_count = 0;
        }

        self.tab.half_transition_count = 0;
        self.tilde.half_transition_count = 0;
        self.enter.half_transition_count = 0;
        self.space.half_transition_count = 0;
        self.escape.half_transition_count = 0;
        self.delete.half_transition_count = 0;

        self.arrow_up.half_transition_count = 0;
        self.arrow_right.half_transition_count = 0;
        self.arrow_down.half_transition_count = 0;
        self.arrow_left.half_transition_count = 0;

        self.shift.half_transition_count = 0;
        self.control.half_transition_count = 0;
    }

    #[allow(dead_code)] // @TODO: dead_code
    pub fn alpha_key(&self, key: char) -> &Button {
        assert!(key >= 'a' && key <= 'z');
        let index = key as usize - 'a' as usize;
        &self.alpha[index]
    }
}
