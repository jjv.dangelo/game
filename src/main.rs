#![windows_subsystem = "windows"]

extern crate game;

#[cfg(windows)]
mod windows;

#[cfg(windows)]
use crate::windows::Platform;

fn main() {
    Platform::new().map(|mut platform| platform.run());
}
