pub mod fonts;

use crate::renderer::{model, model::Model};

use std::{
    collections::HashMap,
    fs,
    path::PathBuf,
    sync::{mpsc, mpsc::Sender, Arc, Mutex},
    thread,
    thread::JoinHandle,
};

pub struct AssetManager {
    pub models: AssetCollection<Model>,
}

impl AssetManager {
    pub fn new() -> Self {
        let models = AssetCollection::new(load_model);
        Self { models }
    }
}

pub struct AssetCollection<T: Send> {
    data: Arc<Mutex<HashMap<String, T>>>,
    sender: Sender<String>,
    _worker: JoinHandle<()>,
}

fn load_string(name: &str) -> Option<String> {
    let path: PathBuf = ["assets", name].iter().collect();
    match fs::read_to_string(path) {
        Ok(data) => Some(data),
        Err(_) => None,
    }
}

impl<T: Send + 'static> AssetCollection<T> {
    pub fn new(loader: fn(&str) -> Option<T>) -> Self {
        let (sender, receiver) = mpsc::channel::<String>();
        let data = Arc::new(Mutex::new(HashMap::with_capacity(32)));

        let thread_data = data.clone();
        let _worker = thread::spawn(move || {
            while let Ok(asset_name) = receiver.recv() {
                let mut data = thread_data.lock().unwrap();
                if !data.contains_key(asset_name.as_str()) {
                    let asset = loader(asset_name.as_str()).unwrap();
                    data.insert(asset_name, asset);
                };
            }
        });

        Self {
            data,
            sender,
            _worker,
        }
    }

    pub fn get_asset<F>(&self, name: &str, mut callback: F)
    where
        F: FnMut(&T),
    {
        if let Ok(models) = self.data.lock() {
            if let Some(model) = models.get(name) {
                callback(model);
            } else {
                self.sender.send(name.to_owned()).unwrap();
            }
        }
    }
}

fn load_model(name: &str) -> Option<Model> {
    let asset_data = load_string(name).unwrap();
    model::load_obj(name.to_owned(), asset_data, |f| load_string(f))
}
