use crate::{
    assets::AssetManager,
    components::{CameraRay, PositionComponent},
    math::{bounding::BoundingBox, Vec3},
    platform::GameInput,
    renderer::{color, RenderCommand, RenderCommands},
    world::World,
};

type Entity = crate::array::Index;

pub struct Editor {
    rot: f32,
    target_model: usize,
    inserting: bool,
    selected_entities: Vec<Entity>,
}

impl Default for Editor {
    fn default() -> Self {
        Self {
            rot: 0.,
            target_model: 0,
            inserting: true,
            selected_entities: Vec::with_capacity(16),
        }
    }
}

impl Editor {
    pub fn frame(
        &mut self,
        render_commands: &mut RenderCommands,
        game_input: &GameInput,
        world_mode: &mut World,
        asset_manager: &mut AssetManager,
    ) {
        if game_input.keyboard.delete.was_down() {
            for &entity in self.selected_entities.iter() {
                world_mode.allocator.deallocate(entity);
                if let Some(index) = world_mode.aabb_components.get(entity) {
                    world_mode.aabb.remove(*index);
                }
            }

            self.selected_entities.clear();
        } else {
            for &entity in self.selected_entities.iter() {
                if let Some(model) = world_mode.model_components.get(entity) {
                    if let Some(position) = world_mode.position_components.get(entity) {
                        render_commands.push_model_highlight(
                            model.filename.clone(),
                            color::CYAN,
                            position.create_transform(),
                        );
                    }
                }
            }
        }

        let camera = &mut world_mode.camera;
        let (mouse_x, mouse_y) = (game_input.mouse.x as f32, game_input.mouse.y as f32);
        let CameraRay { ray, origin, dir } = camera.calc_ray(mouse_x, mouse_y);

        if self.inserting {
            let found = world_mode.aabb.cast_closest(ray).map(|(_, t)| t);

            let controller = &game_input.controller;
            if controller.move_left.was_down() {
                self.rot += std::f32::consts::PI / 2.;
            }

            if controller.move_right.was_down() {
                self.rot -= std::f32::consts::PI / 2.;
            }

            let models = [
                "Brown_Cliff_Bottom_01.obj",
                "Brown_Cliff_Top_01.obj",
                "Brown_Waterfall_Top_01.obj",
                "Brown_Cliff_Bottom_Green_Top_01.obj",
                "Brown_Cliff_End_Green_Top_01.obj",
                "brown_cliff_corner_01.obj",
                "trunk_01.obj",
                "large_oak_fall_01.obj",
                "fallen_trunk_01.obj",
                "grass_01.obj",
                "grey_waterfall_01.obj",
                "mushroom_tall_01.obj",
                "plate_river_01.obj",
                "wood_fence_01.obj",
                "campfire_01.obj",
                "brown_waterfall_01.obj",
                "naturePack_flat_001.obj",
                "plate_grass_01.obj",
                "plate_grass_dirt_01.obj",
                "wood_fence_broken_01.obj",
                "wood_fence_gate_01.obj",
            ];

            if game_input.keyboard.f[2].was_down() {
                self.target_model = (self.target_model + 1) % models.len();
            }

            if game_input.keyboard.f[1].was_down() {
                self.target_model = match self.target_model {
                    0 => models.len() - 1,
                    _ => self.target_model - 1,
                };
            }

            let target_model = models[self.target_model];
            asset_manager.models.get_asset(target_model, |model| {
                let aabb = BoundingBox::wrap(model.vertices.iter());

                let t =
                    found.unwrap_or_else(|| -(Vec3::Y_AXIS.dot(origin)) / Vec3::Y_AXIS.dot(dir));
                let p = origin + (dir * t);
                let (x, z) = if game_input.keyboard.shift.is_down() {
                    (p.x, p.z)
                } else {
                    ((p.x).round(), (p.z).round())
                };
                let y = p.y;

                let rot = crate::math::Quaternion::new(Vec3::Y_AXIS, self.rot);
                let pos = PositionComponent::new(Vec3::new(x, y, z)).with_rot(rot);
                let world = pos.create_transform();

                render_commands.push(RenderCommand::PushModel(model.filename.clone(), world));

                if game_input.mouse.left_button.was_down() {
                    let entity = world_mode.allocator.allocate();
                    world_mode.position_components.set(entity, pos);
                    world_mode.model_components.set(entity, model.clone());
                    let aabb_id = world_mode
                        .aabb
                        .insert(entity, aabb * pos.create_transform());
                    world_mode.aabb_components.set(entity, aabb_id);
                    self.selected_entities.clear();
                }
            });
        } else {
            if game_input.mouse.left_button.was_down() {
                let mut hit = Vec::new();
                world_mode.aabb.cast(ray, |id, dist| {
                    hit.push((*id, dist));
                    true
                });

                let mut found = None;
                for (entity, dist) in hit.iter() {
                    match found {
                        None => found = Some((*entity, dist)),
                        Some((_, current)) if dist < current => found = Some((*entity, dist)),
                        _ => (),
                    }
                }

                match found {
                    Some((entity, _)) => {
                        if game_input.keyboard.control.is_down() {
                            if self.selected_entities.contains(&entity) {
                                self.selected_entities.remove_item(&entity);
                            } else {
                                self.selected_entities.push(entity);
                            }
                        } else {
                            self.selected_entities.clear();
                            self.selected_entities.push(entity);
                        }
                    }
                    None => self.selected_entities.clear(),
                }
            }

            if self.selected_entities.len() > 0 {
                let mut translation = Vec3::ZERO;
                let (fwd, right) = calc_action_trans(world_mode.camera.pos.normalized());

                if game_input.controller.action_left.was_down() {
                    translation -= right * 0.25;
                }

                if game_input.controller.action_right.was_down() {
                    translation += right * 0.25;
                }

                if game_input.controller.action_up.was_down() {
                    if game_input.controller.select.is_down() {
                        translation += Vec3::Y_AXIS * 0.25;
                    } else {
                        translation += fwd * 0.25;
                    }
                }

                if game_input.controller.action_down.was_down() {
                    if game_input.controller.select.is_down() {
                        translation -= Vec3::Y_AXIS * 0.25;
                    } else {
                        translation -= fwd * 0.25;
                    }
                }

                if translation != Vec3::ZERO {
                    for &entity in self.selected_entities.iter() {
                        if let Some(pos_comp) = world_mode.position_components.get_mut(entity) {
                            pos_comp.pos += translation;

                            if let Some(aabb_id) = world_mode.aabb_components.get_mut(entity) {
                                world_mode.aabb.remove(*aabb_id);
                                if let Some(model) = world_mode.model_components.get(entity) {
                                    let aabb = BoundingBox::wrap(model.vertices.iter());
                                    *aabb_id = world_mode
                                        .aabb
                                        .insert(entity, aabb * pos_comp.create_transform());
                                }
                            }
                        }
                    }
                }
            }
        }

        if game_input.keyboard.f[5].was_down() {
            self.inserting = !self.inserting;
        }
    }
}

/// Calculates the action directions (forward and right) depending on the camera's current position.
/// The idea is to help move entities correctly using the arrow keys without needing to remember
/// or visualize the x- and z-axes.
fn calc_action_trans(pos: Vec3) -> (Vec3, Vec3) {
    use std::f32::consts::PI;

    let x_dot = pos.dot(Vec3::X_AXIS);
    let theta_x = x_dot.cos();
    let z_dot = pos.dot(Vec3::Z_AXIS);

    if theta_x > PI * 0.25 && theta_x <= PI * 0.75 {
        if z_dot < 0. {
            (Vec3::Z_AXIS, Vec3::X_AXIS)
        } else {
            (-Vec3::Z_AXIS, -Vec3::X_AXIS)
        }
    } else {
        if x_dot < 0. {
            (Vec3::X_AXIS, -Vec3::Z_AXIS)
        } else {
            (-Vec3::X_AXIS, Vec3::Z_AXIS)
        }
    }
}
