use crate::{
    array::{Array, GenerationalIndexAllocator, Index},
    components::{bvh::avl, bvh::avl::Tree, Camera, PositionComponent},
    controls::Mouse,
    math::Vec3,
    platform::MAX_OBJECTS,
    renderer::model::Model,
};

type Entity = Index;

#[allow(dead_code)] // @TODO: dead_code
pub struct World {
    pub allocator: GenerationalIndexAllocator,
    pub position_components: Array<PositionComponent>,
    pub model_components: Array<Model>,
    pub aabb_components: Array<avl::Index>,
    pub aabb: Tree<Entity>,

    pub sun_dir: Vec3,
    pub camera: Camera,
    pub target_camera: Camera,

    pub elapsed: f32,

    pub last_mouse: Mouse,

    pub player_pos: Vec3,
    pub player_dir: Vec3,

    pub show_strobes: bool,
}

impl World {
    pub fn new(camera: Camera, mouse: Mouse) -> Self {
        Self {
            allocator: GenerationalIndexAllocator::with_capacity(MAX_OBJECTS),
            position_components: Array::with_capacity(MAX_OBJECTS),
            model_components: Array::with_capacity(MAX_OBJECTS),
            aabb_components: Array::with_capacity(MAX_OBJECTS),

            aabb: Tree::new(),

            sun_dir: Vec3::new(0.57735, -0.57735, 0.57735),
            camera,
            target_camera: camera,

            elapsed: 0.,

            last_mouse: mouse,

            player_pos: Vec3::ZERO,
            player_dir: Vec3::Z_AXIS,

            show_strobes: false,
        }
    }
}
