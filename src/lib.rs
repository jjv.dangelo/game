#![feature(vec_remove_item)]
#![feature(try_trait)]

pub mod array;
pub mod assets;
pub mod clock;
pub mod components;
pub mod controls;
pub mod deflate;
pub mod editor;
pub mod math;
pub mod platform;
pub mod renderer;
pub mod stream;
mod ui;
mod world;

use crate::{
    assets::AssetManager,
    components::{Camera, PositionComponent},
    math::{Lerp, Quaternion, Vec2, Vec3, Vec4},
    platform::{GameInput, GameOutput, PlatformApi},
    renderer::{
        color,
        lights::{Directional, Point},
        Color4, RenderCommand, RenderCommands,
    },
    world::World,
};

enum GameMode {
    Uninitialized,
    WorldMode(World),
}

impl Default for GameMode {
    fn default() -> Self {
        GameMode::Uninitialized
    }
}

#[derive(Default)]
pub struct GameState {
    game_mode: GameMode,
    editor: editor::Editor,
    editing: bool,
}

impl GameState {
    pub fn update_frame<P: PlatformApi>(
        &mut self,
        _platform: &mut P,
        game_input: &GameInput,
        render_commands: &mut RenderCommands,
        asset_manager: &mut AssetManager,
    ) -> GameOutput {
        match self.game_mode {
            GameMode::Uninitialized => {
                let camera = Camera::new(game_input.window_width, game_input.window_height);
                let mut world = World::new(camera, game_input.mouse);
                world.target_camera.pos = Vec3::new(0., 25., 15.);
                world.target_camera.up = -world.target_camera.pos.cross(-Vec3::X_AXIS).normalized();
                self.game_mode = GameMode::WorldMode(world);
            }

            GameMode::WorldMode(ref mut world) => {
                world.camera.window_width = game_input.window_width;
                world.camera.window_height = game_input.window_height;
                world.elapsed += game_input.tick_info.dt;

                let camera = &mut world.camera;
                let current_mouse = game_input.mouse;

                if game_input.keyboard.enter.is_down() {
                    camera.pos = camera.pos.lerp(world.target_camera.pos, game_input.tick_info.dt);
                    camera.up = camera.up.lerp(world.target_camera.up, game_input.tick_info.dt);
                }

                if current_mouse.scroll_delta != 0 {
                    let delta = current_mouse.scroll_delta as f32;
                    let pos = camera.pos.normalized() * delta * game_input.tick_info.dt;

                    let dist_to_look_at = (camera.pos + pos - camera.look_at).len();
                    if dist_to_look_at > 0.17 && dist_to_look_at < 50. {
                        camera.pos += pos;
                    }
                }

                if game_input.mouse.right_button.is_down() {
                    let (dx, dy) = (
                        (current_mouse.x - world.last_mouse.x) as f32,
                        (current_mouse.y - world.last_mouse.y) as f32,
                    );

                    let dt = game_input.tick_info.dt * 2.;
                    let target_axis = camera.pos.cross(Vec3::Y_AXIS).normalized();
                    let rot = Quaternion::new(target_axis, dy * dt)
                        * Quaternion::new(Vec3::Y_AXIS, dx * dt);
                    camera.pos = rot.rotate_vec(camera.pos);

                    let dir = (camera.pos - camera.look_at).normalized();
                    let right = Vec3::Y_AXIS.cross(dir).normalized();
                    camera.up = dir.cross(right).normalized();
                }

                if game_input.keyboard.f[12].is_down() {
                    world.target_camera =
                        Camera::new(game_input.window_width, game_input.window_height);
                }

                if !self.editing {
                    let mut rot = Quaternion::new(Vec3::Z_AXIS, 0.);

                    if game_input.keyboard.arrow_left.is_down() {
                        rot *= Quaternion::new(Vec3::Y_AXIS, game_input.tick_info.dt);
                    }

                    if game_input.keyboard.arrow_right.is_down() {
                        rot *= Quaternion::new(Vec3::Y_AXIS, -game_input.tick_info.dt);
                    }

                    let target_axis = camera.pos.cross(Vec3::Y_AXIS).normalized();
                    if game_input.keyboard.arrow_up.is_down() {
                        rot *= Quaternion::new(target_axis, game_input.tick_info.dt);
                    }

                    if game_input.keyboard.arrow_down.is_down() {
                        rot *= Quaternion::new(target_axis, -game_input.tick_info.dt);
                    }

                    camera.pos = rot.rotate_vec(camera.pos);
                    let dir = camera.look_at - camera.pos;
                    let right = Vec3::Y_AXIS.cross(dir).normalized();
                    camera.up = dir.cross(right).normalized();
                }

                render_commands.push(RenderCommand::PushDirectionalLight(Directional::new(
                    Color4::rgb(0.1, 0.1, 0.1),
                    Color4::rgb(0.25, 0.25, 0.25),
                    Color4::rgb(0.25, 0.25, 0.25),
                    (camera.look_at - camera.pos).normalized(),
                )));

                render_commands.push(RenderCommand::PushDirectionalLight(Directional::new(
                    Color4::rgb(0.2, 0.2, 0.2),
                    Color4::rgb(0.5, 0.5, 0.5),
                    Color4::rgb(0.5, 0.5, 0.5),
                    world.sun_dir,
                )));

                let multiplier = (((game_input.tick_info.t * 4.).sin() as f32 + 1.) * 0.5) + 0.1;

                if game_input.keyboard.f[3].was_down() {
                    world.show_strobes = !world.show_strobes;
                }

                if game_input.keyboard.space.is_down() || world.show_strobes {
                    for &(x, y, z) in
                        [(-5., 0., 5.), (5., 0., 5.), (5., 0., -5.), (-5., 0., -5.)].iter()
                    {
                        render_commands.push(RenderCommand::PushPointLight(Point::new(
                            color::RED * multiplier,
                            color::RED * multiplier,
                            color::RED * multiplier,
                            Vec3::new(x, y, z),
                            5.,
                            Vec3::new(0., 0., 1.),
                        )));
                    }
                }

                render_commands.push(RenderCommand::BakeLights);

                if game_input.keyboard.f[11].was_down() {
                    self.editing = !self.editing;
                }

                let clear_color = Color4::new(0.035, 0.335, 0.735, 0.);
                render_commands.push(RenderCommand::ClearWindow(clear_color));
                render_commands.push(RenderCommand::SetPerspective(
                    world.camera.view_proj(),
                    world.camera.pos,
                ));

                if self.editing {
                    self.editor
                        .frame(render_commands, &game_input, world, asset_manager);
                }

                let pos_comp = PositionComponent::new(Vec3::new(-5., 0., 5.))
                    .with_scale(Vec3::new(0.025, 0., 0.025));
                render_commands.push_rect(color::RED * multiplier, pos_comp.create_transform());

                let pos_comp = PositionComponent::new(Vec3::new(5., 0., 5.))
                    .with_scale(Vec3::new(0.025, 0., 0.025));
                render_commands.push_rect(color::RED * multiplier, pos_comp.create_transform());

                let pos_comp = PositionComponent::new(Vec3::new(5., 0., -5.))
                    .with_scale(Vec3::new(0.025, 0., 0.025));
                render_commands.push_rect(color::RED * multiplier, pos_comp.create_transform());

                let pos_comp = PositionComponent::new(Vec3::new(-5., 0., -5.))
                    .with_scale(Vec3::new(0.025, 0., 0.025));
                render_commands.push_rect(color::RED * multiplier, pos_comp.create_transform());

                render_commands.push_rect_outline(Vec3::ZERO, 3., 3., color::WHITE);

                let allocator = &mut world.allocator;
                let position_components = &world.position_components;
                let model_components = &world.model_components;

                allocator.iter(|index| {
                    if let Some(position) = position_components.get(index) {
                        if let Some(model) = model_components.get(index) {
                            render_commands.push(RenderCommand::PushModel(
                                model.filename.clone(),
                                position.create_transform(),
                            ));
                        }
                    }

                    true
                });

                world.last_mouse = current_mouse;
            }
        }

        {
            // @TODO: UI API
            let delta = ((game_input.tick_info.t / 2.).sin() as f32 + 1.) / 2.;
            let (width, height) = (
                game_input.window_width as f32,
                game_input.window_height as f32,
            );
            // render_commands.push_ui_rect(
            //     Color4::new(1. * delta, 1. * delta, 1. * delta, 1.),
            //     Vec2::new(50., 50.),
            //     Vec2::new(550. * delta, 35.),
            // );
            // render_commands.push_ui_rect(
            //     Color4::new(0., 1., 0., 1.),
            //     Vec2::new(50., 95.),
            //     Vec2::new(550. * delta, 35.),
            // );
            // render_commands.push_ui_rect(
            //     Color4::new(0., 1., 1., 1.),
            //     Vec2::new(50., 95. + 35. + 10.),
            //     Vec2::new(550. * delta, 35.),
            // );
            // render_commands.push_ui_rect(
            //     Color4::new(0.155, 0.155, 0.155, 0.5),
            //     Vec2::new(40., 40.),
            //     Vec2::new(570., 145.),
            // );
            // render_commands.push_ui_rect(
            //     Color4::new(0.155, 0.155, 0.155, 0.5),
            //     Vec2::new(width * 0.5 - 450., height - 150.),
            //     Vec2::new(900., 125.),
            // );

            // render_commands.push_string(
            //     "Hello world!".to_owned(),
            //     Vec2::new((width * 0.5 - 500.).round(), (height * 0.5 - 500.).round()),
            //     color::GREEN,
            // );

            let mouse = Vec2::new(game_input.mouse.x as _, game_input.mouse.y as _);
            let mut ui = ui::Ui::new(Vec2::new(width, height), mouse, render_commands);
            ui.stack_panel(Vec2::new(40., 40.), Vec2::new(570., 145.), Vec4::new(15., 15., 15., 15.));

            { // Health bar
                ui.panel(Vec2::new(0., 0.), Vec2::new(560., 40.));

                ui.panel(Vec2::new(0., 0.), Vec2::new(560. * delta, 40.));
                ui.set_color(Color4::new(1., 0., 0., 1.));
                ui.end_panel(true);

                ui.set_color(Color4::new(0., 0., 0., 1.));
                ui.end_panel(true);
            }

            { // Energy bar
                ui.panel(Vec2::new(0., 0.), Vec2::new(560., 40.));

                ui.panel(Vec2::new(0., 0.), Vec2::new(560. * delta, 40.));
                let old_color = ui.set_color(Color4::new(0., 1., 0., 1.));
                ui.end_panel(true);

                ui.set_color(old_color);
                ui.end_panel(true);
            }

            { // Mana bar
                ui.panel(Vec2::new(0., 0.), Vec2::new(560., 40.));

                ui.panel(Vec2::new(0., 0.), Vec2::new(560. * delta, 40.));
                let old_color = ui.set_color(Color4::new(0., 0., 1., 1.));
                ui.end_panel(true);

                ui.set_color(old_color);
                ui.end_panel(true);
            }

            ui.set_color(Color4::new(0.155, 0.155, 0.155, 0.5));
            ui.end_panel(true);
        }

        GameOutput::Continue
    }
}
