use crate::{
    color::Color4,
    math::{Vec2, Vec4},
    renderer::RenderCommands,
};

#[derive(Clone, Copy)]
struct UiData {
    top_left: Vec2,
    dim: Vec2,
}

#[derive(Clone, Copy)]
struct StackData {
    offset: Vec2,
    padding: Vec4,
}

impl StackData {
    fn new(top_left: Vec2, padding: Vec4) -> Self {
        Self {
            offset: top_left + [0., 5.],
            padding,
        }
    }
}

#[derive(Clone)]
enum UiState {
    Absolute(Box<UiData>),
    Stack(Box<(UiData, StackData)>),
}

impl UiState {
    fn absolute(top_left: Vec2, dim: Vec2) -> Self {
        UiState::Absolute(Box::new(UiData { top_left, dim }))
    }

    fn stack(top_left: Vec2, dim: Vec2, padding: Vec4) -> Self {
        UiState::Stack(Box::new((UiData { top_left, dim }, StackData::new(top_left, padding))))
    }

    fn data(&self) -> &UiData {
        match self {
            UiState::Absolute(ui_data) => &*ui_data,
            UiState::Stack(ref data) => &data.0,
        }
    }
}

pub struct Ui<'a> {
    states: Vec<UiState>,
    render_commands: &'a mut RenderCommands,
    color: Color4,
    mouse_pos: Vec2,
}

impl<'a> Ui<'a> {
    pub fn new(dim: Vec2, mouse_pos: Vec2, render_commands: &'a mut RenderCommands) -> Self {
        Self {
            states: vec![UiState::absolute(Vec2::new(0., 0.), dim)],
            render_commands,
            color: Color4::new(0., 0., 0., 0.),
            mouse_pos,
        }
    }

    pub fn fill(&mut self) {
        match self.states.last() {
            Some(UiState::Absolute(ref state)) => {
                self.render_commands.push_ui_rect(
                    self.color,
                    state.top_left,
                    state.dim,
                );
            }

            Some(UiState::Stack(ref data)) => {
                let state = &data.0;
                let offset = data.1.offset;

                let dim = if offset.y > state.dim.y {
                    Vec2::new(state.dim.x, offset.y)
                } else {
                    state.dim
                };

                self.render_commands.push_ui_rect(
                    self.color,
                    state.top_left,
                    dim + [data.1.padding.x, data.1.padding.y] + [data.1.padding.z, data.1.padding.w],
                );
            }

            None => {}
        }
    }

    pub fn panel(&mut self, pos: Vec2, dim: Vec2) -> bool {
        let new_panel = match self.states.last_mut() {
            Some(UiState::Absolute(ref ui_data)) => {
                let top_left = ui_data.top_left;
                UiState::absolute(
                    top_left + pos,
                    dim,
                )
            }

            Some(UiState::Stack(ref mut data)) => {
                let stack_data = data.1;
                let padding = stack_data.padding;

                let new_panel = UiState::absolute(
                    stack_data.offset + pos + [padding.x, padding.y],
                    dim - [padding.z, padding.x],
                );

                data.1.offset += Vec2::new(0., dim.y + 5.);

                new_panel
            }

            None => { unreachable!() }
        };

        let new_panel_data = new_panel.data();
        let mouse_over = self.mouse_pos.x >= new_panel_data.top_left.x
            && self.mouse_pos.x < new_panel_data.top_left.x + dim.x
            && self.mouse_pos.y >= new_panel_data.top_left.y
            && self.mouse_pos.y < new_panel_data.top_left.y + dim.y;

        self.states.push(new_panel);
        mouse_over
    }

    pub fn stack_panel(&mut self, pos: Vec2, dim: Vec2, padding: Vec4) -> bool {
        let new_panel = match self.states.last_mut() {
            Some(UiState::Absolute(ref ui_data)) => {
                let top_left = ui_data.top_left;
                UiState::stack(
                    top_left + pos,
                    dim,
                    padding,
                )
            }

            Some(UiState::Stack(ref mut data)) => {
                let top_left = data.1;
                let new_panel = UiState::stack(
                    top_left.offset + pos,
                    dim,
                    padding,
                );

                data.1.offset += Vec2::new(0., dim.y + 5.);

                new_panel
            }

            None => { unreachable!() }
        };

        let new_panel_data = new_panel.data();
        let mouse_over = self.mouse_pos.x >= new_panel_data.top_left.x
            && self.mouse_pos.x < new_panel_data.top_left.x + dim.x
            && self.mouse_pos.y >= new_panel_data.top_left.y
            && self.mouse_pos.y < new_panel_data.top_left.y + dim.y;

        self.states.push(new_panel);
        mouse_over
    }

    pub fn end_panel(&mut self, fill: bool) {
        if fill {
            self.fill();
        }

        self.states.pop();
    }

    pub fn set_color(&mut self, color: Color4) -> Color4 {
        let old_color = self.color;
        self.color = color;
        old_color
    }
}
