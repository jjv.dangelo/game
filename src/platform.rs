use crate::{
    clock::TickInfo,
    controls::{Controller, Keyboard, Mouse},
};

pub const MAX_OBJECTS: usize = 4096;

#[allow(dead_code)] // @TODO: dead_code
pub enum GameOutput {
    Error,
    Continue,
    Quit,
}

pub struct GameInput {
    pub tick_info: TickInfo,
    pub controller: Controller,
    pub mouse: Mouse,
    pub keyboard: Keyboard,
    pub window_width: u32,
    pub window_height: u32,
}

pub trait PlatformApi {
    fn load_blob(&self, asset_name: &str) -> Option<Vec<u8>>;
    fn load_text(&self, asset_name: &str) -> Option<String>;
}
