pub mod bounding;
pub use bounding::*;

pub mod matrix;
pub use matrix::*;

pub mod ray;
pub use ray::*;

pub mod vec;
pub use vec::*;

pub mod quaternion;
pub use quaternion::*;

pub trait Lerp {
    fn lerp(self, other: Self, d: f32) -> Self;
}

impl<T> Lerp for T
where
    T: std::ops::Sub<T, Output = T> + std::ops::Add<T, Output = T>,
    T: Copy,
    f32: std::ops::Mul<T, Output = T>,
{
    fn lerp(self, y: T, d: f32) -> T {
        // x * (1. - d) + y * d
        self + d * (y - self)
    }
}

#[cfg(test)]
mod vec3_tests {
    use super::Vec3;

    #[test]
    fn can_add_two_vec3s() {
        let x = Vec3::new(1., 1., 1.);
        let y = Vec3::new(1., 1., 1.);
        assert_eq!(x + y, Vec3::new(2., 2., 2.));
    }

    #[test]
    fn can_add_assign_vec3s() {
        let x = Vec3::new(1., 1., 1.);
        let mut y = Vec3::new(1., 1., 1.);
        y += x;
        assert_eq!(y, Vec3::new(2., 2., 2.));
    }

    #[test]
    fn can_multiply_vec3_by_scalar() {
        let x = Vec3::new(1., 1., 1.);
        assert_eq!(x * 5., Vec3::new(5., 5., 5.));
    }

    #[test]
    fn can_mul_assign_vec3_by_scalar() {
        let mut x = Vec3::new(1., 1., 1.);
        x *= 5.;
        assert_eq!(x, Vec3::new(5., 5., 5.));
    }

    #[test]
    fn can_subtract_vec3s() {
        let x = Vec3::new(2., 2., 2.);
        let y = Vec3::new(1., 1., 1.);
        assert_eq!(x - y, Vec3::new(1., 1., 1.));
    }

    #[test]
    fn can_sub_assign_vec3s() {
        let x = Vec3::new(1., 1., 1.);
        let mut y = Vec3::new(2., 2., 2.);
        y -= x;
        assert_eq!(y, Vec3::new(1., 1., 1.));
    }

    #[test]
    fn can_calculate_magnitude_squared_of_vec3() {
        let x = Vec3::new(1., 1., 1.);
        assert_eq!(x.mag_sqrd(), 3.);
    }

    #[test]
    fn can_convert_array_to_vec3() {
        let x: Vec3 = std::convert::From::from([1., 2., 3.]);
        assert_eq!(x, Vec3::new(1., 2., 3.));
    }

    #[test]
    fn can_negate_vec3() {
        let x = Vec3::new(1., 2., 3.);
        let result = -x;
        assert_eq!(result, Vec3::new(-1., -2., -3.,));
    }
}

#[cfg(test)]
mod vec4_tests {
    use super::Vec4;

    #[test]
    fn can_add_two_vec4s() {
        let x = Vec4::new(1., 1., 1., 1.);
        let y = Vec4::new(1., 1., 1., 1.);
        assert_eq!(x + y, Vec4::new(2., 2., 2., 2.));
    }

    #[test]
    fn can_add_assign_two_vec4s() {
        let x = Vec4::new(1., 1., 1., 1.);
        let mut y = Vec4::new(1., 1., 1., 1.);
        y += x;
        assert_eq!(y, Vec4::new(2., 2., 2., 2.));
    }

    #[test]
    fn can_multiply_vec4_by_scalar() {
        let x = Vec4::new(1., 1., 1., 1.);
        assert_eq!(x * 5., Vec4::new(5., 5., 5., 5.));
    }

    #[test]
    fn can_mul_assign_vec4_by_scalar() {
        let mut x = Vec4::new(1., 1., 1., 1.);
        x *= 5.;
        assert_eq!(x, Vec4::new(5., 5., 5., 5.));
    }

    #[test]
    fn can_subtract_vec4s() {
        let x = Vec4::new(2., 2., 2., 2.);
        let y = Vec4::new(1., 1., 1., 1.);
        assert_eq!(x - y, Vec4::new(1., 1., 1., 1.));
    }

    #[test]
    fn can_sub_assign_vec4s() {
        let x = Vec4::new(1., 1., 1., 1.);
        let mut y = Vec4::new(2., 2., 2., 2.);
        y -= x;
        assert_eq!(y, Vec4::new(1., 1., 1., 1.));
    }

    #[test]
    fn can_calculate_magnitude_squared_of_vec4() {
        let x = Vec4::new(1., 1., 1., 1.);
        assert_eq!(x.mag_sqrd(), 4.);
    }

    #[test]
    fn can_calculate_len_of_vec4() {
        let x = Vec4::new(1., 0., 0., 0.);
        assert_eq!(x.len(), 1.);
    }

    #[test]
    fn can_convert_vec4_normalized_length() {
        let x = Vec4::new(5., 0., 0., 0.);
        assert_eq!(x.normalized(), Vec4::new(1., 0., 0., 0.));
    }

    #[test]
    fn can_convert_array_to_vec4() {
        let x: Vec4 = std::convert::From::from([1., 2., 3., 4.]);
        assert_eq!(x, Vec4::new(1., 2., 3., 4.));
    }
}

#[cfg(test)]
mod matrix4x4_tests {
    use super::{Matrix4x4, Vec3, Vec4};

    #[test]
    fn can_multiply_by_4x4() {
        let left = Matrix4x4::new([
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
        ]);

        let right = Matrix4x4::new([
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
        ]);

        let expected = Matrix4x4::new([
            Vec4::new(10., 20., 30., 40.),
            Vec4::new(10., 20., 30., 40.),
            Vec4::new(10., 20., 30., 40.),
            Vec4::new(10., 20., 30., 40.),
        ]);

        let result = left * right;
        assert_eq!(result, expected);
    }

    #[test]
    fn can_multiply_by_v4() {
        let left = Vec4::new(1., 2., 3., 4.);
        let right = Matrix4x4::new([
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
            Vec4::new(1., 2., 3., 4.),
        ]);

        let expected = Vec4::new(10., 20., 30., 40.);
        let result = left * right;
        assert_eq!(result, expected);
    }

    #[test]
    fn can_calculate_determinant_of_4x4() {
        let m = Matrix4x4::new([
            Vec4::new(6., 7., 8., 9.),
            Vec4::new(4., 3., 2., 1.),
            Vec4::new(1., 7., 2., 9.),
            Vec4::new(4., 3., 8., 1.),
        ]);
        let expected = -600.;
        let result = m.det();
        assert_eq!(result, expected);
    }

    #[test]
    fn can_calculate_cofactor_of_4x4() {
        let m = Matrix4x4::new([
            Vec4::new(-2., 0., 1., 6.),
            Vec4::new(1., 1., 0., 2.),
            Vec4::new(8., -1., -1., 1.),
            Vec4::new(3., 4., 5., 6.),
        ]);
        let expected = Matrix4x4::new([
            Vec4::new(-13., -75., 15., 44.),
            Vec4::new(-4., 235., -230., 37.),
            Vec4::new(32., -50., 10., 9.),
            Vec4::new(9., 5., 60., -7.),
        ]);
        let result = m.cofactor();
        assert_eq!(result, expected);
    }

    #[test]
    fn can_calculate_transpose_of_4x4() {
        let m = Matrix4x4::new([
            Vec4::new(0., 1., 2., 3.),
            Vec4::new(4., 5., 6., 7.),
            Vec4::new(8., 9., 10., 11.),
            Vec4::new(12., 13., 14., 15.),
        ]);
        let expected = Matrix4x4::new([
            Vec4::new(0., 4., 8., 12.),
            Vec4::new(1., 5., 9., 13.),
            Vec4::new(2., 6., 10., 14.),
            Vec4::new(3., 7., 11., 15.),
        ]);
        let result = m.transpose();
        assert_eq!(result, expected);
    }

    #[test]
    fn can_calculate_adjoint_of_4x4() {
        let m = Matrix4x4::new([
            Vec4::new(-2., 0., 1., 6.),
            Vec4::new(1., 1., 0., 2.),
            Vec4::new(8., -1., -1., 1.),
            Vec4::new(3., 4., 5., 6.),
        ]);
        let expected = Matrix4x4::new([
            Vec4::new(-13., -4., 32., 9.),
            Vec4::new(-75., 235., -50., 5.),
            Vec4::new(15., -230., 10., 60.),
            Vec4::new(44., 37., 9., -7.),
        ]);
        let result = m.adjoint();
        assert_eq!(result, expected);
    }

    // @TODO: This test will fail because of floating point rounding errors.
    // We could test that everything outside of the diagonal is less than std::f32::EPSILON.
    // #[test]
    // fn matrix_times_inverse_is_identity() {
    //     let m = Matrix4x4::new([
    //         Vec4::new(-2., 0., 1., 6.,),
    //         Vec4::new(1., 1., 0., 2.,),
    //         Vec4::new(8., -1., -1., 1.,),
    //         Vec4::new(3., 4., 5., 6.,),
    //     ]);
    //     let inverse = m.inverse();
    //     let result = m * inverse;
    //     assert_eq!(result, Matrix4x4::IDENTITY);
    // }

    #[test]
    fn can_calculate_translation_on_point() {
        let v = Vec4::point(1., 1., 1.);
        let t = Matrix4x4::translation(Vec3::new(1., 1., 1.));
        let result = v * t;
        assert_eq!(result, Vec4::point(2., 2., 2.,));
    }

    #[test]
    fn does_not_translate_vector() {
        let v = Vec4::vec(1., 1., 1.);
        let t = Matrix4x4::translation(Vec3::new(1., 1., 1.));
        let result = v * t;
        assert_eq!(result, v);
    }

    #[test]
    fn can_calculate_scale() {
        let v = Vec4::point(1., 1., 1.);
        let s = Matrix4x4::scale(3., 3., 3.);
        let result = v * s;
        assert_eq!(result, Vec4::point(3., 3., 3.,));
    }

    #[test]
    fn rotation_axis_for_x_gives_same_as_rotation_x() {
        let about_x = Matrix4x4::rotation_x(0.75);
        let result = Matrix4x4::rotation_axis(Vec3::new(1., 0., 0.), 0.75);
        assert_eq!(result, about_x);
    }

    #[test]
    fn rotation_back_and_forth_gives_original_position() {
        let rotation_axis = Vec3::new(1., 1., 1.);
        let theta = 0.75;
        let forward = Matrix4x4::rotation_axis(rotation_axis, theta);
        let backward = Matrix4x4::rotation_axis(rotation_axis, -theta);
        let combined = forward * backward;
        let vec = Vec4::point(4., 2., 2.);
        let result = vec * combined;
        assert_eq!(result, vec);
    }
}

#[cfg(test)]
mod bounding_box_tests {
    use super::{BoundingBox, Vec3};

    #[test]
    fn can_wrap_set_of_points() {
        let points = [
            Vec3::new(-1., -1., -1.),
            Vec3::new(-1., 1., -1.),
            Vec3::new(1., -1., -1.),
            Vec3::new(1., 1., -1.),
            Vec3::new(-1., -1., 1.),
            Vec3::new(-1., 1., 1.),
            Vec3::new(1., 1., 1.),
            Vec3::new(1., -1., 1.),
        ];

        let result = BoundingBox::wrap(points.iter());
        assert_eq!(
            result,
            BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.))
        );
    }

    #[test]
    fn can_check_points_inside() {
        let bb = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        assert!(bb.contains_point(Vec3::ZERO));
        assert!(!bb.contains_point(Vec3::new(2., 2., 2.)));
    }

    #[test]
    fn can_check_if_two_bounding_boxes_overlap() {
        let bb_1 = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        let bb_2 = BoundingBox::new(Vec3::ZERO, Vec3::new(2., 2., 2.));
        assert!(bb_1.overlaps(&bb_2));
        assert!(bb_2.overlaps(&bb_1));
    }

    #[test]
    fn can_check_if_bounding_box_fully_contains_other() {
        let bb_1 = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        let bb_2 = BoundingBox::new(Vec3::new(-2., -2., -2.), Vec3::new(2., 2., 2.));
        let bb_3 = BoundingBox::new(Vec3::new(-3., -3., -3.), Vec3::new(3., 3., 3.));
        assert!(!bb_1.contains(&bb_2));
        assert!(bb_2.contains(&bb_1));
        assert!(bb_3.contains(&bb_1));
        assert!(bb_3.contains(&bb_2));
    }

    #[test]
    fn can_combine_two_bounding_boxes() {
        let bb_1 = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        let bb_2 = BoundingBox::new(Vec3::ZERO, Vec3::new(2., 2., 2.));
        let expected = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(2., 2., 2.));
        assert_eq!(bb_1.combine(&bb_2), expected);
        assert_eq!(bb_1.combine(&bb_2), bb_2.combine(&bb_1));
        assert_eq!(bb_1.combine(&bb_1), bb_1);
        assert_eq!(bb_2.combine(&bb_2), bb_2);
    }

    #[test]
    fn combining_a_bounding_box_with_itself_yields_itself() {
        let bb = BoundingBox::new(Vec3::new(-1., -1., 1.), Vec3::new(1., 1., -1.));
        assert_eq!(bb.combine(&bb), bb);
    }

    #[test]
    fn combining_contained_bounding_box_returns_outer() {
        let bb_1 = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        let bb_2 = BoundingBox::new(Vec3::new(-2., -2., -2.), Vec3::new(2., 2., 2.));
        assert_eq!(bb_2.combine(&bb_1), bb_2);
        assert_eq!(bb_1.combine(&bb_2), bb_2);
    }

    #[test]
    fn can_calculate_perimeter() {
        let bb = BoundingBox::new(Vec3::new(-1., -1., -1.), Vec3::new(1., 1., 1.));
        assert_eq!(bb.perimeter(), 24.);
    }

    #[test]
    fn can_scale_bounding_box() {
        let bb = BoundingBox::new(Vec3::new(-1., -1., 1.), Vec3::new(1., 1., -1.));
        assert_eq!(
            bb * 2.,
            BoundingBox::new(Vec3::new(-2., -2., 2.), Vec3::new(2., 2., -2.))
        );
        assert_eq!(bb * 2., 2. * bb);
    }

    #[test]
    fn can_scale_bounding_box_ref() {
        let bb = &BoundingBox::new(Vec3::new(-1., -1., 1.), Vec3::new(1., 1., -1.));
        assert_eq!(
            bb * 2.,
            BoundingBox::new(Vec3::new(-2., -2., 2.), Vec3::new(2., 2., -2.))
        );
        assert_eq!(bb * 2., 2. * bb);
        assert_eq!(bb * 2., (*bb) * 2.);
    }
}
