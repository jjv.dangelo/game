#[derive(Clone, Copy, Debug)]
/// Wraps system time management calls.
pub struct Clock {
    get_wall_clock: fn() -> f64,
    t: f64,
    dt: f32,
    accumulator: f32,
    clock_freq: f64,
    last_tick: f64,
}

#[derive(Clone, Copy, Debug)]
/// Provides clock information about the current simulation tick.
pub struct TickInfo {
    pub t: f64,
    pub dt: f32,
}

impl TickInfo {
    fn new(t: f64, dt: f32) -> Self {
        Self { t, dt }
    }
}

impl Clock {
    /// Creates a new step-based clock given the wall clock and clock frequency functions passed
    /// in.
    pub fn new(step: f32, get_wall_clock: fn() -> f64, get_clock_freq: fn() -> f64) -> Self {
        let clock_freq = get_clock_freq();
        let last_tick = get_wall_clock();

        Self {
            get_wall_clock: get_wall_clock,
            t: 0.,
            dt: step.max(0.01),
            accumulator: 0.,
            clock_freq,
            last_tick,
        }
    }

    fn get_secs_elapsed(&self, new_tick: f64) -> f64 {
        (new_tick - self.last_tick) / self.clock_freq
    }

    /// Runs the simulation with a fixed timestep, dt. Based on the article [Fix Your
    /// Timestep](https://gafferongames.com/post/fix_your_timestep/).
    ///
    /// ```
    /// # fn get_wall_clock() -> f64 { 0. }
    /// # fn get_clock_freq() -> f64 { 0. }
    /// # fn integrate(_: f32, _: f32, _: f32) -> f32 { 0. }
    /// # let mut current_state = 0.;
    /// # let mut previous_state = 0.;
    ///
    /// let fixed_dt_step = 0.01;
    /// let mut clock = Clock::new(fixed_dt_step, get_wall_clock, get_clock_freq);
    ///
    /// loop {
    ///     let frame_alpha = clock.run_frame(|tick| {
    ///         previous_state = current_state;
    ///         integrate(current_state, tick.t, tick.dt);
    ///     }
    ///
    ///     // Linear interpolate between the previous and new states.
    ///     current_state = current_state * alpha + previous_state * (1. - frame_alpha);
    /// }
    /// ```
    pub fn run_frame<S>(&mut self, mut simulation: S) -> f32
    where
        S: FnMut(TickInfo),
    {
        let new_tick = (self.get_wall_clock)();
        let frame_time = self.get_secs_elapsed(new_tick).min(0.25) as f32;

        self.last_tick = new_tick;
        self.accumulator += frame_time;

        while self.accumulator >= self.dt {
            simulation(TickInfo::new(self.t, self.dt));
            self.t += self.dt as f64;
            self.accumulator -= self.dt;
        }

        self.accumulator / self.dt
    }
}
