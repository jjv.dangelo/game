cbuffer PerObject: register(b0) {
     float4x4 world;
}
 
cbuffer PerFrame: register(b1) {
    float4x4 view_proj;
    float elapsed;
    float dt;
}

Texture2D _texture: register(t0);
SamplerState _sampler_state: register(s0);

struct VIn {
    float3 position  : POSITION;
    float4 color     : COLOR;
    float2 uv        : TEX_COORD;
};

struct VOut {
    float4 position  : SV_POSITION;
    float4 color     : COLOR;
    float2 uv        : TEX_COORD;
};

VOut VShader(VIn input) {
    VOut output;

    float4x4 wvp = mul(world, view_proj);

    output.color = input.color;
    output.position = mul(float4(input.position, 1.), wvp);
    output.uv = input.uv;

    return output;
}

float4 PShader(VOut vs_output) : SV_TARGET {
    return _texture.Sample(_sampler_state, vs_output.uv) * vs_output.color;
}
